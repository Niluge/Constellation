package com.constellation.core.model;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Représente un OI d'une rubrique.
 * 
 * @author j.varin
 * 
 */
public class OI implements Parcelable {

	/**
	 * L'identifiant de l'OI.
	 */
	private String id;

	/**
	 * Le nom de l'OI.
	 */
	private String title;

	/**
	 * La latitude de l'OI.
	 */
	private float coordX;

	/**
	 * La longitude de l'OI.
	 */
	private float coordY;

	/**
	 * L'identifiant du type de l'OI.
	 */
	private String idType;

	/**
	 * L'identifiant du sous-type de l'OI;
	 */
	private String idSousType;

	/**
	 * L'identifiant de la catÃ©gorue de l'OI.
	 */
	private String idCategory;

	/**
	 * L'adresse de l'OI.
	 */
	private Address address;

	/**
	 * Les moyens de communiquer avec l'OI (mail, tÃ©l...).
	 */
	private List<Communication> moyensCom;

	/**
	 * Le classement officiels ?
	 */
	private List<Label> classOfficiels;

	/**
	 * Le classement par labels ?
	 */
	private List<Label> classLabels;

	/**
	 * Le classement Handi ?
	 */
	private List<Label> classHandi;

	/**
	 * Le classement par chaines.
	 */
	private List<Label> classChaines;

	/**
	 * Les moyens de paiement.
	 */
	private List<Label> modesPaiement;

	/**
	 * Les langues parlÃ©s dans l'Ã©tablissement.
	 */
	private List<Label> langAccueil;

	/**
	 * La desciption de l'OI.
	 */
	private Traduction trad;

	/**
	 * La liste des médias associés.
	 */
	private List<Media> medias;
	
	/**
	 * La liste des médias associés.
	 */
	private List<EmbeddedMedia> embeddedMedias;

	/**
	 * La liste des vidéos associés.
	 */
	private List<Media> videos;

	/**
	 * La liste des services scea ?
	 */
	private List<Label> sceaServices;

	/**
	 * La liste des activites scea ?
	 */
	private List<Label> sceaActivites;

	/**
	 * La liste des conforts scea ?
	 */
	private List<Label> sceaConforts;

	/**
	 * La liste des encadrements scea ?
	 */
	private List<Label> sceaEncadrements;

	/**
	 * La liste des équipements scea ?
	 */
	private List<Label> sceaEquipements;

	/**
	 * La liste des dates.
	 */
	private List<Dates> dates;

	/**
	 * Retourne l'identifiant de l'OI.
	 * 
	 * @return L'id.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Modifie l'identifiant de l'OI.
	 * 
	 * @param id
	 *            L'id Ã  modifier.
	 */
	@JsonSetter("Id")
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Retourne le nom de l'OI.
	 * 
	 * @return Le title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Modifie le nom de l'OI.
	 * 
	 * @param title
	 *            Le title Ã  modifier.
	 */
	@JsonSetter("Title")
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Retourne la latitude de l'OI.
	 * 
	 * @return La coordX;
	 */
	@JsonProperty("X")
	public float getCoordX() {
		return coordX;
	}

	/**
	 * Modifie la latitude de l'OI.
	 * 
	 * @param coordX
	 *            La coordX Ã  modifier.
	 */
	@JsonSetter("X")
	public void setCoordX(float coordX) {
		this.coordX = coordX;
	}

	/**
	 * Retourne la longitude de l'OI.
	 * 
	 * @return La coordY.
	 */
	@JsonProperty("Y")
	public float getCoordY() {
		return coordY;
	}

	/**
	 * Modifie la longitude de l'OI.
	 * 
	 * @param coordY
	 *            La coordY Ã  modifier.
	 */
	@JsonSetter("Y")
	public void setCoordY(float coordY) {
		this.coordY = coordY;
	}

	/**
	 * Retourne l'identifiant du type de l'OI.
	 * 
	 * @return L'idType.
	 */
	public String getIdType() {
		return idType;
	}

	/**
	 * Modifie l'identifiant du type de l'OI.
	 * 
	 * @param idType
	 *            L'idType Ã  modifier.
	 */
	@JsonSetter("IdType")
	public void setIdType(String idType) {
		this.idType = idType;
	}

	/**
	 * Retourne l'identifiant du sous-type de l'OI;
	 * 
	 * @return L'idSousType.
	 */
	public String getIdSousType() {
		return idSousType;
	}

	/**
	 * Modifie l'identifiant du sous-type de l'OI;
	 * 
	 * @param idSousType
	 *            L'idSousType Ã  modifier.
	 */
	@JsonSetter("IdSousType")
	public void setIdSousType(String idSousType) {
		this.idSousType = idSousType;
	}

	/**
	 * Retourne l''identifiant de la catégorue de l'OI.
	 * 
	 * @return L'idCategory.
	 */
	public String getIdCategory() {
		return idCategory;
	}

	/**
	 * Retourne l'identifiant de la catégorue de l'OI.
	 * 
	 * @param idCategory
	 *            L'idCategory Ã  modifier.
	 */
	@JsonSetter("IdCategorie")
	public void setIdCategory(String idCategory) {
		this.idCategory = idCategory;
	}

	/**
	 * Retourne l'adresse de l'OI.
	 * 
	 * @return L'address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * Modifie l'adresse de l'OI.
	 * 
	 * @param address
	 *            L'address Ã  modifier.
	 */
	@JsonSetter("Adresse")
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * Retourne les moyens de communiquer avec l'OI (mail, tél...)
	 * 
	 * @return Les moyensCom.
	 */
	public List<Communication> getMoyensCom() {
		return moyensCom;
	}

	/**
	 * Modifie les moyens de communiquer avec l'OI (mail, tél...)
	 * 
	 * @param moyensCom
	 *            Les moyensCom Ã  modifier.
	 */
	@JsonSetter("MoyensCom")
	public void setMoyensCom(List<Communication> moyensCom) {
		this.moyensCom = moyensCom;
	}

	/**
	 * Retourne le classement officiels ?
	 * 
	 * @return Les classOfficiels.
	 */
	public List<Label> getClassOfficiels() {
		return classOfficiels;
	}

	/**
	 * Modifie le classement officiels ?
	 * 
	 * @param classOfficiels
	 *            Les classOfficiels Ã  modifier.
	 */
	@JsonSetter("ClassOfficiels")
	public void setClassOfficiels(List<Label> classOfficiels) {
		this.classOfficiels = classOfficiels;
	}

	/**
	 * Retourne le classement par labels ?
	 * 
	 * @return Les classLabels.
	 */
	public List<Label> getClassLabels() {
		return classLabels;
	}

	/**
	 * Modifie le classement par labels ?
	 * 
	 * @param classLabels
	 *            Les classLabels Ã  modifier.
	 */
	@JsonSetter("ClassLabels")
	public void setClassLabels(List<Label> classLabels) {
		this.classLabels = classLabels;
	}

	/**
	 * Retourne le classement Handi ?
	 * 
	 * @return Les classHandi.
	 */
	public List<Label> getClassHandi() {
		return classHandi;
	}

	/**
	 * Modifie le classement Handi ?
	 * 
	 * @param classHandi
	 *            Les classHandi Ã  modifier.
	 */
	@JsonSetter("ClassHandi")
	public void setClassHandi(List<Label> classHandi) {
		this.classHandi = classHandi;
	}

	/**
	 * Retourne le classement par chaines.
	 * 
	 * @return Les classChaines.
	 */
	public List<Label> getClassChaines() {
		return classChaines;
	}

	/**
	 * Modifie le classement par chaines.
	 * 
	 * @param classChaines
	 *            Les classChaines Ã  modifier.
	 */
	@JsonSetter("ClassChaines")
	public void setClassChaines(List<Label> classChaines) {
		this.classChaines = classChaines;
	}

	/**
	 * Retourne les moyens de paiement.
	 * 
	 * @return Les modesPaiement.
	 */
	public List<Label> getModesPaiement() {
		return modesPaiement;
	}

	/**
	 * Modifie les moyens de paiement.
	 * 
	 * @param modesPaiement
	 *            Les modesPaiement Ã  modifier.
	 */
	@JsonSetter("ModesPaiement")
	public void setModesPaiement(List<Label> modesPaiement) {
		this.modesPaiement = modesPaiement;
	}

	/**
	 * Retourne les langues parlés dans l'établissement.
	 * 
	 * @return Les langAccueil.
	 */
	public List<Label> getLangAccueil() {
		return langAccueil;
	}

	/**
	 * Modifie les langues parlés dans l'établissement.
	 * 
	 * @param langAccueil
	 *            Les langAccueil Ã  modifier.
	 */
	@JsonSetter("LangAccueil")
	public void setLangAccueil(List<Label> langAccueil) {
		this.langAccueil = langAccueil;
	}

	/**
	 * Retourne la desciption de l'OI.
	 * 
	 * @return La trad.
	 */
	public Traduction getTrad() {
		return trad;
	}

	/**
	 * Modifie la desciption de l'OI.
	 * 
	 * @param trad
	 *            La trad Ã  modifier.
	 */
	@JsonSetter("Traduction")
	public void setTrad(Traduction trad) {
		this.trad = trad;
	}

	/**
	 * Retourne la liste des médias associés.
	 * 
	 * @return Les medias.
	 */
	public List<Media> getMedias() {
		return medias;
	}

	/**
	 * Modifie la liste des médias associés.
	 * 
	 * @param medias
	 *            Les medias Ã  modifier.
	 */
	@JsonSetter("Medias")
	public void setMedias(List<Media> medias) {
		this.medias = medias;
	}

	/**
	 * Retourne la liste des vidéos associés.
	 * 
	 * @return Les videos.
	 */
	public List<Media> getVideos() {
		return videos;
	}

	/**
	 * Modifie la liste des vidéos associés.
	 * 
	 * @param videos
	 *            Les videos Ã  modifier.
	 */
	@JsonSetter("Videos")
	public void setVideos(List<Media> videos) {
		this.videos = videos;
	}

	/**
	 * Retourne la liste des services scea ?
	 * 
	 * @return Les sceaServices.
	 */
	public List<Label> getSceaServices() {
		return sceaServices;
	}

	/**
	 * Modifie la liste des services scea ?
	 * 
	 * @param sceaServices
	 *            Les sceaServices Ã  modifier.
	 */
	@JsonSetter("SCEA_Services")
	public void setSceaServices(List<Label> sceaServices) {
		this.sceaServices = sceaServices;
	}

	/**
	 * Retourne la liste des activites scea ?
	 * 
	 * @return Les sceaActivites.
	 */
	public List<Label> getSceaActivites() {
		return sceaActivites;
	}

	/**
	 * Modifie la liste des activites scea ?
	 * 
	 * @param sceaActivites
	 *            Les sceaActivites Ã  modifier.
	 */
	@JsonSetter("SCEA_Activites")
	public void setSceaActivites(List<Label> sceaActivites) {
		this.sceaActivites = sceaActivites;
	}

	/**
	 * Retourne la liste des conforts scea ?
	 * 
	 * @return Les sceaConforts.
	 */
	public List<Label> getSceaConforts() {
		return sceaConforts;
	}

	/**
	 * Modifie la liste des conforts scea ?
	 * 
	 * @param sceaConforts
	 *            Les sceaConforts Ã  modifier.
	 */
	@JsonSetter("SCEA_Conforts")
	public void setSceaConforts(List<Label> sceaConforts) {
		this.sceaConforts = sceaConforts;
	}

	/**
	 * Retourne la liste des encadrements scea ?
	 * 
	 * @return Les sceaEncadrements.
	 */
	public List<Label> getSceaEncadrements() {
		return sceaEncadrements;
	}

	/**
	 * Modifie la liste des encadrements scea ?
	 * 
	 * @param sceaEncadrements
	 *            Les sceaEncadrements Ã  modifier.
	 */
	@JsonSetter("SCEA_Encadrements")
	public void setSceaEncadrements(List<Label> sceaEncadrements) {
		this.sceaEncadrements = sceaEncadrements;
	}

	/**
	 * Retourne la liste des équipements scea ?
	 * 
	 * @return Les sceaEquipements.
	 */
	public List<Label> getSceaEquipements() {
		return sceaEquipements;
	}

	/**
	 * Modifie la liste des équipements scea ?
	 * 
	 * @param sceaEquipements
	 *            Les sceaEquipements Ã  modifier.
	 */
	@JsonSetter("SCEA_Equipements")
	public void setSceaEquipements(List<Label> sceaEquipements) {
		this.sceaEquipements = sceaEquipements;
	}

	/**
	 * Retourne la liste des dates.
	 * 
	 * @return Les dates.
	 */
	public List<Dates> getDates() {
		return dates;
	}

	/**
	 * Modifie la liste des dates.
	 * 
	 * @param dates
	 *            Les dates Ã  modifier.
	 */
	@JsonSetter("Dates")
	public void setDates(List<Dates> dates) {
		this.dates = dates;
	}
	
	/**
	 * Retourne la liste des dates.
	 * 
	 * @return Les dates.
	 */
	public List<EmbeddedMedia> getEmbeddedMedias() {
		return embeddedMedias;
	}

	/**
	 * Modifie la liste des dates.
	 * 
	 * @param dates
	 *            Les dates Ã  modifier.
	 */
	@JsonSetter("EmbeddedMedias")
	public void setEmbeddedMedias(List<EmbeddedMedia> embeddedMedias) {
		
		this.embeddedMedias = embeddedMedias;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result
				+ ((classChaines == null) ? 0 : classChaines.hashCode());
		result = prime * result
				+ ((classHandi == null) ? 0 : classHandi.hashCode());
		result = prime * result
				+ ((classLabels == null) ? 0 : classLabels.hashCode());
		result = prime * result
				+ ((classOfficiels == null) ? 0 : classOfficiels.hashCode());
		result = prime * result + Float.floatToIntBits(coordX);
		result = prime * result + Float.floatToIntBits(coordY);
		result = prime * result + ((dates == null) ? 0 : dates.hashCode());
		result = prime * result
				+ ((embeddedMedias == null) ? 0 : embeddedMedias.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((idCategory == null) ? 0 : idCategory.hashCode());
		result = prime * result
				+ ((idSousType == null) ? 0 : idSousType.hashCode());
		result = prime * result + ((idType == null) ? 0 : idType.hashCode());
		result = prime * result
				+ ((langAccueil == null) ? 0 : langAccueil.hashCode());
		result = prime * result + ((medias == null) ? 0 : medias.hashCode());
		result = prime * result
				+ ((modesPaiement == null) ? 0 : modesPaiement.hashCode());
		result = prime * result
				+ ((moyensCom == null) ? 0 : moyensCom.hashCode());
		result = prime * result
				+ ((sceaActivites == null) ? 0 : sceaActivites.hashCode());
		result = prime * result
				+ ((sceaConforts == null) ? 0 : sceaConforts.hashCode());
		result = prime
				* result
				+ ((sceaEncadrements == null) ? 0 : sceaEncadrements.hashCode());
		result = prime * result
				+ ((sceaEquipements == null) ? 0 : sceaEquipements.hashCode());
		result = prime * result
				+ ((sceaServices == null) ? 0 : sceaServices.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((trad == null) ? 0 : trad.hashCode());
		result = prime * result + ((videos == null) ? 0 : videos.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OI other = (OI) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (classChaines == null) {
			if (other.classChaines != null)
				return false;
		} else if (!classChaines.equals(other.classChaines))
			return false;
		if (classHandi == null) {
			if (other.classHandi != null)
				return false;
		} else if (!classHandi.equals(other.classHandi))
			return false;
		if (classLabels == null) {
			if (other.classLabels != null)
				return false;
		} else if (!classLabels.equals(other.classLabels))
			return false;
		if (classOfficiels == null) {
			if (other.classOfficiels != null)
				return false;
		} else if (!classOfficiels.equals(other.classOfficiels))
			return false;
		if (Float.floatToIntBits(coordX) != Float.floatToIntBits(other.coordX))
			return false;
		if (Float.floatToIntBits(coordY) != Float.floatToIntBits(other.coordY))
			return false;
		if (dates == null) {
			if (other.dates != null)
				return false;
		} else if (!dates.equals(other.dates))
			return false;
		if (embeddedMedias == null) {
			if (other.embeddedMedias != null)
				return false;
		} else if (!embeddedMedias.equals(other.embeddedMedias))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idCategory == null) {
			if (other.idCategory != null)
				return false;
		} else if (!idCategory.equals(other.idCategory))
			return false;
		if (idSousType == null) {
			if (other.idSousType != null)
				return false;
		} else if (!idSousType.equals(other.idSousType))
			return false;
		if (idType == null) {
			if (other.idType != null)
				return false;
		} else if (!idType.equals(other.idType))
			return false;
		if (langAccueil == null) {
			if (other.langAccueil != null)
				return false;
		} else if (!langAccueil.equals(other.langAccueil))
			return false;
		if (medias == null) {
			if (other.medias != null)
				return false;
		} else if (!medias.equals(other.medias))
			return false;
		if (modesPaiement == null) {
			if (other.modesPaiement != null)
				return false;
		} else if (!modesPaiement.equals(other.modesPaiement))
			return false;
		if (moyensCom == null) {
			if (other.moyensCom != null)
				return false;
		} else if (!moyensCom.equals(other.moyensCom))
			return false;
		if (sceaActivites == null) {
			if (other.sceaActivites != null)
				return false;
		} else if (!sceaActivites.equals(other.sceaActivites))
			return false;
		if (sceaConforts == null) {
			if (other.sceaConforts != null)
				return false;
		} else if (!sceaConforts.equals(other.sceaConforts))
			return false;
		if (sceaEncadrements == null) {
			if (other.sceaEncadrements != null)
				return false;
		} else if (!sceaEncadrements.equals(other.sceaEncadrements))
			return false;
		if (sceaEquipements == null) {
			if (other.sceaEquipements != null)
				return false;
		} else if (!sceaEquipements.equals(other.sceaEquipements))
			return false;
		if (sceaServices == null) {
			if (other.sceaServices != null)
				return false;
		} else if (!sceaServices.equals(other.sceaServices))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (trad == null) {
			if (other.trad != null)
				return false;
		} else if (!trad.equals(other.trad))
			return false;
		if (videos == null) {
			if (other.videos != null)
				return false;
		} else if (!videos.equals(other.videos))
			return false;
		return true;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OI [id=");
		builder.append(id);
		builder.append(", title=");
		builder.append(title);
		builder.append(", coordX=");
		builder.append(coordX);
		builder.append(", coordY=");
		builder.append(coordY);
		builder.append(", idType=");
		builder.append(idType);
		builder.append(", idSousType=");
		builder.append(idSousType);
		builder.append(", idCategory=");
		builder.append(idCategory);
		builder.append(", address=");
		builder.append(address);
		builder.append(", moyensCom=");
		builder.append(moyensCom);
		builder.append(", classOfficiels=");
		builder.append(classOfficiels);
		builder.append(", classLabels=");
		builder.append(classLabels);
		builder.append(", classHandi=");
		builder.append(classHandi);
		builder.append(", classChaines=");
		builder.append(classChaines);
		builder.append(", modesPaiement=");
		builder.append(modesPaiement);
		builder.append(", langAccueil=");
		builder.append(langAccueil);
		builder.append(", trad=");
		builder.append(trad);
		builder.append(", medias=");
		builder.append(medias);
		builder.append(", embeddedMedias=");
		builder.append(embeddedMedias);
		builder.append(", videos=");
		builder.append(videos);
		builder.append(", sceaServices=");
		builder.append(sceaServices);
		builder.append(", sceaActivites=");
		builder.append(sceaActivites);
		builder.append(", sceaConforts=");
		builder.append(sceaConforts);
		builder.append(", sceaEncadrements=");
		builder.append(sceaEncadrements);
		builder.append(", sceaEquipements=");
		builder.append(sceaEquipements);
		builder.append(", dates=");
		builder.append(dates);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Constructeur par défaut
	 */
	public OI() {
		super();
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public OI(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramÃ¨tre d'une actitivity.
	 */
	public static final Parcelable.Creator<OI> CREATOR = new Parcelable.Creator<OI>() {

		public OI createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new OI(in);
		}

		public OI[] newArray(int size) {
			return new OI[size];
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	@SuppressWarnings("unchecked")
	private void readFromParcel(Parcel in) {

		this.setId(in.readString());
		this.setTitle(in.readString());
		this.setCoordX(in.readFloat());
		this.setCoordY(in.readFloat());
		this.setIdType(in.readString());
		this.setIdSousType(in.readString());
		this.setIdCategory(in.readString());
		this.setAddress((Address) in.readParcelable(Address.class
				.getClassLoader()));
		this.setMoyensCom(in.readArrayList(Communication.class.getClassLoader()));
		this.setClassOfficiels(in.readArrayList(Label.class.getClassLoader()));
		this.setClassLabels(in.readArrayList(Label.class.getClassLoader()));
		this.setClassHandi(in.readArrayList(Label.class.getClassLoader()));
		this.setClassChaines(in.readArrayList(Label.class.getClassLoader()));
		this.setModesPaiement(in.readArrayList(Label.class.getClassLoader()));
		this.setLangAccueil(in.readArrayList(Label.class.getClassLoader()));
		this.setTrad((Traduction) in.readParcelable(Traduction.class
				.getClassLoader()));
		this.setMedias(in.readArrayList(Media.class.getClassLoader()));
		this.setVideos(in.readArrayList(Media.class.getClassLoader()));
		this.setSceaServices(in.readArrayList(Label.class.getClassLoader()));
		this.setSceaActivites(in.readArrayList(Label.class.getClassLoader()));
		this.setSceaConforts(in.readArrayList(Label.class.getClassLoader()));
		this.setSceaEncadrements(in.readArrayList(Label.class.getClassLoader()));
		this.setSceaEquipements(in.readArrayList(Label.class.getClassLoader()));
		this.setDates(in.readArrayList(Dates.class.getClassLoader()));
		this.setEmbeddedMedias(in.readArrayList(EmbeddedMedia.class.getClassLoader()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.getId());
		dest.writeString(this.getTitle());
		dest.writeFloat(this.getCoordX());
		dest.writeFloat(this.getCoordY());
		dest.writeString(this.getIdType());
		dest.writeString(this.getIdSousType());
		dest.writeString(this.getIdCategory());
		dest.writeParcelable(this.getAddress(),
				Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
		dest.writeList(this.getMoyensCom());
		dest.writeList(this.getClassOfficiels());
		dest.writeList(this.getClassLabels());
		dest.writeList(this.getClassHandi());
		dest.writeList(this.getClassChaines());
		dest.writeList(this.getModesPaiement());
		dest.writeList(this.getLangAccueil());
		dest.writeParcelable(this.getTrad(),
				Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
		dest.writeList(this.getMedias());
		dest.writeList(this.getVideos());
		dest.writeList(this.getSceaServices());
		dest.writeList(this.getSceaActivites());
		dest.writeList(this.getSceaConforts());
		dest.writeList(this.getSceaEncadrements());
		dest.writeList(this.getSceaEquipements());
		dest.writeList(this.getDates());
		dest.writeList(this.getEmbeddedMedias());
	}
}
