package com.constellation.core.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonSetter;

/**
 * Représente une liste de projets.
 * 
 * @author j.varin
 * 
 */
public class Projets {

	/**
	 * La liste de projet.
	 */
	private List<Project> projects;

	/**
	 * Retourne la liste de projet.
	 * 
	 * @return Les projects
	 */
	public List<Project> getProjects() {
		return projects;
	}

	/**
	 * Modifie la liste de projet.
	 * 
	 * @param projects
	 *            Les projects à modifier.
	 */
	@JsonSetter("Projets")
	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((projects == null) ? 0 : projects.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Projets other = (Projets) obj;
		if (projects == null) {
			if (other.projects != null)
				return false;
		} else if (!projects.equals(other.projects))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Projets [projects=");
		builder.append(projects);
		builder.append("]");
		return builder.toString();
	}
}
