package com.constellation.core.model;

import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

public class EmbeddedMedia implements Parcelable {
	
	/**
	 * Le titre du média.
	 */
	protected String titre;

	/**
	 * Le code du média.
	 */
	protected String code;

	/**
	 * Retourne le titre du média.
	 * 
	 * @return Le titre.
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * Modifier le titre du média.
	 * 
	 * @param titre
	 *            Le titre à modifier.
	 */
	@JsonSetter("Titre")
	public void setTitre(String titre) {
		this.titre = titre;
	}

	/**
	 * Retourne le code du média.
	 * 
	 * @return Le code du média.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Modifier le code du média.
	 * 
	 * @param code
	 *            Le code du média.
	 */
	@JsonSetter("Code")
	public void setCode(String code) {
		this.code = code;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((titre == null) ? 0 : titre.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmbeddedMedia other = (EmbeddedMedia) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (titre == null) {
			if (other.titre != null)
				return false;
		} else if (!titre.equals(other.titre))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmbeddedMedia [titre=");
		builder.append(titre);
		builder.append(", code=");
		builder.append(code);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Constructeur par défaut
	 */
	public EmbeddedMedia() {
		super();
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public EmbeddedMedia(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramètre d'une actitivity.
	 */
	public static final Parcelable.Creator<EmbeddedMedia> CREATOR = new Parcelable.Creator<EmbeddedMedia>() {

		public EmbeddedMedia createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new EmbeddedMedia(in);
		}

		public EmbeddedMedia[] newArray(int size) {
			return new EmbeddedMedia[size];
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	private void readFromParcel(Parcel in) {

		this.setTitre(in.readString());
		this.setCode(in.readString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.getTitre());
		dest.writeString(this.getCode());
	}

}
