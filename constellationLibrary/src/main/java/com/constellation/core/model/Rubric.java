package com.constellation.core.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Représente les rubriques d'un thème.
 * 
 * @author j.varin
 * 
 */
public class Rubric implements Parcelable {
	
	/**
	 * L'identifiant unique de la rubrique.
	 */
	private String id;

	/**
	 * Le nom de la rubrique.
	 */
	private String title;

	/**
	 * Le type de la rubrique.
	 */
	private RubricType type;

	/**
	 * L'URL du pictogramme à afficher sur la carte pour les OIs de cette
	 * rubrique.
	 */
	private String image;

	/**
	 * Le nombre total d'OIs de la rubrique.
	 */
	private int totalOIs;

	/**
	 * La liste des OIs de la rubrique.
	 */
	private List<OI> oiList;

	/**
	 * Retourne l'identifiant unique de la rubrique.
	 * 
	 * @return L'id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Modifie l'identifiant unique de la rubrique.
	 * 
	 * @param id
	 *            L'id à modifier.
	 */
	@JsonSetter("Id")
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Retourne le nom de la rubrique.
	 * 
	 * @return Le title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Modifie le nom de la rubrique.
	 * 
	 * @param title
	 *            Le title à modifier.
	 */
	@JsonSetter("Title")
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Retourne le type de la rubrique.
	 * 
	 * @return Le type
	 */
	public RubricType getType() {
		return type;
	}
	
	/**
	 * Modifie le type de la rubrique.
	 * 
	 * @param type
	 *            Le type à modifier.
	 */
	public void setType(RubricType type) {
		this.type = type;
	}

	/**
	 * Modifie le type de la rubrique.
	 * 
	 * @param type
	 *            Le type à modifier.
	 */
	@JsonSetter("Type")
	public void setType(Integer type) {
		this.type = RubricType.getValue(type);
	}

	/**
	 * Retourne l'URL du pictogramme à afficher
	 * 
	 * @return L'image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Modifie l'URL du pictogramme à afficher
	 * 
	 * @param image
	 *            L'image à modifier.
	 */
	@JsonSetter("Img")
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Retourne le nombre total d'OIs de la rubrique.
	 * 
	 * @return Le totalOIs
	 */
	public int getTotalOIs() {
		return totalOIs;
	}

	/**
	 * Modifie le nombre total d'OIs de la rubrique.
	 * 
	 * @param totalOIs
	 *            Le totalOIs à modifier.
	 */
	@JsonSetter("TotalOIs")
	public void setTotalOIs(int totalOIs) {
		this.totalOIs = totalOIs;
	}

	/**
	 * Retourne la liste des OIs de la rubrique.
	 * 
	 * @return La oiList
	 */
	/*public List<OI> getOiList() {
		return oiList;
	}*/

	/**
	 * Modifie la liste des OIs de la rubrique.
	 * 
	 * @param oiList
	 *            La oiList à modifier.
	 */
	/*public void setOiList(List<OI> oiList) {
		this.oiList = oiList;
	}*/

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + ((oiList == null) ? 0 : oiList.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + totalOIs;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rubric other = (Rubric) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (oiList == null) {
			if (other.oiList != null)
				return false;
		} else if (!oiList.equals(other.oiList))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (totalOIs != other.totalOIs)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Rubric [id=");
		builder.append(id);
		builder.append(", title=");
		builder.append(title);
		builder.append(", type=");
		builder.append(type);
		builder.append(", image=");
		builder.append(image);
		builder.append(", totalOIs=");
		builder.append(totalOIs);
		builder.append(", oiList=");
		builder.append(oiList);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Constructeur par défaut
	 */
	public Rubric() {
		super();
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public Rubric(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramètre d'une actitivity.
	 */
	public static final Parcelable.Creator<Rubric> CREATOR = new Parcelable.Creator<Rubric>() {

		public Rubric createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new Rubric(in);
		}

		public Rubric[] newArray(int size) {
			return new Rubric[size];
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	//@SuppressWarnings("unchecked")
	private void readFromParcel(Parcel in) {

		this.setId(in.readString());
		this.setTitle(in.readString());
		this.setType(in.readInt());
		this.setImage(in.readString());
		this.setTotalOIs(in.readInt());
		//this.setOiList(in.readArrayList(OI.class.getClassLoader()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.getId());
		dest.writeString(this.getTitle());
		dest.writeInt(this.getType().getIntValue());
		dest.writeString(this.getImage());
		dest.writeInt(this.getTotalOIs());
		//dest.writeList(this.getOiList());
	}
}
