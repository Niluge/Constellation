package com.constellation.core.model;

import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Représente les dates d'un OI.
 * 
 * @author j.varin
 * 
 */
public class Dates implements Parcelable {

	private int id;
	
	/**
	 * Date de debut.
	 */
	private String from;

	/**
	 * Date de fin.
	 */
	private String to;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retourne la date de début.
	 * 
	 * @return Le from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * Modifie la date de début.
	 * 
	 * @param from
	 *            Le from à modifier
	 */
	@JsonSetter("Du")
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * Retourne la date de fin.
	 * 
	 * @return Le to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * Modifie la date de fin.
	 * 
	 * @param to
	 *            Le to à modifier.
	 */
	@JsonSetter("Au")
	public void setTo(String to) {
		this.to = to;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dates other = (Dates) obj;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Dates [from=");
		builder.append(from);
		builder.append(", to=");
		builder.append(to);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Constructeur par défaut
	 */
	public Dates() {
		super();
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public Dates(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramètre d'une actitivity.
	 */
	public static final Parcelable.Creator<Dates> CREATOR = new Parcelable.Creator<Dates>() {

		public Dates createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new Dates(in);
		}

		public Dates[] newArray(int size) {
			return new Dates[size];
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	private void readFromParcel(Parcel in) {

		this.setFrom(in.readString());
		this.setTo(in.readString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.getFrom());
		dest.writeString(this.getTo());
	}
}
