package com.constellation.core.model;

import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Représente la desciption de l'OI.
 * 
 * @author j.varin
 * 
 */
public class Traduction implements Parcelable {

	/**
	 * Le titre de la description.
	 */
	private String titre;

	/**
	 * La valeur chapeau de la description.
	 */
	private String chapeau;

	/**
	 * La valeur de la description.
	 */
	private String article;

	/**
	 * Retourne le titre.
	 * 
	 * @return Le titre.
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * Modifie le titre
	 * 
	 * @param titre
	 *            Le titre à modifier.
	 */
	@JsonSetter("Titre")
	public void setTitre(String titre) {
		this.titre = titre;
	}

	/**
	 * Retourne la valeur de la description.
	 * 
	 * @return Le chapeau.
	 */
	public String getChapeau() {
		return chapeau;
	}

	/**
	 * Modifie la valeur chapeau de la description.
	 * 
	 * @param chapeau
	 *            Le chapeau à modifier.
	 */
	@JsonSetter("Chapeau")
	public void setChapeau(String chapeau) {
		this.chapeau = chapeau;
	}

	/**
	 * Retourne la valeur de la description.
	 * 
	 * @return L'article
	 */
	public String getArticle() {
		return article;
	}

	/**
	 * Modifie la valeur de la description.
	 * 
	 * @param article
	 *            L'article à modifier.
	 */
	@JsonSetter("Article")
	public void setArticle(String article) {
		this.article = article;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((article == null) ? 0 : article.hashCode());
		result = prime * result + ((chapeau == null) ? 0 : chapeau.hashCode());
		result = prime * result + ((titre == null) ? 0 : titre.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Traduction other = (Traduction) obj;
		if (article == null) {
			if (other.article != null)
				return false;
		} else if (!article.equals(other.article))
			return false;
		if (chapeau == null) {
			if (other.chapeau != null)
				return false;
		} else if (!chapeau.equals(other.chapeau))
			return false;
		if (titre == null) {
			if (other.titre != null)
				return false;
		} else if (!titre.equals(other.titre))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Traduction [titre=");
		builder.append(titre);
		builder.append(", chapeau=");
		builder.append(chapeau);
		builder.append(", article=");
		builder.append(article);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Constructeur par défaut
	 */
	public Traduction() {
		super();
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public Traduction(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramètre d'une actitivity.
	 */
	public static final Parcelable.Creator<Traduction> CREATOR = new Parcelable.Creator<Traduction>() {

		public Traduction createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new Traduction(in);
		}

		public Traduction[] newArray(int size) {
			return new Traduction[size];
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	private void readFromParcel(Parcel in) {

		this.setTitre(in.readString());
		this.setChapeau(in.readString());
		this.setArticle(in.readString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.getTitre());
		dest.writeString(this.getChapeau());
		dest.writeString(this.getArticle());
	}
}
