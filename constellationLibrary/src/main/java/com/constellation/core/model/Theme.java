package com.constellation.core.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Représente les différents thèmes d'une guide.
 * 
 * @author j.varin
 * 
 */
public class Theme implements Parcelable {

	/**
	 * L'identifiant du theme
	 */
	private int id;

	/**
	 * Le nom du thème.
	 */
	private String name;

	/**
	 * La description du thème.
	 */
	private String desc;

	/**
	 * L'URL du pictogramme du thème.
	 */
	private String img;

	/**
	 * L'image affichée en header de la page du thème.
	 */
	private String header;

	/**
	 * Le rayon max de la recherche à proximité (0 = pas de limite)
	 */
	private float radius;

	/**
	 * Type d'affichage des rubriques.
	 */
	private Display display;

	/**
	 * Les rubriques associés au thème.
	 */
	private List<Rubric> rubrics;

	/**
	 * L'URL de la page à afficher.
	 */
	private String url;

	/**
	 * Le type du theme.
	 */
	private ThemeType type;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retourne le nom du thème.
	 * 
	 * @return Le name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Modifie le nom du thème.
	 * 
	 * @param name
	 *            Le name à modifier.
	 */
	@JsonSetter("Name")
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Retourne la description du thème.
	 * 
	 * @return Le desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Modifie la description du thème.
	 * 
	 * @param desc
	 *            Le desc à modifier.
	 */
	@JsonSetter("Desc")
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * Retourne l'URL du pictogramme du thème.
	 * 
	 * @return Le img
	 */
	public String getImg() {
		return img;
	}

	/**
	 * Modifie l'URL du pictogramme du thème.
	 * 
	 * @param img
	 *            Le img à modifier.
	 */
	@JsonSetter("Img")
	public void setImg(String img) {
		this.img = img;
	}

	/**
	 * Retourne l'image affichée en header de la page du thème.
	 * 
	 * @return Le header.
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * Modifie l'image affichée en header de la page du thème.
	 * 
	 * @param header
	 *            Le header à modifier.
	 */
	@JsonSetter("Header")
	public void setHeader(String header) {
		this.header = header;
	}

	/**
	 * Retourne le rayon de recherche.
	 * 
	 * @return Le radius.
	 */
	public float getRadius() {
		return radius;
	}

	/**
	 * Modifie le rayon de recherche.
	 * 
	 * @param radius
	 *            Le radius à modifier.
	 */
	@JsonSetter("Radius")
	public void setRadius(float radius) {
		this.radius = radius;
	}

	/**
	 * Retourne les rubriques associés au thème.
	 * 
	 * @return Les rubrics
	 */
	public List<Rubric> getRubrics() {

		return rubrics;
	}

	/**
	 * Modifie les rubriques associés au thème.
	 * 
	 * @param rubrics
	 *            Les rubrics à modifier.
	 */
	@JsonSetter("Rubriques")
	public void setRubrics(List<Rubric> rubrics) {

		this.rubrics = rubrics;
	}

	/**
	 * Retourne le type d'affichage des rubriques.
	 * 
	 * @return Le display.
	 */
	public Display getDisplay() {
		if (display == null)
			return Display.Liste;
		return display;
	}

	/**
	 * Modifie le type d'affichage des rubriques.
	 * 
	 * @param display
	 *            Le display à modifier.
	 */
	public void setDisplay(Display display) {
		this.display = display;
	}

	/**
	 * Modifie le type d'affichage des rubriques.
	 * 
	 * @param display
	 *            Le display à modifier.
	 */
	@JsonSetter("Display")
	public void setDisplay(Integer display) {
		this.display = Display.getValue(display);
	}

	/**
	 * Retourne l'URL de la page.
	 * 
	 * @return L'url.
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Modifie l'URL de la page.
	 * 
	 * @param url
	 *            L'url à modifier.
	 */
	@JsonSetter("Url")
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Retourne le type du theme.
	 * 
	 * @return Le type
	 */
	public ThemeType getType() {
		return type;
	}

	/**
	 * Modifie le type du theme.
	 * 
	 * @param type
	 *            Le type à modifier.
	 */
	public void setType(ThemeType type) {
		this.type = type;
	}

	/**
	 * Modifie le type du theme.
	 * 
	 * @param type
	 *            Le type à modifier.
	 */
	@JsonSetter("Type")
	public void setType(Integer type) {
		this.type = ThemeType.getValue(type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((display == null) ? 0 : display.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Float.floatToIntBits(radius);
		result = prime * result + ((rubrics == null) ? 0 : rubrics.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Theme other = (Theme) obj;
		if (desc == null) {
			if (other.desc != null)
				return false;
		} else if (!desc.equals(other.desc))
			return false;
		if (display != other.display)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Float.floatToIntBits(radius) != Float.floatToIntBits(other.radius))
			return false;
		if (getRubrics() == null) {
			if (other.getRubrics() != null)
				return false;
		} else if (!getRubrics().equals(other.getRubrics()))
			return false;
		if (type != other.type)
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Theme [name=");
		builder.append(name);
		builder.append(", desc=");
		builder.append(desc);
		builder.append(", img=");
		builder.append(img);
		builder.append(", header=");
		builder.append(header);
		builder.append(", radius=");
		builder.append(radius);
		builder.append(", rubrics=");
		builder.append(rubrics);
		builder.append(", url=");
		builder.append(url);
		builder.append(", type=");
		builder.append(type);
		builder.append(", display=");
		builder.append(display);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Constructeur par défaut
	 */
	public Theme() {
		super();
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public Theme(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramètre d'une actitivity.
	 */
	public static final Parcelable.Creator<Theme> CREATOR = new Parcelable.Creator<Theme>() {

		public Theme createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new Theme(in);
		}

		public Theme[] newArray(int size) {
			return new Theme[size];
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	@SuppressWarnings("unchecked")
	private void readFromParcel(Parcel in) {

		this.setName(in.readString());
		this.setDesc(in.readString());
		this.setImg(in.readString());
		this.setHeader(in.readString());
		this.setRadius(in.readFloat());
		this.setRubrics(in.readArrayList(Rubric.class.getClassLoader()));
		this.setDisplay(in.readInt());
		this.setUrl(in.readString());
		this.setType(in.readInt());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.getName());
		dest.writeString(this.getDesc());
		dest.writeString(this.getImg());
		dest.writeString(this.getHeader());
		dest.writeFloat(this.getRadius());
		dest.writeList(this.getRubrics());
		dest.writeInt(this.getDisplay().getIntValue());
		dest.writeString(this.getUrl());
		dest.writeInt(this.getType().getIntValue());
	}
}
