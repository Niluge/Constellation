package com.constellation.core.model;

import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Représente un média associé à un OI.
 * 
 * @author j.varin
 * 
 */
public class Media implements Parcelable {
	
	/**
	 * Le titre du média.
	 */
	protected String titre;

	/**
	 * l'URL du média.
	 */
	protected String url;

	/**
	 * Retourne le titre du média.
	 * 
	 * @return Le titre.
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * Modifier le titre du média.
	 * 
	 * @param titre
	 *            Le titre à modifier.
	 */
	@JsonSetter("Titre")
	public void setTitre(String titre) {
		this.titre = titre;
	}

	/**
	 * Retourne l'URL du média.
	 * 
	 * @return L'url.
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Modifie l'URL du média.
	 * 
	 * @param url
	 *            L'url à modifier.
	 */
	@JsonSetter("Url")
	public void setUrl(String url) {
		this.url = url;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((titre == null) ? 0 : titre.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Media other = (Media) obj;
		if (titre == null) {
			if (other.titre != null)
				return false;
		} else if (!titre.equals(other.titre))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Media [titre=");
		builder.append(titre);
		builder.append(", url=");
		builder.append(url);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Constructeur par défaut
	 */
	public Media() {
		super();
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public Media(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramètre d'une actitivity.
	 */
	public static final Parcelable.Creator<Media> CREATOR = new Parcelable.Creator<Media>() {

		public Media createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new Media(in);
		}

		public Media[] newArray(int size) {
			return new Media[size];
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	private void readFromParcel(Parcel in) {

		this.setTitre(in.readString());
		this.setUrl(in.readString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.getTitre());
		dest.writeString(this.getUrl());
	}
}
