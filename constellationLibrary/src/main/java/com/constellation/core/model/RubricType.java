package com.constellation.core.model;

/**
 * Spécifie le type des rubriques.
 * 
 * @author j.varin
 * 
 */
public enum RubricType {

	/**
	 * Le type par défaut.
	 */
	Default(0),

	/**
	 * Le type hébergement
	 */
	Hebergement(1),

	/**
	 * Le type évènement
	 */
	Evenement(2),

	/**
	 * Le type actualité.
	 */
	Actu(3);

	/**
	 * La valeur Constellation du type.
	 */
	private int value;

	/**
	 * Constructeur de type.
	 * 
	 * @param value
	 *            La valeur Constellation.
	 */
	private RubricType(int value) {
		this.value = value;
	}

	/**
	 * Retourne la valeur Constellation du type.
	 * 
	 * @return La valeur du type.
	 */
	public int getIntValue() {
		return value;
	}

	/**
	 * Retourne le type de rubrique associé à la valeur Constellation.
	 * 
	 * @param value
	 *            La valeur Constellation.
	 * @return Le type.
	 */
	public static RubricType getValue(int value) {

		if (Default.getIntValue() == value) {
			return Default;
		}
		if (Hebergement.getIntValue() == value) {
			return Hebergement;
		}
		if (Evenement.getIntValue() == value) {
			return Evenement;
		}
		if (Actu.getIntValue() == value) {
			return Actu;
		}

		return Default;
	}

}
