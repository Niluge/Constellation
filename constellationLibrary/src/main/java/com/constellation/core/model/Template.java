package com.constellation.core.model;

/**
 * Spécifie les templates des guides.
 * 
 * @author j.varin
 * 
 */
public enum Template {

	/**
	 * Pas de template.
	 */
	None(-1),

	/**
	 * Template orange.
	 */
	Orange(0),

	/**
	 * Template vert.
	 */
	Green(1),
	
	/**
	 * Template bleu.
	 */
	Blue(2);

	/**
	 * La valeur Constellation du template.
	 */
	private int value;

	/**
	 * Constructeur de template.
	 * 
	 * @param value
	 *            La valeur Constellation.
	 */
	private Template(int value) {
		this.value = value;
	}

	/**
	 * Retourne la valeur Constellation du template.
	 * 
	 * @return La valeur du template.
	 */
	public int getIntValue() {
		return value;
	}

	/**
	 * Retourne le template associé à la valeur Constellation.
	 * 
	 * @param value
	 *            La valeur Constellation.
	 * @return Le template.
	 */
	public static Template getValue(int value) {

		if (Orange.getIntValue() == value) {
			return Orange;
		}
		if (Blue.getIntValue() == value) {
			return Blue;
		}
		if (Green.getIntValue() == value) {
			return Green;
		}
		
		return None;
	}
}
