package com.constellation.core.model;

import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Représente un moyen de contact d'un OI.
 * 
 * @author j.varin
 * 
 */
public class Communication implements Parcelable {
	
	/**
	 * Le type du moyen de communication.
	 */
	private String type;

	/**
	 * La valeur du moyen de communication.
	 */
	private String coord;
	
	private OI oi;

	/**
	 * Retourne le type du moyen de communication.
	 * 
	 * @return Le type.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Modifie le type du moyen de communication.
	 * 
	 * @param type
	 *            Le type à modifier.
	 */
	@JsonSetter("Type")
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Retourne la valeur du moyen de communication.
	 * 
	 * @return La coord.
	 */
	public String getCoord() {
		return coord;
	}

	/**
	 * Modifie la valeur du moyen de communication.
	 * 
	 * @param coord
	 *            La coord à modifier.
	 */
	@JsonSetter("Coord")
	public void setCoord(String coord) {
		this.coord = coord;
	}
	
	/**
	 * @return the oi
	 */
	public OI getOi() {
		return oi;
	}

	/**
	 * @param oi the oi to set
	 */
	public void setOi(OI oi) {
		this.oi = oi;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((coord == null) ? 0 : coord.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Communication other = (Communication) obj;
		if (coord == null) {
			if (other.coord != null)
				return false;
		} else if (!coord.equals(other.coord))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Communication [type=");
		builder.append(type);
		builder.append(", coord=");
		builder.append(coord);
		builder.append("]");
		return builder.toString();
	}
	
	/**
	 * Constructeur par défaut
	 */
	public Communication() {
		super();
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public Communication(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramètre d'une actitivity.
	 */
	public static final Parcelable.Creator<Communication> CREATOR = new Parcelable.Creator<Communication>() {

		public Communication createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new Communication(in);
		}

		public Communication[] newArray(int size) {
			return new Communication[size];
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	private void readFromParcel(Parcel in) {

		this.setType(in.readString());
		this.setCoord(in.readString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.getType());
		dest.writeString(this.getCoord());
	}
}
