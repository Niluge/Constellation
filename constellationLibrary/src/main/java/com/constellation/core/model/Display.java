package com.constellation.core.model;

/**
 * Spécifie le type de l'affichage des listes de rubriques.
 * 
 * @author j.varin
 * 
 */
public enum Display {

	/**
	 * Affiche liste.
	 */
	Liste(1),

	/**
	 * Affiche avec des pictos.
	 */
	Picto(2),

	/**
	 * Affiche coverflow.
	 */
	Coverflow(3);

	/**
	 * La valeur Constellation du display.
	 */
	private int value;

	/**
	 * Constructeur de display.
	 * 
	 * @param value
	 *            La valeur Constellation.
	 */
	private Display(int value) {
		this.value = value;
	}

	/**
	 * Retourne la valeur Constellation du display.
	 * 
	 * @return La valeur du display.
	 */
	public int getIntValue() {
		return value;
	}

	/**
	 * Retourne le display associé à la valeur Constellation.
	 * 
	 * @param value
	 *            La valeur Constellation.
	 * @return Le display.
	 */
	public static Display getValue(int value) {

		if (Liste.getIntValue() == value) {
			return Liste;
		}
		if (Picto.getIntValue() == value) {
			return Picto;
		}
		if (Coverflow.getIntValue() == value) {
			return Picto;
		}

		return Picto;
	}
}
