package com.constellation.core.model;

import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Représente le label d'un classement.
 * 
 * @author j.varin
 * 
 */
public class Label implements Parcelable {
	
	/**
	 * le nom du label.
	 */
	protected String name;

	/**
	 * Le code du label.
	 */
	protected String code;

	/**
	 * Retourne le nom du label.
	 * 
	 * @return Le name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Modifie le nom du label.
	 * 
	 * @param name
	 *            Le nom à modifier.
	 */
	@JsonSetter("Nom")
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Retourne le code du label.
	 * 
	 * @return Le code.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Modifie le code du label.
	 * 
	 * @param code
	 *            Le code à modifier.
	 */
	@JsonSetter("Code")
	public void setCode(String code) {
		this.code = code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Label other = (Label) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Label [name=");
		builder.append(name);
		builder.append(", code=");
		builder.append(code);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Constructeur par défaut
	 */
	public Label() {
		super();
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public Label(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramÃ¨tre d'une actitivity.
	 */
	public static final Parcelable.Creator<Label> CREATOR = new Parcelable.Creator<Label>() {

		public Label createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new Label(in);
		}

		public Label[] newArray(int size) {
			return new Label[size];
		}
	};
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	protected void readFromParcel(Parcel in) {

		this.setName(in.readString());
		this.setCode(in.readString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.getName());
		dest.writeString(this.getCode());
	}
}
