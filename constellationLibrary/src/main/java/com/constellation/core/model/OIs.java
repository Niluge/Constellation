package com.constellation.core.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonSetter;

/**
 * Représente les Ois d'une rubrique.
 * 
 * @author j.varin
 * 
 */
public class OIs {

	/**
	 * La liste des OIs.
	 */
	private List<OI> ois;

	/**
	 * Retourne les OIs.
	 * 
	 * @return Les ois
	 */
	public List<OI> getOIs() {
		return ois;
	}

	/**
	 * Modifie les OIs.
	 * 
	 * @param projects
	 *            Les projects à modifier.
	 */
	@JsonSetter("Ois")
	public void setOIs(List<OI> ois) {
		this.ois = ois;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((ois == null) ? 0 : ois.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OIs other = (OIs) obj;
		if (ois == null) {
			if (other.ois != null)
				return false;
		} else if (!ois.equals(other.ois))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("OIs [ois=");
		builder.append(ois);
		builder.append("]");
		return builder.toString();
	}
}
