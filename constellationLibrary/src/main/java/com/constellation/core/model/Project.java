package com.constellation.core.model;

import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Représente un guide.
 * 
 * @author j.varin
 * 
 */
public class Project implements Parcelable {

	/**
	 * L'identifiant unique du guide.
	 */
	private String id;

	/**
	 * Le code du projet permettant une recherche rapide du guide.
	 */
	private String code;

	/**
	 * Le nom du guide.
	 */
	private String title;

	/**
	 * La description du guide.
	 */
	private String description;

	/**
	 * L'URL de la vignette du guide.
	 */
	private String image;

	/**
	 * L'URL du logo du guide.
	 */
	private String logo;

	/**
	 * L'image affichée en header de la page.
	 */
	private String header;

	/**
	 * L'image affichée pendant le chargement d'un guide.
	 */
	private String splashScreen;

	/**
	 * L'identifiant unique de l'ECT.
	 */
	private String ectId;

	/**
	 * Le nom de l'ECT.
	 */
	private String ectName;

	/**
	 * La latitude de l'ECT.
	 */
	private String coordX;

	/**
	 * La longitude de l'ECT.
	 */
	private String coordY;

	/**
	 * L'identifiant de la langue de l'application (Français = 134).
	 */
	private int language;

	/**
	 * L'identifiant du template graphique.
	 */
	private Template template;

	/**
	 * L'identifiant unique de la playlist utilisée.
	 */
	private String playlistId;

	/**
	 * L'état de l'option de recherche à proximité.
	 */
	private boolean activeArroundMe;

	/**
	 * L'état de l'option "Réseaux sociaux".
	 */
	private boolean activeSocialNetworks;

	/**
	 * Les thèmes du guide.
	 */
	private List<Theme> themes;

	/**
	 * L'URL à partager
	 */
	private String urlForShare;

	private boolean store;

	/**
	 * Retourne l'identifiant unique du guide.
	 * 
	 * @return L'id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Modifie l'identifiant unique du guide.
	 * 
	 * @param id
	 *            L'id à modifier.
	 */
	@JsonSetter("Id")
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Retourne le code du projet.
	 * 
	 * @return Le code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Modifie le code du projet.
	 * 
	 * @param code
	 *            Le code à modifier.
	 */
	@JsonSetter("Code")
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Retourne le nom du guide.
	 * 
	 * @return Le title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Modifie le nom du guide.
	 * 
	 * @param title
	 *            Le title à modifier.
	 */
	@JsonSetter("Title")
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Retourne la description du guide.
	 * 
	 * @return La description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Modifie la description du guide.
	 * 
	 * @param description
	 *            La description à modifier.
	 */
	@JsonSetter("Description")
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Retourne l'URL de la vignette du guide.
	 * 
	 * @return L'image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Modifie l'URL de la vignette du guide.
	 * 
	 * @param image
	 *            L'image à modifier.
	 */
	@JsonSetter("Image")
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Retourne l'URL du logo du guide.
	 * 
	 * @return Le logo
	 */
	public String getLogo() {
		return logo;
	}

	/**
	 * Modifie l'URL du logo du guide.
	 * 
	 * @param logo
	 *            Le logo à modifier.
	 */
	@JsonSetter("Logo")
	public void setLogo(String logo) {
		this.logo = logo;
	}

	/**
	 * Retourne l'image affichée en header de la page.
	 * 
	 * @return Le header.
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * Modifie l'image affichée en header de la page.
	 * 
	 * @param header
	 *            Le header à modifier.
	 */
	@JsonSetter("Header")
	public void setHeader(String header) {
		this.header = header;
	}

	/**
	 * Retourne l'image affichée pendant le chargement d'un guide.
	 * 
	 * @return Le splashScreen
	 */
	public String getSplashScreen() {
		return splashScreen;
	}

	/**
	 * Modifie l'image affichée pendant le chargement d'un guide.
	 * 
	 * @param splashScreen
	 *            Le splashScreen à modifier.
	 */
	@JsonSetter("SplashScreen")
	public void setSplashScreen(String splashScreen) {
		this.splashScreen = splashScreen;
	}

	/**
	 * Retourne l'identifiant unique de l'ECT.
	 * 
	 * @return L'ectId
	 */
	public String getEctId() {
		return ectId;
	}

	/**
	 * Modifie l'identifiant unique de l'ECT.
	 * 
	 * @param ectId
	 *            L'ectId à modifier.
	 */
	@JsonSetter("EctId")
	public void setEctId(String ectId) {
		this.ectId = ectId;
	}

	/**
	 * Retourne le nom de l'ECT.
	 * 
	 * @return L'ectName
	 */
	public String getEctName() {
		return ectName;
	}

	/**
	 * Modifie le nom de l'ECT.
	 * 
	 * @param ectName
	 *            L'ectName à modifier.
	 */
	@JsonSetter("EctName")
	public void setEctName(String ectName) {
		this.ectName = ectName;
	}

	/**
	 * Retourne la latitude de l'ECT.
	 * 
	 * @return La coordX
	 */
	@JsonProperty("X")
	public String getCoordX() {
		return coordX;
	}

	/**
	 * Modifie la latitude de l'ECT.
	 * 
	 * @param coordX
	 *            La coordX à modifier.
	 */
	@JsonSetter("X")
	public void setCoordX(String coordX) {
		this.coordX = coordX;
	}

	/**
	 * Retourne la longitude de l'ECT.
	 * 
	 * @return La coordY
	 */
	@JsonProperty("Y")
	public String getCoordY() {
		return coordY;
	}

	/**
	 * Modifie la longitude de l'ECT.
	 * 
	 * @param coordY
	 *            La coordY à modifier.
	 */
	@JsonSetter("Y")
	public void setCoordY(String coordY) {
		this.coordY = coordY;
	}

	/**
	 * Retourne l'identifiant de la langue de l'application.
	 * 
	 * @return Le language
	 */
	public int getLanguage() {
		return language;
	}

	/**
	 * Modifie l'identifiant de la langue de l'application.
	 * 
	 * @param language
	 *            Le language à modifier.
	 */
	@JsonSetter("Language")
	public void setLanguage(int language) {
		this.language = language;
	}

	/**
	 * Retourne l'identifiant du template graphique.
	 * 
	 * @return Le template
	 */
	public Template getTemplate() {
		return template;
	}

	/**
	 * Modifie l'identifiant du template graphique.
	 * 
	 * @param template
	 *            Le template à modifier.
	 */
	public void setTemplate(Template template) {
		this.template = template;
	}

	/**
	 * Modifie l'identifiant du template graphique.
	 * 
	 * @param template
	 *            Le template à modifier.
	 */
	@JsonSetter("Template")
	public void setTemplate(Integer template) {
		this.template = Template.getValue(template);
	}

	/**
	 * Retourne l'identifiant unique de la playlist utilisée.
	 * 
	 * @return Le playlistId
	 */
	public String getPlaylistId() {
		return playlistId;
	}

	/**
	 * Modifie l'identifiant unique de la playlist utilisée.
	 * 
	 * @param playlistId
	 *            Le playlistId à modifier.
	 */
	@JsonSetter("IdPlaylist")
	public void setPlaylistId(String playlistId) {
		this.playlistId = playlistId;
	}

	/**
	 * @return the activeArroundMe
	 */
	public boolean isActiveArroundMe() {
		return activeArroundMe;
	}

	/**
	 * @param activeArroundMe
	 *            the activeArroundMe to set
	 */
	public void setActiveArroundMe(boolean activeArroundMe) {
		this.activeArroundMe = activeArroundMe;
	}

	/**
	 * @return the activeSocialNetworks
	 */
	public boolean isActiveSocialNetworks() {
		return activeSocialNetworks;
	}

	/**
	 * @param activeSocialNetworks
	 *            the activeSocialNetworks to set
	 */
	public void setActiveSocialNetworks(boolean activeSocialNetworks) {
		this.activeSocialNetworks = activeSocialNetworks;
	}

	/**
	 * Modifie les options à afficher.
	 * 
	 * @param options
	 *            Les options à modifier.
	 */
	@JsonSetter("Options")
	public void setOptions(List<Options> options) {
		for (Iterator<Options> iterator = options.iterator(); iterator
				.hasNext();) {

			Options option = iterator.next();
			this.activeArroundMe = (this.activeArroundMe || option
					.isActiveAroundMe());
			this.activeSocialNetworks = (this.activeSocialNetworks || option
					.isActiveSocialNetworks());
		}
	}

	/**
	 * Retourne les thèmes du guide.
	 * 
	 * @return Les themes
	 */
	public List<Theme> getThemes() {

		return themes;
	}

	/**
	 * Modifie les thèmes du guide.
	 * 
	 * @param themes
	 *            Les themes à modifier.
	 */
	@JsonSetter("Themes")
	public void setThemes(List<Theme> themes) {

		this.themes = themes;
	}

	/**
	 * @return the store
	 */
	public boolean isStore() {
		return store;
	}

	/**
	 * @param store
	 *            the store to set
	 */
	public void setStore(boolean store) {
		this.store = store;
	}

	/**
	 * Modifie l'URL à partager.
	 * 
	 * @param UrlForShare
	 *            L'URL à modifier.
	 */
	@JsonSetter("UrlForShare")
	public void setUrlForShare(String urlForShare) {
		this.urlForShare = urlForShare;
	}

	/**
	 * Retourne l'URL à partager.
	 * 
	 * @return URL
	 */
	public String getUrlForShare() {
		return urlForShare;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (activeArroundMe ? 1231 : 1237);
		result = prime * result + (activeSocialNetworks ? 1231 : 1237);
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((coordX == null) ? 0 : coordX.hashCode());
		result = prime * result + ((coordY == null) ? 0 : coordY.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((ectId == null) ? 0 : ectId.hashCode());
		result = prime * result + ((ectName == null) ? 0 : ectName.hashCode());
		result = prime * result + ((header == null) ? 0 : header.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + language;
		result = prime * result + ((logo == null) ? 0 : logo.hashCode());
		result = prime * result
				+ ((urlForShare == null) ? 0 : urlForShare.hashCode());
		result = prime * result
				+ ((playlistId == null) ? 0 : playlistId.hashCode());
		result = prime * result
				+ ((splashScreen == null) ? 0 : splashScreen.hashCode());
		result = prime * result
				+ ((template == null) ? 0 : template.hashCode());
		result = prime * result + ((themes == null) ? 0 : themes.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (activeArroundMe != other.activeArroundMe)
			return false;
		if (activeSocialNetworks != other.activeSocialNetworks)
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (coordX == null) {
			if (other.coordX != null)
				return false;
		} else if (!coordX.equals(other.coordX))
			return false;
		if (coordY == null) {
			if (other.coordY != null)
				return false;
		} else if (!coordY.equals(other.coordY))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (ectId == null) {
			if (other.ectId != null)
				return false;
		} else if (!ectId.equals(other.ectId))
			return false;
		if (ectName == null) {
			if (other.ectName != null)
				return false;
		} else if (!ectName.equals(other.ectName))
			return false;
		if (header == null) {
			if (other.header != null)
				return false;
		} else if (!header.equals(other.header))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (language != other.language)
			return false;
		if (logo == null) {
			if (other.logo != null)
				return false;
		} else if (!logo.equals(other.logo))
			return false;
		if (urlForShare == null) {
			if (other.urlForShare != null)
				return false;
		} else if (!urlForShare.equals(other.urlForShare))
			return false;
		if (playlistId == null) {
			if (other.playlistId != null)
				return false;
		} else if (!playlistId.equals(other.playlistId))
			return false;
		if (splashScreen == null) {
			if (other.splashScreen != null)
				return false;
		} else if (!splashScreen.equals(other.splashScreen))
			return false;
		if (template != other.template)
			return false;
		if (getThemes() == null) {
			if (other.getThemes() != null)
				return false;
		} else if (!getThemes().equals(other.getThemes()))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Project [id=");
		builder.append(id);
		builder.append(", code=");
		builder.append(code);
		builder.append(", title=");
		builder.append(title);
		builder.append(", description=");
		builder.append(description);
		builder.append(", image=");
		builder.append(image);
		builder.append(", logo=");
		builder.append(logo);
		builder.append(", header=");
		builder.append(header);
		builder.append(", splashScreen=");
		builder.append(splashScreen);
		builder.append(", ectId=");
		builder.append(ectId);
		builder.append(", ectName=");
		builder.append(ectName);
		builder.append(", coordX=");
		builder.append(coordX);
		builder.append(", coordY=");
		builder.append(coordY);
		builder.append(", language=");
		builder.append(language);
		builder.append(", template=");
		builder.append(template);
		builder.append(", urlForShare=");
		builder.append(urlForShare);
		builder.append(", playlistId=");
		builder.append(playlistId);
		builder.append(", activeArroundMe=");
		builder.append(activeArroundMe);
		builder.append(", activeSocialNetworks=");
		builder.append(activeSocialNetworks);
		builder.append(", themes=");
		builder.append(getThemes());
		builder.append(", store=");
		builder.append(isStore());
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Constructeur par défaut
	 */
	public Project() {
		super();
		this.store = false;
		this.activeArroundMe = false;
		this.activeSocialNetworks = false;
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public Project(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramètre d'une actitivity.
	 */
	public static final Parcelable.Creator<Project> CREATOR = new Parcelable.Creator<Project>() {

		public Project createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new Project(in);
		}

		public Project[] newArray(int size) {
			return new Project[size];
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	@SuppressWarnings("unchecked")
	private void readFromParcel(Parcel in) {

		this.setId(in.readString());
		this.setCode(in.readString());
		this.setTitle(in.readString());
		this.setDescription(in.readString());
		this.setImage(in.readString());
		this.setLogo(in.readString());
		this.setHeader(in.readString());
		this.setSplashScreen(in.readString());
		this.setEctId(in.readString());
		this.setEctName(in.readString());
		this.setCoordX(in.readString());
		this.setCoordY(in.readString());
		this.setLanguage(in.readInt());
		this.setTemplate(in.readInt());
		this.setUrlForShare(in.readString());
		this.setPlaylistId(in.readString());
		this.setActiveArroundMe(in.readByte() == 1);
		this.setActiveSocialNetworks(in.readByte() == 1);
		this.setStore(in.readByte() == 1);
		this.setThemes(in.readArrayList(Theme.class.getClassLoader()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.getId());
		dest.writeString(this.getCode());
		dest.writeString(this.getTitle());
		dest.writeString(this.getDescription());
		dest.writeString(this.getImage());
		dest.writeString(this.getLogo());
		dest.writeString(this.getHeader());
		dest.writeString(this.getSplashScreen());
		dest.writeString(this.getEctId());
		dest.writeString(this.getEctName());
		dest.writeString(this.getCoordX());
		dest.writeString(this.getCoordY());
		dest.writeInt(this.getLanguage());
		dest.writeInt(this.getTemplate().getIntValue());
		dest.writeString(this.getUrlForShare());
		dest.writeString(this.getPlaylistId());
		byte activeArroundMe;
		if (this.isActiveArroundMe()) {
			activeArroundMe = 1;
		} else {
			activeArroundMe = 0;
		}
		dest.writeByte(activeArroundMe);
		byte activeSocialNetworks;
		if (this.isActiveSocialNetworks()) {
			activeSocialNetworks = 1;
		} else {
			activeSocialNetworks = 0;
		}
		dest.writeByte(activeSocialNetworks);
		byte isStore;
		if (this.isStore()) {
			isStore = 1;
		} else {
			isStore = 0;
		}
		dest.writeByte(isStore);
		dest.writeList(this.getThemes());
	}
}
