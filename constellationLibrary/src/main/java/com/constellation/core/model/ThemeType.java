package com.constellation.core.model;

/**
 * Spécifie le type des themes.
 * 
 * @author j.varin
 * 
 */
public enum ThemeType {

	/**
	 * Le type rubrique.
	 */
	Rubric(1),

	/**
	 * Le type évènement.
	 */
	Separator(2),

	/**
	 * Le type hébergement.
	 */
	Web(3),

	/**
	 * Le type actualité.
	 */
	AroundMe(4);

	/**
	 * La valeur Constellation du type.
	 */
	private int value;

	/**
	 * Constructeur de type.
	 * 
	 * @param value
	 *            La valeur Constellation.
	 */
	private ThemeType(int value) {
		this.value = value;
	}

	/**
	 * Retourne la valeur Constellation du type.
	 * 
	 * @return La valeur du type.
	 */
	public int getIntValue() {
		return value;
	}

	/**
	 * Retourne le type de rubrique associé à la valeur Constellation.
	 * 
	 * @param value
	 *            La valeur Constellation.
	 * @return Le type.
	 */
	public static ThemeType getValue(int value) {

		if (Rubric.getIntValue() == value) {
			return Rubric;
		}
		if (Separator.getIntValue() == value) {
			return Separator;
		}
		if (Web.getIntValue() == value) {
			return Web;
		}
		if (AroundMe.getIntValue() == value) {
			return AroundMe;
		}

		return Rubric;
	}

}
