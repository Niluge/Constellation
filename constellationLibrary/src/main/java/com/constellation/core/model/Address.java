package com.constellation.core.model;

import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Représente une adresse d'un OI.
 * 
 * @author j.varin
 * 
 */

public class Address implements Parcelable {

	
	/**
	 * L'identifiant de l'adresse.
	 */

	private String address0;

	/**
	 * Un complément d'adresse.
	 */
	private String complementAddress;

	/**
	 * Le numéro et le nom de la voie.
	 */
	private String libelleVoie;

	/**
	 * Les mentions de distribution spéciale.
	 */
	private String distributionSpeciale;

	/**
	 * Les code postal.
	 */
	private String codePostal;

	/**
	 * Le commune.
	 */
	private String commune;

	/**
	 * Retourne l'adresse.
	 * 
	 * @return L'address0.
	 */
	public String getAddress0() {
		return address0;
	}

	/**
	 * Modifie l'adresse.
	 * 
	 * @param address0
	 *            L'address0 à modifier.
	 */
	@JsonSetter("Adresse0")
	public void setAddress0(String address0) {
		this.address0 = address0;
	}

	/**
	 * Retourne un complément d'adresse.
	 * 
	 * @return Le complementAddress.
	 */
	public String getComplementAddress() {
		return complementAddress;
	}

	/**
	 * Modifie un complément d'adresse.
	 * 
	 * @param complementAddress
	 *            Le complementAddress à modifier.
	 */
	@JsonSetter("ComplementAdresse")
	public void setComplementAddress(String complementAddress) {
		this.complementAddress = complementAddress;
	}

	/**
	 * Retourne le numéro et le nom de la voie.
	 * 
	 * @return Le libelleVoie
	 */
	public String getLibelleVoie() {
		return libelleVoie;
	}

	/**
	 * Modifie le numéro et le nom de la voie.
	 * 
	 * @param libelleVoie
	 *            Le libelleVoie à modifier.
	 */
	@JsonSetter("LibelleVoie")
	public void setLibelleVoie(String libelleVoie) {
		this.libelleVoie = libelleVoie;
	}

	/**
	 * Retourne les mentions de distribution spéciale.
	 * 
	 * @return la distributionSpeciale.
	 */
	public String getDistributionSpeciale() {
		return distributionSpeciale;
	}

	/**
	 * Modifie les mentions de distribution spéciale.
	 * 
	 * @param distributionSpeciale
	 *            La distributionSpeciale à modifier.
	 */
	@JsonSetter("DistributionSpeciale")
	public void setDistributionSpeciale(String distributionSpeciale) {
		this.distributionSpeciale = distributionSpeciale;
	}

	/**
	 * Retourne le code postal.
	 * 
	 * @return Le codePostal.
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * Modifie le code postal.
	 * 
	 * @param codePostal
	 *            Le codePostal à modifier.
	 */
	@JsonSetter("CodePostal")
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * Retourne la commune.
	 * 
	 * @return La commune.
	 */
	public String getCommune() {
		return commune;
	}

	/**
	 * Modifie la commune.
	 * 
	 * @param commune
	 *            La commune à modifier.
	 */
	@JsonSetter("Commune")
	public void setCommune(String commune) {
		this.commune = commune;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((address0 == null) ? 0 : address0.hashCode());
		result = prime * result
				+ ((codePostal == null) ? 0 : codePostal.hashCode());
		result = prime * result + ((commune == null) ? 0 : commune.hashCode());
		result = prime
				* result
				+ ((complementAddress == null) ? 0 : complementAddress
						.hashCode());
		result = prime
				* result
				+ ((distributionSpeciale == null) ? 0 : distributionSpeciale
						.hashCode());
		result = prime * result
				+ ((libelleVoie == null) ? 0 : libelleVoie.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (address0 == null) {
			if (other.address0 != null)
				return false;
		} else if (!address0.equals(other.address0))
			return false;
		if (codePostal == null) {
			if (other.codePostal != null)
				return false;
		} else if (!codePostal.equals(other.codePostal))
			return false;
		if (commune == null) {
			if (other.commune != null)
				return false;
		} else if (!commune.equals(other.commune))
			return false;
		if (complementAddress == null) {
			if (other.complementAddress != null)
				return false;
		} else if (!complementAddress.equals(other.complementAddress))
			return false;
		if (distributionSpeciale == null) {
			if (other.distributionSpeciale != null)
				return false;
		} else if (!distributionSpeciale.equals(other.distributionSpeciale))
			return false;
		if (libelleVoie == null) {
			if (other.libelleVoie != null)
				return false;
		} else if (!libelleVoie.equals(other.libelleVoie))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Address [address0=");
		builder.append(address0);
		builder.append(", complementAddress=");
		builder.append(complementAddress);
		builder.append(", libelleVoie=");
		builder.append(libelleVoie);
		builder.append(", distributionSpeciale=");
		builder.append(distributionSpeciale);
		builder.append(", codePostal=");
		builder.append(codePostal);
		builder.append(", commune=");
		builder.append(commune);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Constructeur par défaut
	 */
	public Address() {
		super();
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public Address(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramètre d'une actitivity.
	 */
	public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {

		public Address createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new Address(in);
		}

		public Address[] newArray(int size) {
			return new Address[size];
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	private void readFromParcel(Parcel in) {

		this.setAddress0(in.readString());
		this.setComplementAddress(in.readString());
		this.setLibelleVoie(in.readString());
		this.setDistributionSpeciale(in.readString());
		this.setCodePostal(in.readString());
		this.setCommune(in.readString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.getAddress0());
		dest.writeString(this.getComplementAddress());
		dest.writeString(this.getLibelleVoie());
		dest.writeString(this.getDistributionSpeciale());
		dest.writeString(this.getCodePostal());
		dest.writeString(this.getCommune());
	}
}
