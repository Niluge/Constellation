package com.constellation.core.model;

import org.codehaus.jackson.annotate.JsonSetter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Représente les états d'affichage des options des OIs du guide.
 * 
 * @author j.varin
 * 
 */
public class Options implements Parcelable {

	/**
	 * L'état de l'option de recherche à proximité.
	 */
	private boolean activeAroundMe;

	/**
	 * L'état de l'option "Réseaux sociaux".
	 */
	private boolean activeSocialNetworks;

	/**
	 * Retourne l'état de l'option de recherche à proximité.
	 * 
	 * @return Le activeAroundMe.
	 */
	public boolean isActiveAroundMe() {
		return activeAroundMe;
	}

	/**
	 * Modifie l'état de l'option de recherche à proximité.
	 * 
	 * @param activeAroundMe
	 *            Le activeAroundMe à modifier.
	 */
	public void setActiveAroundMe(boolean activeAroundMe) {
		this.activeAroundMe = activeAroundMe;
	}
	
	/**
	 * Modifie l'état de l'option de recherche à proximité.
	 * 
	 * @param activeAroundMe
	 *            Le activeAroundMe à modifier.
	 */
	@JsonSetter("ActiveAroundMe")
	public void setActiveAroundMe(String activeAroundMe) {
		this.activeAroundMe = Boolean
				.parseBoolean(activeAroundMe.toLowerCase());
	}

	/**
	 * Retourne l'état de l'option "Réseaux sociaux".
	 * 
	 * @return Le activeSocialNetworks
	 */
	public boolean isActiveSocialNetworks() {
		return activeSocialNetworks;
	}

	/**
	 * Modifie l'état de l'option "Réseaux sociaux".
	 * 
	 * @param activeSocialNetworks
	 *            Le activeSocialNetworks à modifier.
	 */
	public void setActiveSocialNetworks(boolean activeSocialNetworks) {
		this.activeSocialNetworks = activeSocialNetworks;
	}
	
	/**
	 * Modifie l'état de l'option "Réseaux sociaux".
	 * 
	 * @param activeSocialNetworks
	 *            Le activeSocialNetworks à modifier.
	 */
	@JsonSetter("ActiveSocialNetworks")
	public void setActiveSocialNetworks(String activeSocialNetworks) {
		this.activeSocialNetworks = Boolean.parseBoolean(activeSocialNetworks
				.toLowerCase());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + (activeAroundMe ? 1231 : 1237);
		result = prime * result + (activeSocialNetworks ? 1231 : 1237);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Options other = (Options) obj;
		if (activeAroundMe != other.activeAroundMe)
			return false;
		if (activeSocialNetworks != other.activeSocialNetworks)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("Options [activeAroundMe=");
		builder.append(activeAroundMe);
		builder.append(", activeSocialNetworks=");
		builder.append(activeSocialNetworks);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Constructeur par défaut
	 */
	public Options() {
		super();
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	public Options(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Méthode qui permet de passé l'objet en paramètre d'une actitivity.
	 */
	public static final Parcelable.Creator<Options> CREATOR = new Parcelable.Creator<Options>() {

		public Options createFromParcel(Parcel in) {
			if (in == null) {
				return null;
			}
			return new Options(in);
		}

		public Options[] newArray(int size) {
			return new Options[size];
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Injecte les données du Parcel dans l'objet courant.
	 * 
	 * @param in
	 *            Parcel source.
	 */
	private void readFromParcel(Parcel in) {

		this.setActiveAroundMe(in.readString());
		this.setActiveSocialNetworks(in.readString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(Boolean.toString(this.isActiveAroundMe()));
		dest.writeString(Boolean.toString(this.isActiveSocialNetworks()));
	}
}
