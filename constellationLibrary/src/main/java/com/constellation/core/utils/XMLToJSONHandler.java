package com.constellation.core.utils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Handler pour parser le le flux XML.
 * 
 * @author j.varin
 * 
 */
public class XMLToJSONHandler extends DefaultHandler {

	private StringBuffer jsonStream;

	private boolean isListOpen;

	private boolean isStringOpen;

	public XMLToJSONHandler() {

		isListOpen = false;
		isStringOpen = false;
	}

	public String getJsonStream() {
		if (jsonStream == null) {
			return new String();
		}
		return jsonStream.toString();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {

		if (qName.equals("OIS")) {

			isListOpen = true;
			jsonStream = new StringBuffer();
		}

		if (qName.equals("string")) {

			isStringOpen = true;
			jsonStream = new StringBuffer();
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		if (qName.equals("OIS")) {
			isListOpen = false;
		}

		if (qName.equals("string")) {
			isStringOpen = false;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		if (isStringOpen) {
			jsonStream.append(new String(ch, start, length));
		}
		if (isListOpen) {
			jsonStream.append(new String(ch, start, length));
		}
	}

}
