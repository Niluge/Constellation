package com.constellation.core.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager.BadTokenException;

import com.constellation.core.R;
import com.constellation.core.activity.AbstractActivity;
import com.constellation.core.activity.CloseOIsMapActivity;

public class DialogUtils {

	public static void showMessageError(final Activity activity, Handler handler) {

		handler.post(new Runnable() {

			@Override
			public void run() {

				AlertDialog alertDialog = new AlertDialog.Builder(activity)
						.setMessage(activity.getString(R.string.msg_error))
						.setTitle(activity.getString(R.string.msg_error_title))
						.setCancelable(false)
						.setPositiveButton(R.string.btn_ok,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
										activity.finish();
									}
								}).create();

				try {
					alertDialog.show();
				} catch (BadTokenException e) {
					Log.e(getClass().getCanonicalName(),
							"Cannot show message error dialog.", e);
				}
			}
		});
	}

	public static void showConnectionError(final Activity activity,
			Handler handler) {

		handler.post(new Runnable() {

			@Override
			public void run() {

				AlertDialog.Builder alertDialogBuilder;
				alertDialogBuilder = new AlertDialog.Builder(activity);

				alertDialogBuilder.setMessage(activity
						.getString(R.string.msg_error_connection));
				alertDialogBuilder.setTitle(activity
						.getString(R.string.msg_error_title));

				alertDialogBuilder.setPositiveButton(
						activity.getString(R.string.btn_retry),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								if (activity instanceof AbstractActivity) {
									((AbstractActivity) activity).loadData();
								}
							}
						});
				alertDialogBuilder.setNegativeButton(
						activity.getString(R.string.btn_cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								activity.finish();
							}
						});

				AlertDialog alertDialog = alertDialogBuilder.create();

				try {
					alertDialog.show();
				} catch (BadTokenException e) {
					Log.e(getClass().getCanonicalName(),
							"Cannot show connection error dialog.", e);
				}
			}
		});
	}
	
	public static void showGeolocError(final Activity activity,
			Handler handler) {

		handler.post(new Runnable() {

			@Override
			public void run() {

				AlertDialog.Builder alertDialogBuilder;
				alertDialogBuilder = new AlertDialog.Builder(activity);

				alertDialogBuilder.setMessage(activity
						.getString(R.string.msg_error_geoloc));
				alertDialogBuilder.setTitle(activity
						.getString(R.string.msg_error_title));

				alertDialogBuilder.setPositiveButton(
						activity.getString(R.string.btn_retry),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								if (activity instanceof CloseOIsMapActivity) {
									((CloseOIsMapActivity) activity).loadData();
								}
							}
						});
				alertDialogBuilder.setNegativeButton(
						activity.getString(R.string.btn_cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								activity.finish();
							}
						});

				AlertDialog alertDialog = alertDialogBuilder.create();

				try {
					alertDialog.show();
				} catch (BadTokenException e) {
					Log.e(getClass().getCanonicalName(),
							"Cannot show geolocation error dialog.", e);
				}
			}
		});
	}
}
