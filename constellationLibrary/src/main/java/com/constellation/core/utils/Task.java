package com.constellation.core.utils;

/**
 * Représente les tâches de récupération des données à effectuer de manière
 * asynchrone.
 * 
 * @author j.varin
 * 
 */
public class Task extends Thread {

	/**
	 * L'identifiant de la tâche.
	 */
	private Integer taskId;

	/**
	 * Constructeur.
	 * 
	 * @param runnable
	 *            la tâche à exécuter.
	 * @param taskId
	 *            L'identifiant de la tâche.
	 */
	public Task(Runnable runnable, Integer taskId) {
		super(runnable);
		this.taskId = taskId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		super.run();
	}

	/**
	 * Retourne l'identifiant de la tâche
	 * 
	 * @return Le taskId
	 */
	public Integer getTaskId() {
		return taskId;
	}
}
