package com.constellation.core.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import android.util.Log;

import com.constellation.core.model.Dates;
import com.constellation.core.model.Label;
import com.constellation.core.model.OI;

public class FilterUtils {

	/**
	 * Filtre les OIs
	 */
	public static ArrayList<OI> filterOIs(List<OI> oisToFilter,
			int filterScore, long filterDateFrom, long filterDateTo) {

		Log.d(FilterUtils.class.getSimpleName(), "Filter S : " + filterScore);
		Log.d(FilterUtils.class.getSimpleName(), "Filter F : " + filterDateFrom);
		Log.d(FilterUtils.class.getSimpleName(), "Filter T : " + filterDateTo);

		ArrayList<OI> oisFiltered = (ArrayList<OI>) oisToFilter;

		if ((filterScore <= 5) && (filterScore >= 0)) {

			oisFiltered = new ArrayList<OI>();
			for (int i = 0; i < oisToFilter.size(); i++) {

				OI oiToFilter = oisToFilter.get(i);
				if (oiToFilter != null) {

					if ((oiToFilter.getClassOfficiels() != null)
							&& ((oiToFilter.getClassOfficiels().size() > 0))) {

						Label classOfficiel = oiToFilter.getClassOfficiels()
								.get(0);
						if (getScore(classOfficiel.getCode()) == filterScore) {
							oisFiltered.add(oiToFilter);
						}
					} else {
						if ((oiToFilter.getClassLabels() != null)
								&& ((oiToFilter.getClassLabels().size() > 0))) {

							Label classLabel = oiToFilter.getClassLabels().get(
									0);
							if (getScore(classLabel.getCode()) == filterScore) {
								oisFiltered.add(oiToFilter);
							}
						}
					}
				}
			}
		}

		else {
			if ((filterDateFrom > 0) && (filterDateTo > 0)) {

				oisFiltered = new ArrayList<OI>();
				for (OI currentOI : oisToFilter) {

					boolean toDisplay = false;

					if ((currentOI != null) && (currentOI.getDates() != null)
							&& (currentOI.getDates().size() > 0)) {

						for (Dates dates : currentOI.getDates()) {

							String dayF = dates.getFrom().substring(0, 2);
							String monthF = dates.getFrom().substring(3, 5);
							String yearF = dates.getFrom().substring(6, 10);
							String dayT = dates.getTo().substring(0, 2);
							String monthT = dates.getTo().substring(3, 5);
							String yearT = dates.getTo().substring(6, 10);

							Calendar fromCal = GregorianCalendar.getInstance(Locale.FRANCE);
							fromCal.set(Calendar.YEAR, Integer.parseInt(yearF));
							fromCal.set(Calendar.MONTH, Integer.parseInt(monthF) - 1);
							fromCal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayF));
							Date from = fromCal.getTime();
							
							Calendar toCal = GregorianCalendar.getInstance(Locale.FRANCE);
							toCal.set(Calendar.YEAR, Integer.parseInt(yearT));
							toCal.set(Calendar.MONTH, Integer.parseInt(monthT) - 1);
							toCal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayT));
							Date to = toCal.getTime();

							Calendar calDateFrom = GregorianCalendar.getInstance(Locale.FRANCE);
							calDateFrom.setTimeInMillis(filterDateFrom); 
							Date dateFrom = calDateFrom.getTime();
							Calendar calDateTo = GregorianCalendar.getInstance(Locale.FRANCE);
							calDateTo.setTimeInMillis(filterDateTo); 
							Date dateTo = calDateTo.getTime();

							if (from.after(dateFrom)) {
								if (from.after(dateTo)) {
									toDisplay = false;
								}
								else {
									toDisplay = true;
									break;
								}
							}
							else {
								if (to.before(dateTo)) {
									toDisplay = false;
								}
								else {
									toDisplay = true;
									break;
								}
							}
						}
					}

					if (toDisplay) {
						Log.d(FilterUtils.class.getSimpleName(), "OI displayed");
						oisFiltered.add(currentOI);
					}
				}
			}
		}

		return oisFiltered;
	}

	/**
	 * Calcul le nombre d'étoiles en fonction du code
	 * 
	 * @param code
	 * @return
	 */
	public static int getScore(String code) {
		if ((code.equals("06.03.01.02.01")) || (code.equals("06.03.01.03.01"))
				|| (code.equals("06.04.01.02.01"))
				|| (code.equals("06.04.01.03.01"))
				|| (code.equals("06.03.02.01.07"))
				|| (code.equals("06.03.02.02.01"))
				|| (code.equals("06.04.01.04.01"))
				|| (code.equals("06.03.05.01.01"))
				|| (code.equals("06.03.05.01.04"))
				|| (code.equals("06.03.05.01.07"))) {
			return 1;
		}
		if ((code.equals("06.03.01.02.02")) || (code.equals("06.03.01.03.02"))
				|| (code.equals("06.04.01.02.02"))
				|| (code.equals("06.04.01.03.02"))
				|| (code.equals("06.03.02.01.08"))
				|| (code.equals("06.03.02.02.02"))
				|| (code.equals("06.04.01.04.02"))
				|| (code.equals("06.03.05.01.02"))
				|| (code.equals("06.03.05.01.05"))
				|| (code.equals("06.03.05.01.08"))) {
			return 2;
		}
		if ((code.equals("06.03.01.02.03")) || (code.equals("06.03.01.03.03"))
				|| (code.equals("06.04.01.02.03"))
				|| (code.equals("06.04.01.03.03"))
				|| (code.equals("06.03.02.01.09"))
				|| (code.equals("06.03.02.02.03"))
				|| (code.equals("06.04.01.04.03"))
				|| (code.equals("06.03.05.01.03"))
				|| (code.equals("06.03.05.01.06"))
				|| (code.equals("06.03.05.01.09"))) {
			return 3;
		}
		if ((code.equals("06.03.01.03.04")) || (code.equals("06.04.01.02.04"))
				|| (code.equals("06.04.01.03.04"))
				|| (code.equals("06.03.02.01.10"))
				|| (code.equals("06.03.02.02.04"))
				|| (code.equals("06.04.01.04.04"))) {
			return 4;
		}
		if ((code.equals("06.04.01.03.05")) || (code.equals("06.04.01.03.11"))
				|| (code.equals("06.03.02.01.11"))
				|| (code.equals("06.03.02.02.05"))
				|| (code.equals("06.04.01.04.05"))) {
			return 5;
		}
		return 0;
	}

}
