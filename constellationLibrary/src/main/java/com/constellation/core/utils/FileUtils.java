package com.constellation.core.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.content.Context;

import com.constellation.core.exception.ConstellationException;
import com.constellation.core.model.Project;

public class FileUtils {

	public static boolean isProjectExists(Context context, String id) {

		File file = context.getFilesDir();
		List<String> listFile = Arrays.asList(file.list());
		return listFile.contains("Project_" + id + ".json");
	}
	
	public static boolean isRubricExists(Context context, String id) {

		File file = context.getFilesDir();
		List<String> listFile = Arrays.asList(file.list());
		return listFile.contains("Rubric_" + id + ".json");
	}

	@SuppressWarnings("unchecked")
	public static <T> T getObject(String fileName, Context context,
			TypeReference<T> typeReference) throws ConstellationException {

		try {

			FileInputStream fis = context.openFileInput(fileName);

			// Conversion du flux en String.
			String mainJSONStream = getJSONStream(fis);
			if (mainJSONStream == null) {
				return null;
			}
			
			// Traitement de la réponse.
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			T result = (T) mapper.readValue(mainJSONStream, typeReference);

			// Fermeture du flux.
			fis.close();

			return result;
		} catch (IOException e) {
			throw new ConstellationException("Cannot read JSON stream.", e);
		}
	}

	public static List<Project> getProjects(Context context) {

		File file = context.getFilesDir();
		List<String> listFile = Arrays.asList(file.list());

		List<Project> result = new ArrayList<Project>();
		for (String fileName : listFile) {
			try {
				Project project = getObject(fileName, context,
						new TypeReference<Project>() {
						});
				project.setStore(true);
				result.add(project);
			} catch (ConstellationException e) {
			}
		}

		return result;
	}

	/**
	 * Retourne le flux JSON contenu dans la réponse du Web Service.
	 * 
	 * @param inputStream
	 *            Le flus du Web Service.
	 * @return Le flux JSON.
	 * @throws ConstellationException
	 *             Une erreur s'est lors de la récupération des données.
	 */
	public static String getJSONStream(InputStream inputStream)
			throws ConstellationException {

		try {

			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();

			XMLToJSONHandler xmlToJSONHandler = new XMLToJSONHandler();
			xr.setContentHandler(xmlToJSONHandler);
			xr.parse(new InputSource(inputStream));

			String jsonStream = xmlToJSONHandler.getJsonStream();
			jsonStream = jsonStream.replaceAll("\n", "");

			return jsonStream;

		} catch (ParserConfigurationException e) {
			throw new ConstellationException("Cannot read JSON stream.", e);
		} catch (SAXException e) {
			return null;
		} catch (IOException e) {
			throw new ConstellationException("Cannot read JSON stream.", e);
		}
	}
}
