package com.constellation.core.exception;

/**
 * Exception métier de l'application Constellation.
 * 
 * @author j.varin
 */
public class ConstellationException extends Exception {

	/**
	 * UID de sérialisation
	 */
	private static final long serialVersionUID = -3762568839620735659L;

	/**
	 * Construit une exception qui s'ajoute dans la pile d'exception.
	 */
	public ConstellationException() {
		super();
	}

	/**
	 * Construit une exception qui s'ajoute dans la pile d'exception avec le
	 * message passé en paramètre.
	 * 
	 * @param message
	 *            Détail de l'exception
	 */
	public ConstellationException(String message) {
		super(message);
	}

	/**
	 * Construit une exception qui s'ajoute dans la pile d'exception avec la
	 * cause passée en paramètre.
	 * 
	 * @param throwable
	 *            Cause de l'exception
	 */
	public ConstellationException(Throwable throwable) {
		super(throwable);
	}

	/**
	 * Construit une exception qui s'ajoute dans la pile d'exception avec le
	 * message et la cause passés en paramètre.
	 * 
	 * @param message
	 *            Détail de l'exception
	 * @param throwable
	 *            Cause de l'exception
	 */
	public ConstellationException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
