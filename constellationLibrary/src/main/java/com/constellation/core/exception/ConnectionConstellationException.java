package com.constellation.core.exception;

/**
 * Exception métier de l'application Constellation dûe à une erreur de connexion
 * avec le Web Service.
 * 
 * @author j.varin
 */
public class ConnectionConstellationException extends ConstellationException {

	/**
	 * UID de sérialisation
	 */
	private static final long serialVersionUID = 6555380633553727130L;

	/**
	 * Construit une exception qui s'ajoute dans la pile d'exception.
	 */
	public ConnectionConstellationException() {
		super();
	}

	/**
	 * Construit une exception qui s'ajoute dans la pile d'exception avec le
	 * message passé en paramètre.
	 * 
	 * @param message
	 *            Détail de l'exception
	 */
	public ConnectionConstellationException(String message) {
		super(message);
	}

	/**
	 * Construit une exception qui s'ajoute dans la pile d'exception avec la
	 * cause passée en paramètre.
	 * 
	 * @param throwable
	 *            Cause de l'exception
	 */
	public ConnectionConstellationException(Throwable throwable) {
		super(throwable);
	}

	/**
	 * Construit une exception qui s'ajoute dans la pile d'exception avec le
	 * message et la cause passés en paramètre.
	 * 
	 * @param message
	 *            Détail de l'exception
	 * @param throwable
	 *            Cause de l'exception
	 */
	public ConnectionConstellationException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
