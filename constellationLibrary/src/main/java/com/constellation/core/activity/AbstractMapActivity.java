package com.constellation.core.activity;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;

import com.constellation.core.R;
import com.constellation.core.exception.ConnectionConstellationException;
import com.constellation.core.model.OI;
import com.constellation.core.model.Project;
import com.constellation.core.model.Rubric;
import com.constellation.core.model.Theme;
import com.constellation.core.services.ServiceObserver;
import com.constellation.core.utils.DialogUtils;
import com.constellation.core.view.APIItemizedOverlay;
import com.constellation.core.view.ConstellationOverlay;
import com.constellation.core.view.HeaderLayout;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

/**
 * Activité abstraite qui contient les actions communes à toutes les activités
 * affichant une carte.
 * 
 * @author j.varin
 * 
 */
public class AbstractMapActivity extends MapActivity implements ServiceObserver {

	/**
	 * Le nom du paramètre par lequel on passe le guide.
	 */
	public static final String PROJECT = "project";

	/**
	 * Le nom du paramètre par lequel on passe le thème.
	 */
	public static final String THEME = "theme";

	/**
	 * Le nom du paramètre par lequel on passe la rubrique.
	 */
	public static final String RUBRIC = "rubric";
	
	/**
	 * Le nom du paramètre par lequel on passe le nombre d'OI à afficher.
	 */
	public static final String MAX = "max";
	
	/**
	 * Le nom du paramètre par lequel on passe le nombre d'OI à afficher.
	 */
	public static final String CLOSE = "close";
	
	/**
	 * Le nom du paramètre par lequel on passe la rubrique.
	 */
	public static final String OIs = "ois";

	/**
	 * Projet en cours
	 */
	protected Project project;

	/**
	 * Theme de l'OI
	 */
	protected Theme theme;

	/**
	 * Rubric de l'OI
	 */
	protected Rubric rubric;

	/**
	 * Handler used to post runnables in the main thread
	 */
	private Handler handler;

	/**
	 * Liste des overlay.
	 */
	protected List<Overlay> overlaysList;

	/**
	 * Header de la page.
	 */
	protected HeaderLayout mapHeaderView;

	/**
	 * MapView affichée
	 */
	protected MapView mapView;

	/**
	 * Le controleur de la map.
	 */
	protected MapController mapController;

	/**
	 * Latitude maximum
	 */
	protected int maxLat;

	/**
	 * Latitude minimum
	 */
	protected int minLat;

	/**
	 * Longitude maximum
	 */
	protected int maxLon;

	/**
	 * Longitude minimum
	 */
	protected int minLon;

	/**
	 * Etat de l'activité.
	 */
	protected boolean isSuspendedActivity = false;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (handler == null)
			handler = new Handler();
	}

	/**
	 * Appelée lorsque l'activité démarre. Permet d'initaliser les contrôles.
	 */
	@Override
	protected void onStart() {

		super.onStart();
		this.isSuspendedActivity = false;
	}

	/**
	 * Appelée lorsque l'activité passe en arrière plan; Libérez les écouteurs,
	 * arrêtez les threads, votre activité peut disparaître de la mémoire.
	 */
	@Override
	protected void onStop() {

		this.isSuspendedActivity = true;
		super.onStop();
	}

	/**
	 * Appelée lorsque l'activité sort de son état de veille.
	 */
	@Override
	protected void onRestart() {

		super.onRestart();
		this.isSuspendedActivity = false;
	}

	/**
	 * Appelée lorsque l'activité est suspendue. Stoppez les actions qui
	 * consomment des ressources. L'activité va passer en arrière-plan.
	 */
	@Override
	protected void onPause() {

		this.isSuspendedActivity = true;
		super.onPause();
	}

	/**
	 * Appelée après le démarrage ou une pause. Relancez les opérations arrêtés
	 * (threads). Mettez à jour votre application et vérifiez vos écouteurs.
	 */
	@Override
	protected void onResume() {

		super.onResume();
		this.isSuspendedActivity = false;
	}

	/**
	 * Lance le chargement des données externes.
	 */
	public void loadData() {

		project = (Project) getIntent().getParcelableExtra(PROJECT);
		theme = (Theme) getIntent().getParcelableExtra(THEME);
		rubric = (Rubric) getIntent().getParcelableExtra(RUBRIC);
		displayHeader();

		mapController = mapView.getController();
		overlaysList = mapView.getOverlays();
		overlaysList.clear();
	}

	/**
	 * Renvoie à la page d'accueil.
	 */
	public void returnHome() {

		Activity activity = this;
		Intent intent = new Intent(activity, GuideHomeActivity.class);
		startActivity(intent);
	}

	/**
	 * Gets the handler used to post runnables in the main thread
	 * 
	 * @return the handler
	 */
	public Handler getHandler() {
		return handler;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.player.services.ServiceObserver#dataChanged(java.lang
	 * .Object, java.lang.Integer)
	 */
	@Override
	public void dataChanged(Object o, Integer taskId) {
		return;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.player.services.ServiceObserver#errorTreatment(java
	 * .lang.Throwable, java.lang.Integer)
	 */
	@Override
	public void errorTreatment(Throwable e, Integer taskId) {

		if (!this.isSuspendedActivity) {

			if (e instanceof ConnectionConstellationException) {
				DialogUtils.showConnectionError(this, getHandler());
			} else {
				DialogUtils.showMessageError(this, getHandler());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.android.maps.MapActivity#isRouteDisplayed()
	 */
	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	/**
	 * Initialise l'ensemble des vues contenues dans la page.
	 */
	protected void initView() {

		mapHeaderView = (HeaderLayout) findViewById(R.id.rubrics_header);
		mapView = (MapView) findViewById(R.id.mapView);
		mapView.setBuiltInZoomControls(true);
	}

	/**
	 * Affiche le header de la page.
	 */
	protected void displayHeader() {

		if ((theme.getHeader() != null) && (theme.getHeader().length() > 0)) {
			mapHeaderView.setHeaderUrl(theme.getHeader());
		} else {
			mapHeaderView.setHeaderUrl(project.getHeader());
		}
		mapHeaderView.setHasTitle(true);
		mapHeaderView.setHeaderTitle(theme.getName());
		mapHeaderView.setHeaderTitlePictoUrl(theme.getImg());
	}

	/**
	 * Affiche un Oi sur la carte.
	 * 
	 * @param oi
	 *            L'OI à afficher.
	 */
	protected void displayOI(OI oi) {

		double latitude = oi.getCoordY() * 1000000;
		double longitude = oi.getCoordX() * 1000000;

		// Données pour calculer le centre et le zoom de la mapView
		maxLat = (int) Math.max(latitude, maxLat);
		minLat = (int) Math.min(latitude, minLat);
		maxLon = (int) Math.max(longitude, maxLon);
		minLon = (int) Math.min(longitude, minLon);

		GeoPoint detailPoint = new GeoPoint((int) latitude, (int) longitude);
		ConstellationOverlay overlayitem = new ConstellationOverlay(
				detailPoint, rubric.getTitle(), oi.getTitle());
		overlayitem.setProject(project);
		overlayitem.setTheme(theme);
		overlayitem.setRubric(rubric);
		overlayitem.setOi(oi);
		Drawable pictoMap = this.getResources().getDrawable(
				getPicto(oi.getIdType().toUpperCase()));
		APIItemizedOverlay itemizedOverlay = new APIItemizedOverlay(pictoMap,
				getBaseContext());
		itemizedOverlay.addOverlay(overlayitem);

		overlaysList.add(itemizedOverlay);
	}

	/**
	 * Retourne l'identifiant du picto à afficher.
	 * 
	 * @param typeOI
	 *            Le type de l'OI.
	 * @return L'identifiant du picto.
	 */
	protected int getPicto(String typeOI) {

		Log.d(getClass().getCanonicalName(), "Type OI : " + typeOI);
		if (typeOI.equals(getString(R.string.id_actualite))) {
			return R.drawable.map_actualites;
		}
		if (typeOI.equals(getString(R.string.id_brochure))) {
			return R.drawable.map_brochures;
		}
		if (typeOI.equals(getString(R.string.id_camping))) {
			return R.drawable.map_campings;
		}
		if (typeOI.equals(getString(R.string.id_commerce))) {
			return R.drawable.map_commerces_services;
		}
		if (typeOI.equals(getString(R.string.id_degustation))) {
			return R.drawable.map_degustation;
		}
		if (typeOI.equals(getString(R.string.id_fete))) {
			return R.drawable.map_fetes_manifestations;
		}
		if (typeOI.equals(getString(R.string.id_grand_espace))) {
			return R.drawable.map_grands_espaces;
		}
		if (typeOI.equals(getString(R.string.id_hotel))) {
			return R.drawable.map_hotels;
		}
		if (typeOI.equals(getString(R.string.id_itineraire))) {
			return R.drawable.map_itineraires;
		}
		if (typeOI.equals(getString(R.string.id_locatif))) {
			return R.drawable.map_locatif;
		}
		if (typeOI.equals(getString(R.string.id_loisir_culturel))) {
			return R.drawable.map_loisirs_culturels;
		}
		if (typeOI.equals(getString(R.string.id_loisir_sportif))) {
			return R.drawable.map_loisirs_sportifs;
		}
		if (typeOI.equals(getString(R.string.id_monument))) {
			return R.drawable.map_monuments_patrimoine;
		}
		if (typeOI.equals(getString(R.string.id_organisme))) {
			return R.drawable.map_organismes;
		}
		if (typeOI.equals(getString(R.string.id_point_interet))) {
			return R.drawable.map_poi;
		}
		if (typeOI.equals(getString(R.string.id_restaurant))) {
			return R.drawable.map_restaurants;
		}
		if (typeOI.equals(getString(R.string.id_sejour))) {
			return R.drawable.map_sejours;
		}
		if (typeOI.equals(getString(R.string.id_seminaire))) {
			return R.drawable.map_seminaires;
		}
		if (typeOI.equals(getString(R.string.id_stage))) {
			return R.drawable.map_stages;
		}
		if (typeOI.equals(getString(R.string.id_transport))) {
			return R.drawable.map_transports;
		}
		if (typeOI.equals(getString(R.string.id_village_vacances))) {
			return R.drawable.map_villages_vacances;
		}
		return R.drawable.map_defaut;
	}
}
