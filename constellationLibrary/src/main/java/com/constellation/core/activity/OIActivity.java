package com.constellation.core.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.constellation.core.R;
import com.constellation.core.adapter.ImageAdapter;
import com.constellation.core.manager.ConstellationManager;
import com.constellation.core.model.EmbeddedMedia;
import com.constellation.core.model.OI;
import com.constellation.core.model.Project;
import com.constellation.core.model.Rubric;
import com.constellation.core.model.Template;
import com.constellation.core.model.Theme;
import com.constellation.core.utils.DialogUtils;
import com.constellation.core.utils.FilterUtils;
import com.constellation.core.view.ConstellationGallery;
import com.constellation.core.view.HeaderLayout;

/**
 * Cette activité permet d'afficher la liste des OIs d'une rubrique.
 * 
 * Les paramètres d'entrée sont : PROJECT Le guide THEME Le theme RUBRIC La
 * rubrique POSITION La position de l'OI à afficher
 * 
 * @author j.varin, j.attal
 * 
 */
public class OIActivity extends AbstractActivity {

	/**
	 * Le nom du paramètre par lequel on passe le guide.
	 */
	public static final String PROJECT = "project";

	/**
	 * Le nom du paramètre par lequel on passe le thème.
	 */
	public static final String THEME = "theme";

	/**
	 * Le nom du paramètre par lequel on passe la rubrique.
	 */
	public static final String RUBRIC = "rubric";

	/**
	 * Le nom du paramètre par lequel on passe la rubrique.
	 */
	public static final String POSITION = "position";

	/**
	 * Le nom du paramètre par lequel on passe l'OI.
	 */
	public static final String OI = "oi";

	/**
	 * Le nom du paramètre par lequel on passe la liste des OIs proches.
	 */
	public static final String OIs = "ois";

	/**
	 * Identifiant de la tâche de récupération des OI.
	 */
	protected Integer taskIdOI;

	/**
	 * Début de la requête de tinyUrl
	 */
	public static String TINYURL = "http://tinyurl.com/api-create.php?url=";

	/**
	 * Header de la page.
	 */
	private HeaderLayout header;
	/**
	 * Le guide à afficher.
	 */
	private Project project;
	/**
	 * Le thème à afficher.
	 */
	private Theme theme;
	/**
	 * La rubrique à afficher.
	 */
	private Rubric rubric;
	/**
	 * Position de l'OI de départ
	 */
	private int position;
	/**
	 * OI à afficher quand il vient de la carte
	 */
	private OI oi;
	private List<OI> ois;
	/**
	 * Gallery des médias
	 */
	private ConstellationGallery galleryMedias;
	/**
	 * Gestionnaire des données contellation.
	 */
	protected ConstellationManager constellationManager;
	/**
	 * Compteur sous la Gallery
	 */
	private TextView count;
	/**
	 * TextView du titre de l'OI
	 */
	private TextView textViewOI;
	/**
	 * TextView de l'adresse
	 */
	private TextView textViewAdd;
	/**
	 * TextView des moyens de communication
	 */
	private TextView textViewCom;
	/**
	 * RatingBar
	 */
	private RatingBar starsBar;
	/**
	 * Fond de la page.
	 */
	private LinearLayout fond;
	/**
	 * Flèche gauche
	 */
	private ImageView leftArrow;
	/**
	 * Flèche droite
	 */
	private ImageView rightArrow;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.oi);
		initView();

		constellationManager = ConstellationManager
				.getConstellationManager(this);
		constellationManager.addListener(this);

		loadData();
	}

	private void initView() {

		fond = (LinearLayout) findViewById(R.id.main);
		header = (HeaderLayout) findViewById(R.id.oi_header);
		leftArrow = (ImageView) findViewById(R.id.header_arrow_left);
		rightArrow = (ImageView) findViewById(R.id.header_arrow_right);
		textViewOI = (TextView) findViewById(R.id.title);
		starsBar = (RatingBar) findViewById(R.id.stars);
		galleryMedias = (ConstellationGallery) findViewById(R.id.gallery);
		count = (TextView) findViewById(R.id.count);
		textViewAdd = (TextView) findViewById(R.id.oi_address);
		textViewCom = (TextView) findViewById(R.id.oi_description);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractActivity#loadData()
	 */
	@Override
	public void loadData() {
		// Récupération des données

		position = getIntent().getIntExtra(OIActivity.POSITION, -1);
		project = (Project) getIntent().getParcelableExtra(OIActivity.PROJECT);
		theme = (Theme) getIntent().getParcelableExtra(OIActivity.THEME);
		rubric = (Rubric) getIntent().getParcelableExtra(OIActivity.RUBRIC);
		ois = getIntent().getParcelableArrayListExtra(OIActivity.OIs);

		displayHeader();
		displayBackground();
		loadOI();
	}

	/**
	 * Affiche le header
	 */
	public void displayHeader() {
		if ((theme.getHeader() != null) && (!theme.getHeader().equals(""))) {
			header.setHeaderUrl(theme.getHeader());
		} else if ((project.getHeader() != null)
				&& (!project.getHeader().equals(""))) {
			header.setHeaderUrl(project.getHeader());
		}
		header.setHasTitle(true);
		header.setHeaderTitle(rubric.getTitle());
		header.setHeaderTitlePictoUrl(theme.getImg());
	}

	/**
	 * Affiche le fond
	 */
	public void displayBackground() {
		if (project.getTemplate() != null) {
			Template tmp = project.getTemplate();
			if (tmp.equals(Template.Orange)) {
				fond.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_orange));
			} else if (tmp.equals(Template.Blue)) {
				fond.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_blue));
			} else {
				fond.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_green));
			}
		}
	}

	public void loadOI() {

		// Charge un OI
		if (position == -1) {

			oi = (OI) getIntent().getParcelableExtra(OIActivity.OI);
			displayOI();
		} else {
			if (ois != null) {
				oi = (OI) ois.get(position);
				displayOI();
			} else {
				taskIdOI = constellationManager.getOI(rubric.getId(),
						project.getPlaylistId(), project.getEctId(), position);
			}
		}
	}

	@Override
	public void dataChanged(final Object o, final Integer taskId) {

		getHandler().post(new Runnable() {

			public void run() {
				Log.d(getClass().getCanonicalName(), "Data Changed");

				if (taskId.equals(taskIdOI)) {

					oi = (OI) o;
					displayOI();
				}
			}
		});
	}

	/**
	 * Affiche un OI
	 */
	public void displayOI() {

		if (oi == null) {
			DialogUtils.showMessageError(this, getHandler());
		}

		displayStars();

		// Affichage du titre
		textViewOI.setText(oi.getTitle());

		// Adresse
		String address = getAddress();
		textViewAdd.setText(address);

		// Affichage des moyens de communication
		String communication = getCom();
		textViewCom.setText(communication);

		arrowsAction();
		displayGallery();
	}

	/**
	 * Affiche les étoiles
	 */
	private void displayStars() {
		int nbStars = 0;
		if (oi != null) {
			if ((oi.getClassOfficiels() != null)
					&& ((oi.getClassOfficiels().size() > 0))) {
				nbStars = FilterUtils.getScore(oi.getClassOfficiels().get(0)
						.getCode());
			} else if ((oi.getClassLabels() != null)
					&& ((oi.getClassLabels().size() > 0))) {
				nbStars = FilterUtils.getScore(oi.getClassLabels().get(0)
						.getCode());
			}
		}

		if (nbStars > 0) {

			starsBar.setVisibility(View.VISIBLE);
			starsBar.setNumStars(nbStars);
			starsBar.setRating(Float.intBitsToFloat(nbStars));
		} else {
			starsBar.setVisibility(View.GONE);
		}
	}

	/**
	 * Récupère l'adresse de l'OI
	 * 
	 * @return
	 */
	private String getAddress() {
		// Address0
		String address = oi.getAddress().getAddress0();
		if (!oi.getAddress().getAddress0().equals(""))
			address += " ";
		// ComplementAddress
		address += oi.getAddress().getComplementAddress();
		if (!oi.getAddress().getComplementAddress().equals(""))
			address += "\n";
		// LibelleVoie
		address += oi.getAddress().getLibelleVoie();
		if (!oi.getAddress().getLibelleVoie().equals(""))
			address += "\n";
		// DistributionSpeciale
		address += oi.getAddress().getDistributionSpeciale();
		if (!oi.getAddress().getDistributionSpeciale().equals(""))
			address += "\n";
		// CodePostal
		address += oi.getAddress().getCodePostal();
		if (!oi.getAddress().getCodePostal().equals(""))
			address += " ";
		// Commune
		address += oi.getAddress().getCommune();

		if (!address.equals(""))
			address += "\n";
		return address;
	}

	/**
	 * Récupère les moyens de communication de l'OI
	 * 
	 * @return
	 */
	private String getCom() {
		String communication = "";
		for (int i = 0; i < oi.getMoyensCom().size(); i++) {
			communication += oi.getMoyensCom().get(i).getType() + " : "
					+ oi.getMoyensCom().get(i).getCoord() + "\n";
		}
		return communication;
	}

	/**
	 * Gère les actions des flèches
	 */
	private void arrowsAction() {
		if (position != 0) {
			leftArrow.setVisibility(View.VISIBLE);
		} else {
			leftArrow.setVisibility(View.INVISIBLE);
		}
		int max = rubric.getTotalOIs() - 1;
		if (ois != null) {
			max = ois.size() - 1;
		}
		if (position < max) {
			rightArrow.setVisibility(View.VISIBLE);
		} else {
			rightArrow.setVisibility(View.INVISIBLE);
		}
		if (position == -1) {
			leftArrow.setVisibility(View.GONE);
			rightArrow.setVisibility(View.GONE);
		}
		leftArrow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				position--;
				loadOI();
			}
		});
		rightArrow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				position++;
				loadOI();
			}
		});
	}

	/**
	 * Affiche la Gallery
	 */
	private void displayGallery() {
		// Récupération de la hauteur de l'écran
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int ht = displaymetrics.heightPixels;

		// Chargement de la Gallery
		final ImageAdapter imageAdapter = new ImageAdapter(this, getHandler(), ht,
				getResources().getDrawable(R.drawable.gallery_fond), oi);
		galleryMedias.setAdapter(imageAdapter);
		galleryMedias.setSpacing(20);
		if (oi.getMedias().size() == 0) {
			galleryMedias.setVisibility(View.GONE);
			count.setVisibility(View.GONE);
		} else {
			galleryMedias.setVisibility(View.VISIBLE);
			count.setVisibility(View.VISIBLE);
		}
		// Mise à jour du compteur
		galleryMedias.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View v,
					int position, long id) {
				int cpt = position + 1;
				count.setText(cpt + "/" + imageAdapter.getCount());
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		
		galleryMedias.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				
				if (imageAdapter.getItem(position) instanceof EmbeddedMedia) {
					
					EmbeddedMedia embeddedMedia = (EmbeddedMedia) imageAdapter.getItem(position);
					String code = embeddedMedia.getCode();
					int index = code.indexOf("href");
					if (index == -1) {
						index = code.indexOf("src");
					}
					boolean startWritting = false;
					StringBuilder url = null;
					for (int i = index ; i < code.length() ; i++) {
						
						if (code.charAt(i) == '\"') {
							startWritting = !startWritting;
							if (url == null) {
								
								url = new StringBuilder();
								continue;
							}
							else {
								break;
							}
						}
						
						if (startWritting) {
							url.append(code.charAt(i));
						}
					}
					
				    Intent i = new Intent(Intent.ACTION_VIEW);  
				    i.setData(Uri.parse(url.toString()));  
				    startActivity(i); 
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_oi, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPrepareOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean isActiveAroundMe = project.isActiveArroundMe();
		boolean isActiveSocialNetworks = project.isActiveSocialNetworks()
				&& (project.getUrlForShare() != null)
				&& (!project.getUrlForShare().equals(""));

		if (!isActiveAroundMe) {
			menu.removeItem(R.id.menu_geoloc);
		}
		if (!isActiveSocialNetworks) {
			menu.removeItem(R.id.menu_share);
		}
		return super.onPrepareOptionsMenu(menu);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.home) {
			returnHome();
		} else if (item.getItemId() == R.id.menu_go_there) {
			Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
					Uri.parse("http://maps.google.com/maps?daddr="
							+ oi.getCoordY() + "," + oi.getCoordX()));
			startActivity(intent);
		} else if (item.getItemId() == R.id.menu_see_map) {
			Intent myMapIntent = new Intent(getBaseContext(),
					DetailMapActivity.class);
			myMapIntent.putExtra(DetailMapActivity.PROJECT, project);
			myMapIntent.putExtra(DetailMapActivity.THEME, theme);
			myMapIntent.putExtra(DetailMapActivity.RUBRIC, rubric);
			myMapIntent.putExtra(DetailMapActivity.OI, oi);
			startActivity(myMapIntent);
		} else if (item.getItemId() == R.id.menu_details) {
			Intent myIntent = new Intent(getBaseContext(),
					OIDetailActivity.class);
			myIntent.putExtra(OIDetailActivity.OI, oi);
			myIntent.putExtra(OIDetailActivity.RUBRIC, rubric);
			myIntent.putExtra(OIDetailActivity.THEME, theme);
			myIntent.putExtra(OIDetailActivity.PROJECT, project);
			startActivity(myIntent);
		} else if (item.getItemId() == R.id.menu_geoloc) {
			Intent myGeoIntent = new Intent(getBaseContext(),
					CloseOIsActivity.class);
			myGeoIntent.putExtra(CloseOIsActivity.PROJECT, project);
			myGeoIntent.putExtra(CloseOIsActivity.THEME, theme);
			myGeoIntent.putExtra(CloseOIsActivity.OI, oi);
			myGeoIntent.putExtra(CloseOIsActivity.RUBRIC, rubric);
			startActivity(myGeoIntent);
		} else if (item.getItemId() == R.id.menu_share) {
			final Intent MessIntent = new Intent(Intent.ACTION_SEND);
			MessIntent.setType("text/plain");
			MessIntent.putExtra(Intent.EXTRA_SUBJECT,
					getString(R.string.email_subject));
			String urlToShare = "";
			try {
				urlToShare = shorter(project.getUrlForShare() + oi.getId());
			} catch (IOException e) {
				e.printStackTrace();
			}
			MessIntent.putExtra(Intent.EXTRA_TEXT, urlToShare);
			OIActivity.this.startActivity(Intent.createChooser(MessIntent,
					getString(R.string.share_with)));
		} else {
		}
		return super.onOptionsItemSelected(item);
	}

	public static String shorter(String url) throws IOException {
		String tinyUrl = TINYURL + url;
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new URL(tinyUrl).openStream()));
		tinyUrl = reader.readLine();

		return tinyUrl;
	}
}
