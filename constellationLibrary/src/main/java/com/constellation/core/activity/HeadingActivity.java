package com.constellation.core.activity;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.constellation.core.R;
import com.constellation.core.adapter.RubricAdapter;
import com.constellation.core.adapter.RubricIconAdapter;
import com.constellation.core.manager.ConstellationManager;
import com.constellation.core.model.Display;
import com.constellation.core.model.Project;
import com.constellation.core.model.Rubric;
import com.constellation.core.model.Template;
import com.constellation.core.model.Theme;
import com.constellation.core.view.HeaderLayout;

/**
 * Cette activité permet d'afficher la liste des rubriques d'un thème.
 * 
 * Les paramètres d'entrée sont : PROJECT Le guide THEME le theme IS_HOME_PAGE
 * Etat de la page.
 * 
 * @author j.varin, j.attal
 * 
 */
public class HeadingActivity extends AbstractActivity {

	/**
	 * Le nom du paramètre par lequel on passe le guide.
	 */
	public static final String PROJECT = "project";

	/**
	 * Le nom du paramètre par lequel on passe le thème.
	 */
	public static final String THEME = "theme";

	/**
	 * Le nom du paramètre par lequel on passe l'état de la page.
	 */
	public static final String IS_HOME_PAGE = "isHomePage";

	/**
	 * Le guide Ã  afficher.
	 */
	private Project project;

	/**
	 * Le thème du guide Ã  afficher.
	 */
	private Theme theme;

	/**
	 * Indique si la page est la Home Page.
	 */
	private boolean isHomePage;

	/**
	 * Header de la page.
	 */
	private HeaderLayout rubricsHeaderView;

	/**
	 * Vue de la description du thème.
	 */
	private TextView rubricsDescTextView;

	/**
	 * Vue de la liste de rubriques.
	 */
	private ListView rubricsListView;
	
	/**
	 * Vue de la grille de rubriques.
	 */
	private GridView rubricsGridView;
	
	/**
	 * Fond de la page.
	 */
	private LinearLayout background;
	
	/**
	 * Manière d'afficher les rubriques
	 */
	private Display display;
	
	/**
	 * Identifiant de la tache de téléchargement du guide.
	 */
	private Integer downloadTaskId;
	
	/**
	 * Gestionnaire des données contellation.
	 */
	private ConstellationManager constellationManager;
	
	/**
	 * Dialog de téléchargement
	 */
	private Dialog downloadingDialog;
	
	/**
	 * Dialog de téléchargement
	 */
	private ProgressBar progressDownloading;
	/**
	 * Dialog de téléchargement
	 */
	private int nbRubrics;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		loadData();
	}

	/**
	 * Initialise l'ensemble des vues contenues dans la page.
	 */
	private void initView() {
		
		background = (LinearLayout) findViewById(R.id.main);
		rubricsHeaderView = (HeaderLayout) findViewById(R.id.rubrics_header);
		rubricsDescTextView = (TextView) findViewById(R.id.rubrics_desc);
	}
	
	/**
	 * Initialise la vue de la liste de rubrique.
	 */
	private void initListView() {
		
		rubricsListView = (ListView) findViewById(R.id.rubrics_list);
		rubricsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				Intent intent = new Intent(getBaseContext(),
						OIsActivity.class);
				intent.putExtra(OIsActivity.PROJECT, project);
				intent.putExtra(OIsActivity.THEME, theme);
				intent.putExtra(OIsActivity.RUBRIC, theme.getRubrics().get(position));
				intent.putExtra("filterScore",-1);
				intent.putExtra("filterDateFrom",-1);
				intent.putExtra("filterDateTo",-1);
				startActivity(intent);
			}
		});
	}
	
	/**
	 * Initialise la vue de la grille de rubriques.
	 */
	private void initGridView() {
		
		rubricsGridView = (GridView) findViewById(R.id.rubrics_grid);
		rubricsGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				Intent intent = new Intent(getBaseContext(),
						OIsActivity.class);
				intent.putExtra(OIsActivity.PROJECT, project);
				intent.putExtra(OIsActivity.THEME, theme);
				intent.putExtra(OIsActivity.RUBRIC, theme.getRubrics().get(position));
				intent.putExtra("filterScore",-1);
				intent.putExtra("filterDateFrom",-1);
				intent.putExtra("filterDateTo",-1);
				startActivity(intent);
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractActivity#loadData()
	 */
	@Override
	public void loadData() {

		project = (Project) getIntent().getParcelableExtra(PROJECT);
		theme = (Theme) getIntent().getParcelableExtra(THEME);
		isHomePage = getIntent().getBooleanExtra(IS_HOME_PAGE, false);
		
		//Log.d(getClass().getCanonicalName(), "Thème : " + theme.getName());
		display = theme.getDisplay();
		if (display.equals(Display.Liste)){
			
			setContentView(R.layout.rubrics);
			initView();
			initListView();
		}
		else{
			setContentView(R.layout.rubrics_icons);
			initView();
			initGridView();
		}
		
		displayHeader();
		displayDesc();
		displayRubrics();
	}

	/**
	 * Affiche le header.
	 */
	private void displayHeader() {

		if ((theme.getHeader() != null) && (theme.getHeader().length() > 0)) {
			rubricsHeaderView.setHeaderUrl(theme.getHeader());
		} else if ((project.getHeader() != null) && (project.getHeader().length() > 0)) {
			rubricsHeaderView.setHeaderUrl(project.getHeader());
		}
		rubricsHeaderView.setHasTitle(true);
		rubricsHeaderView.setHeaderTitle(theme.getName());
		rubricsHeaderView.setHeaderTitlePictoUrl(theme.getImg());
		// Template
		if (project.getTemplate() != null)
		{
			Template tmp = project.getTemplate();
			if (tmp.equals(Template.Orange)){
				background.setBackgroundDrawable(getResources().getDrawable(R.drawable.back_orange));
				if (display.equals(Display.Liste)){
					rubricsListView.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_back_orange));
				}
			}
			else if (tmp.equals(Template.Blue)){
				background.setBackgroundDrawable(getResources().getDrawable(R.drawable.back_blue));
				if (display.equals(Display.Liste)){
					rubricsListView.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_back_blue));
				}
			}
			else{
				background.setBackgroundDrawable(getResources().getDrawable(R.drawable.back_green));
				if (display.equals(Display.Liste)){
					rubricsListView.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_back_green));
				}
			}
		}
	}

	/**
	 * Affiche la description.
	 */
	private void displayDesc() {

		String description = theme.getDesc();
		if ((description == null) || (description.length() == 0)) {
			rubricsDescTextView.setVisibility(View.GONE);
		} else {
			rubricsDescTextView.setText(description);
		}
	}

	/**
	 * Affiche la liste des rubriques.
	 */
	private void displayRubrics() {
		if (display.equals(Display.Liste)){
			RubricAdapter rubricAdapter = new RubricAdapter(getBaseContext(),
					theme.getRubrics(), getHandler());
			
			rubricsListView.setAdapter(rubricAdapter);
		}
		else{
			RubricIconAdapter rubricAdapter = new RubricIconAdapter(getBaseContext(),
					(ArrayList<Rubric>) theme.getRubrics(), getHandler());
			
			rubricsGridView.setAdapter(rubricAdapter);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		if (isHomePage) {

			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.menu_home, menu);
		}
		return true;
	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (getString(R.string.standalone).equals("true")) {
			menu.removeItem(R.id.menu_download);
		}
		return super.onPrepareOptionsMenu(menu);
	}

	/*
	 * (non-Javadoc)
	 *  
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.menu_legal_info) {
			Dialog dialog = new Dialog(HeadingActivity.this);
			dialog.setContentView(R.layout.copyright);
			dialog.setTitle(R.string.copyright_title);
			dialog.show();
			return true;
		} else if (item.getItemId() == R.id.menu_download) {
			downloadProject();
			return true;
		} else {
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void downloadProject() {
		nbRubrics = 0;
		for (Theme theme : project.getThemes()) {
			if (theme.getRubrics() != null) {
				for (@SuppressWarnings("unused") Rubric rubric : theme.getRubrics()) {
					nbRubrics++;
				}
			}
		}
		
		constellationManager = ConstellationManager.getConstellationManager(this);
		constellationManager.addListener(this);
		downloadTaskId = constellationManager.downloadProject(project.getId());
		downloadingDialog = new Dialog(HeadingActivity.this);
		downloadingDialog.setTitle(R.string.downloading);
		downloadingDialog.setContentView(R.layout.downloading);
		downloadingDialog.show();
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.core.activity.AbstractActivity#dataChanged(java.lang
	 * .Object, java.lang.Integer)
	 */
	@Override
	public void dataChanged(final Object o, final Integer taskId) {

		getHandler().post(new Runnable() {
			@Override
			public void run() {

				Log.d(getClass().getCanonicalName(), "Data changed");
				if (taskId.equals(downloadTaskId)) {
					if (o instanceof Project) {
						Project p = (Project) o;
						Log.d(getClass().getSimpleName(), "Got Project "+p.getTitle());
						progressDownloading = (ProgressBar) downloadingDialog.findViewById(R.id.progressBar);
						progressDownloading.setProgress(10);
							
					}
					if (o instanceof String) {
						Log.d(getClass().getSimpleName(), "Got Progress");
						String progress = (String) o;
						
						if (progress.equals("end")) {
							progressDownloading.setProgress(100);
							Toast.makeText(getBaseContext(),getString(R.string.downloading_end),5000);
							Log.d(getClass().getSimpleName(), "END");
							downloadingDialog.dismiss();
						} else {
							int cpt = Integer.parseInt(progress);
							progressDownloading.setProgress(10+(90/nbRubrics)*cpt);
						}
					}
				}
			}
		});
	}
}
