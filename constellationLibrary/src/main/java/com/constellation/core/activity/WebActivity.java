package com.constellation.core.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.constellation.core.R;
import com.constellation.core.model.Project;
import com.constellation.core.model.Template;
import com.constellation.core.model.Theme;
import com.constellation.core.view.HeaderLayout;

public class WebActivity extends AbstractActivity {
	
	/**
	 * Le nom du paramètre par lequel on passe le guide.
	 */
	public static final String PROJECT = "project";

	/**
	 * Le nom du paramètre par lequel on passe le thème.
	 */
	public static final String THEME = "theme";

	/**
	 * Fond de la page.
	 */
	private LinearLayout background;
	
	/**
	 * Header de la page.
	 */
	private HeaderLayout header;

	/**
	 * Webview
	 */
	private WebView webview;
	
	/**
	 * Le guide à afficher.
	 */
	private Project project;

	/**
	 * Le thème du guide à afficher.
	 */
	private Theme theme;
	
	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
        
        initView();
        loadData();
    }
    private void initView() {
		
		background = (LinearLayout) findViewById(R.id.main);
		header = (HeaderLayout) findViewById(R.id.web_header);
		webview = (WebView) findViewById(R.id.webview); 
	}
    public void loadData() {
    	project = (Project) getIntent().getParcelableExtra(PROJECT);
		theme = (Theme) getIntent().getParcelableExtra(THEME);
		String url = "";
		if (!url.contains("http://")){
			url += "http://";
		}
		url	+= getIntent().getStringExtra("url");
		Log.d(getClass().getSimpleName(), "Url : " + url);
		if (project.getHeader()!=null) {
			header.setHeaderUrl(project.getHeader());
		}
		
        webview.loadUrl(url);
//        webview.setWebViewClient(new WebViewClient() {  
//	        @Override  
//	        public boolean shouldOverrideUrlLoading(WebView view, String url)  
//	        {  
//	        	view.loadUrl(url);
//	        	return false;
//	        }  
//        });  
	}
    
    public void displayBackground() {
	    if (project.getTemplate() != null) {
			Template tmp = project.getTemplate();
			if (tmp.equals(Template.Orange)) {
				background.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_orange));
			} else if (tmp.equals(Template.Blue)) {
	
				background.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_blue));
			} else {
				background.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_green));
			}
			header.setBlur(tmp);
		}
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_back_home, menu);
        return true;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
            webview.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.home) {
			returnHome();
		} else {
		}
        return super.onOptionsItemSelected(item);
    }
}
