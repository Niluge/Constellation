package com.constellation.core.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.constellation.core.R;
import com.constellation.core.manager.ConstellationManager;
import com.constellation.core.model.Project;
import com.constellation.core.utils.DialogUtils;
import com.constellation.core.view.DistantImageView;

/**
 * Cette activité permet l'affichage du splashscreen du guide et la redirection
 * vers la Home page adaptée.
 * 
 * @author j.varin, j.attal
 * 
 */
public class GuideHomeActivity extends AbstractActivity {

	/**
	 * Le nom du paramètre par lequel on passe l'identifiant guide.
	 */
	public static final String PROJECT_ID = "project_id";

	/**
	 * Le nom du paramètre par lequel on passe le splash Screen
	 */
	public static final String SPLASHSCREEN = "splash";

	/**
	 * Le projet en cours.
	 */
	private Project project;

	/**
	 * Identifiant de la tache de récupération du guide.
	 */
	private Integer guideHomeTaskId;

	/**
	 * Gestionnaire des données contellation.
	 */
	private ConstellationManager constellationManager;

	/**
	 * Etat de progression du chargement.
	 */
	private boolean isLaunched = false;

	/**
	 * L'identifiant du projet courant.
	 */
	private String projectId;

	/**
	 * Date de lancement du chargement de la Home.
	 */
	private long startDate;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		resetHistory();
		initView();

		constellationManager = ConstellationManager.getConstellationManager(this);
		constellationManager.addListener(this);

		loadData();
	}

	/**
	 * Initialise l'ensemble des vues contenues dans la page.
	 */
	public void initView() {

		projectId = getIntent().getStringExtra(PROJECT_ID);
		if (projectId == null) {
			projectId = getString(R.string.id_project);
		}

		startDate = System.currentTimeMillis();

		String splashscreen = getIntent().getStringExtra(SPLASHSCREEN);
		if ((splashscreen != null) && (splashscreen.length() > 0)) {

			DistantImageView splash = (DistantImageView) findViewById(R.id.splashImage);
			splash.setHandler(getHandler());
			splash.setUrl(splashscreen);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractActivity#loadData()
	 */
	@Override
	public void loadData() {

		guideHomeTaskId = constellationManager.getProjectById(projectId);
		Log.d(getClass().getCanonicalName(), "Load data : " + guideHomeTaskId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.core.activity.AbstractActivity#dataChanged(java.lang
	 * .Object, java.lang.Integer)
	 */
	@Override
	public void dataChanged(final Object o, final Integer taskId) {

		getHandler().post(new Runnable() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {

				Log.d(getClass().getCanonicalName(), "Data changed");
				if (taskId.equals(guideHomeTaskId)) {

					if (o instanceof Project) {

						project = (Project) o;
						displayHome();
					}
				}
			}
		});
	}

	/**
	 * Redirige l'application vers la Home page adaptée.
	 */
	protected void displayHome() {

		int timeToWait = getIntent().getIntExtra("timeToWait", 3000);
		if (!isLaunched) {

			Log.d("Time", "" + System.currentTimeMillis());

			if (System.currentTimeMillis() - startDate > timeToWait) {

				isLaunched = true;
				int nbThemes = project.getThemes().size();
				Log.d(getClass().getCanonicalName(), "Themes : " + nbThemes);

				if (nbThemes > 1) {

					Intent myIntent = new Intent(getBaseContext(),
							ThemeActivity.class);
					myIntent.putExtra(ThemeActivity.PROJECT, project);
					myIntent.putExtra(ThemeActivity.IS_HOME_PAGE, true);
					startActivity(myIntent);
				} else {

					if (nbThemes == 1) {

						Intent headingIntent = new Intent(getBaseContext(),
								HeadingActivity.class);
						headingIntent
								.putExtra(HeadingActivity.PROJECT, project);
						headingIntent.putExtra(HeadingActivity.THEME, project
								.getThemes().get(0));
						headingIntent.putExtra(HeadingActivity.IS_HOME_PAGE,
								true);
						startActivity(headingIntent);
					} else {
						DialogUtils.showMessageError(this, getHandler());
					}
				}
			} else {

				Handler h = new Handler();
				Runnable myrunnable = new Runnable() {
					public void run() {
						displayHome();
					};
				};
				h.postDelayed(myrunnable, 500);
			}
		}
	}

	/**
	 * Appelée lorsque l'activité est suspendue. Stoppez les actions qui
	 * consomment des ressources. L'activité va passer en arrière-plan.
	 */
	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

	/**
	 * Remise à zéro de l'historique de navigation.
	 */
	public void resetHistory() {

		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction(AbstractActivity.ACTION_HOME_ACCESS);
		sendBroadcast(broadcastIntent);
	}
}
