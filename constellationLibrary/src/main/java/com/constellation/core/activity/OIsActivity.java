package com.constellation.core.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.constellation.core.R;
import com.constellation.core.adapter.OIAdapter;
import com.constellation.core.manager.ConstellationManager;
import com.constellation.core.model.OI;
import com.constellation.core.model.Project;
import com.constellation.core.model.Rubric;
import com.constellation.core.model.RubricType;
import com.constellation.core.model.Template;
import com.constellation.core.model.Theme;
import com.constellation.core.utils.FilterUtils;
import com.constellation.core.view.HeaderLayout;
import com.constellation.core.view.SearchDialog;

/**
 * Cette activité permet d'afficher la liste des OIs d'une rubrique.
 * 
 * Les paramètres d'entrée sont : PROJECT Le guide THEME Le theme RUBRIC La
 * rubrique FILTERSCORE Le filtre par étoiles FILTERFROM Le filtre par date de
 * début FILTERTO Le filtre par date de fin
 * 
 * @author j.varin, j.attal
 * 
 */
public class OIsActivity extends AbstractActivity {

	/**
	 * Le nom du paramètre par lequel on passe le guide.
	 */
	public static final String PROJECT = "project";

	/**
	 * Le nom du paramètre par lequel on passe le thème.
	 */
	public static final String THEME = "theme";

	/**
	 * Le nom du paramètre par lequel on passe la rubrique.
	 */
	public static final String RUBRIC = "rubric";

	public static final String FILTERSCORE = "filterScore";

	public static final String FILTERFROM = "filterDateFrom";

	public static final String FILTERTO = "filterDateTo";

	public RubricType filterType = null;

	/**
	 * Gestionnaire des données contellation.
	 */
	protected ConstellationManager constellationManager;

	/**
	 * Le guide à afficher.
	 */
	protected Project project;

	/**
	 * Le thème du guide à afficher.
	 */
	protected Theme theme;

	/**
	 * La rubrique à afficher.
	 */
	protected Rubric rubric;

	/**
	 * La rubrique en cours
	 */
	protected Rubric currentRubric;

	/**
	 * Identifiant de la tâche de récupération des OIs.
	 */
	protected Integer taskIdOIs;

	/**
	 * Header de la page.
	 */
	private HeaderLayout rubricsHeaderView;

	/**
	 * Vue de la liste de OIs.
	 */
	private ListView oisListView;

	/**
	 * Fond de la page.
	 */
	private LinearLayout back;

	/**
	 * Disponibilité du menu;
	 */
	private boolean displayMenu = false;

	/**
	 * Menu
	 */
	private Menu menuOIs;

	/**
	 * Dialog pour affiner la recherche
	 */
	private Dialog searchDialog;

	/**
	 * Filtre par étoiles
	 */
	protected int filterScore;

	/**
	 * Filtre date de début
	 */
	protected long filterDateFrom;

	/**
	 * Filtre date de fin
	 */
	protected long filterDateTo;

	/**
	 * Compteur du nombre d'OIs affichés
	 */
	private int cpt;

	/**
	 * Adapter de la liste d'OIS
	 */
	protected OIAdapter oiAdapter;

	/**
	 * ProgressBar lorsqu'il n'y a pas encore d'OIs affichés
	 */
	private ProgressBar progress;

	/**
	 * Menu actif
	 */
	private boolean menuEnabled = false;
	
	private DatePicker datePickerFrom;
	
	private DatePicker datePickerTo;
	
	private Spinner spinner;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.ois);

		initView();

		constellationManager = ConstellationManager
				.getConstellationManager(this);
		constellationManager.addListener(this);

		cpt = 0;
		loadData();
	}

	private void initView() {

		back = (LinearLayout) findViewById(R.id.main);
		rubricsHeaderView = (HeaderLayout) findViewById(R.id.ois_header);

		oisListView = (ListView) findViewById(R.id.ois_list);
		oisListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				Intent oiIntent = new Intent(getBaseContext(), OIActivity.class);
				oiIntent.putExtra(OIActivity.PROJECT, project);
				oiIntent.putExtra(OIActivity.RUBRIC, currentRubric);
				oiIntent.putExtra(OIActivity.THEME, theme);
				oiIntent.putExtra(OIActivity.POSITION, position);
				oiIntent.putExtra(OIActivity.OIs, FilterUtils.filterOIs(
						oiAdapter.getOIs(), filterScore, filterDateFrom,
						filterDateTo));
				startActivity(oiIntent);
			}
		});
		progress = (ProgressBar) findViewById(R.id.ois_progress);
		displayMenu = false;
	}

	/**
	 * Initialise le menu
	 * 
	 * @param enabled
	 */
	private void enableMenu() {
		if (!menuEnabled) {
			boolean isActiveAroundMe = project.isActiveArroundMe();
			if ((isActiveAroundMe) && (menuOIs != null)) {
				if (displayMenu) {
					menuOIs.findItem(R.id.ois_menu_see_map).setEnabled(true);
				} else {
					menuOIs.findItem(R.id.ois_menu_see_map).setEnabled(false);
				}
			}
			if (((rubric.getType() != null) && (menuOIs != null))
					&& (rubric.getType().equals(RubricType.Default))) {
				if (displayMenu) {
					menuOIs.findItem(R.id.ois_menu_fine_search)
							.setEnabled(true);
				} else {
					menuOIs.findItem(R.id.ois_menu_fine_search).setEnabled(
							false);
				}
			}
		}
		menuEnabled = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractActivity#loadData()
	 */
	@Override
	public void loadData() {

		project = (Project) getIntent().getParcelableExtra(OIsActivity.PROJECT);
		theme = (Theme) getIntent().getParcelableExtra(OIsActivity.THEME);
		rubric = (Rubric) getIntent().getParcelableExtra(OIsActivity.RUBRIC);
		currentRubric = rubric;
		filterScore = getIntent().getIntExtra(OIsActivity.FILTERSCORE, -1);
		filterDateFrom = getIntent().getLongExtra(OIsActivity.FILTERFROM, -1);
		filterDateTo = getIntent().getLongExtra(OIsActivity.FILTERTO, -1);

		oiAdapter = new OIAdapter(this, new ArrayList<OI>(), getHandler(),
				rubric.getTotalOIs());
		oisListView.setAdapter(oiAdapter);

		displayHeader();
		displayBackground();
		loadOIs(0);

	}

	/**
	 * Chargement des OIs
	 * 
	 * @param i
	 *            Postion du début de chargement
	 */
	private void loadOIs(int i) {

		Log.d(getClass().getSimpleName(), "loadOIs");

		taskIdOIs = constellationManager.getOIsByRubricId(project.getEctId(),
				project.getPlaylistId(), rubric.getId(), i);
		Log.d(getClass().getCanonicalName(), "Load Data : " + taskIdOIs);
	}

	/**
	 * Affiche le header
	 */
	protected void displayHeader() {

		if ((theme.getHeader() != null) && (theme.getHeader().length() > 0)) {
			rubricsHeaderView.setHeaderUrl(theme.getHeader());
		} else {
			rubricsHeaderView.setHeaderUrl(project.getHeader());
		}
		rubricsHeaderView.setHasTitle(true);
		rubricsHeaderView.setHeaderTitle(rubric.getTitle());
		rubricsHeaderView.setHeaderTitlePictoUrl(theme.getImg());
	}

	/**
	 * Affiche le fond
	 */
	protected void displayBackground() {
		if (project.getTemplate() != null) {
			Template tmp = project.getTemplate();
			if (tmp.equals(Template.Orange)) {
				back.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_orange));
			} else if (tmp.equals(Template.Blue)) {
				back.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_blue));
			} else {
				back.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_green));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.core.activity.AbstractActivity#dataChanged(java.lang
	 * .Object, java.lang.Integer)
	 */
	@Override
	public void dataChanged(final Object o, final Integer taskId) {

		getHandler().post(new Runnable() {

			@SuppressWarnings("unchecked")
			public void run() {
				Log.d(getClass().getCanonicalName(), "Data Changed");

				if (taskId.equals(taskIdOIs)) {

					ArrayList<OI> newOIs = ((ArrayList<OI>) o);
					ArrayList<OI> ois = filter(newOIs);
					displayOIs(ois, oiAdapter);
					refreshList(ois, currentRubric);
				}
			}
		});
	}

	/**
	 * Filtre des OIs
	 */
	public ArrayList<OI> filter(ArrayList<OI> newOIs) {

		if (newOIs == null) {
			return new ArrayList<OI>();
		}

		Log.d(getClass().getSimpleName(), "filter " + newOIs.size() + " OIs");
		return FilterUtils.filterOIs(newOIs, filterScore, filterDateFrom,
				filterDateTo);
	}

	/**
	 * Rafaîchissement de la liste des OIs
	 */
	public void refreshList(ArrayList<OI> newOIs, Rubric rubric) {

		if (newOIs.size() == 0) {
			loadNextOIs();
		}
	}

	/**
	 * Affichage des OIs
	 */
	public void displayOIs(ArrayList<OI> newOIs, OIAdapter adapter) {
		if ((newOIs != null) && (newOIs.size() > 0)) {
			progress.setVisibility(View.GONE);
			adapter.setOIs(newOIs);

		}
		displayMenu = true;
		enableMenu();

		Log.d(getClass().getSimpleName(), "display " + adapter.getCount()
				+ " OIs");
	}

	/**
	 * Chargement des 10 OIs suivants
	 */
	public void loadNextOIs() {

		if (isEnd()) {

			Log.d(getClass().getSimpleName(), "End of downloading");
			oiAdapter.setTotalCount(oiAdapter.getOIs().size());
			if (oiAdapter.getOIs().size() == 0) {
				setProgressVisibility(View.GONE);
				Toast.makeText(getBaseContext(),
						getString(R.string.empty_adapter), 3000).show();
			}
		} else {

			Log.d(getClass().getSimpleName(), "Load from : " + cpt);
			cpt += 10;
			loadOIs(cpt);
		}
	}

	public void setProgressVisibility(int view) {
		progress.setVisibility(view);
	}

	public void setCurrentRubric(Rubric rubric) {
		currentRubric = rubric;
	}

	public boolean isEnd() {
		return (cpt >= currentRubric.getTotalOIs());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			taskIdOIs = null;
		}
		return super.onKeyDown(keyCode, event);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.ois_menu, menu);
		menuOIs = menu;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPrepareOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menuOIs = menu;

		boolean isActiveAroundMe = project.isActiveArroundMe();
		if (!isActiveAroundMe) {
			menu.removeItem(R.id.ois_menu_see_map);
		} else {
			if (displayMenu) {
				menuEnabled = true;
				menu.findItem(R.id.ois_menu_see_map).setEnabled(true);
			} else {
				menu.findItem(R.id.ois_menu_see_map).setEnabled(false);
			}
		}
		if ((rubric.getType() == null)
				|| (rubric.getType().equals(RubricType.Default))) {
			menu.removeItem(R.id.ois_menu_fine_search);
		} else {
			if (displayMenu) {
				menuEnabled = true;
				menu.findItem(R.id.ois_menu_fine_search).setEnabled(true);
			} else {
				menu.findItem(R.id.ois_menu_fine_search).setEnabled(false);
			}
		}

		return super.onPrepareOptionsMenu(menu);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.ois_menu_home) {
			returnHome();
		} else if (item.getItemId() == R.id.ois_menu_fine_search) {
			
			SearchDialog.Builder dialogBuilder = new SearchDialog.Builder(
					OIsActivity.this);
			dialogBuilder
					.setTitle(R.string.fine_search)
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									RubricType type = rubric.getType();
									if (type.equals(RubricType.Hebergement)) {
										OIsActivity.this
												.dismissDialog(R.layout.fine_search_classe);
									} else {
										OIsActivity.this
												.dismissDialog(R.layout.fine_search_date);
									}
								}
							})
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									
									if (rubric.getType().equals(
											RubricType.Hebergement)) {
										
										filterScore = spinner
												.getSelectedItemPosition() + 1;
									} else {

										Calendar fromCal = GregorianCalendar.getInstance(Locale.FRANCE);
										fromCal.set(Calendar.YEAR, datePickerFrom.getYear());
										fromCal.set(Calendar.MONTH, datePickerFrom.getMonth());
										fromCal.set(Calendar.DAY_OF_MONTH, datePickerFrom.getDayOfMonth());
										Date dateFrom = fromCal.getTime();
										filterDateFrom = dateFrom.getTime();

										Calendar toCal = GregorianCalendar.getInstance(Locale.FRANCE);
										toCal.set(Calendar.YEAR, datePickerTo.getYear());
										toCal.set(Calendar.MONTH, datePickerTo.getMonth());
										toCal.set(Calendar.DAY_OF_MONTH, datePickerTo.getDayOfMonth());
										Date dateTo = toCal.getTime();
										filterDateTo = dateTo.getTime();
									}

									searchDialog.dismiss();
									if (rubric.getType().equals(
											RubricType.Hebergement)) {
										
										finish();
										Intent intent = new Intent(
												getBaseContext(),
												OIsActivity.class);
										intent.putExtra(OIsActivity.PROJECT,
												project);
										intent.putExtra(OIsActivity.THEME,
												theme);
										intent.putExtra(OIsActivity.RUBRIC,
												rubric);
										intent.putExtra(
												OIsActivity.FILTERSCORE, filterScore);
										intent.putExtra(OIsActivity.FILTERFROM,
												-1);
										intent.putExtra(OIsActivity.FILTERTO,
												-1);
										startActivity(intent);
									} else {
										
										finish();
										Intent intent = new Intent(
												getBaseContext(),
												OIsActivity.class);
										intent.putExtra(OIsActivity.PROJECT,
												project);
										intent.putExtra(OIsActivity.THEME,
												theme);
										intent.putExtra(OIsActivity.RUBRIC,
												rubric);
										intent.putExtra(
												OIsActivity.FILTERSCORE, -1);
										intent.putExtra(OIsActivity.FILTERFROM,
												filterDateFrom);
										intent.putExtra(OIsActivity.FILTERTO,
												filterDateTo);
										startActivity(intent);
									}
								}
							});
			searchDialog = dialogBuilder.create(rubric.getType());
			datePickerFrom = (DatePicker) searchDialog
					.findViewById(R.id.date_from);
			if ((datePickerFrom != null) && (filterDateFrom != -1)) {
				
				Calendar cal = GregorianCalendar.getInstance();
				cal.setTimeInMillis(filterDateFrom);
				datePickerFrom.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			}
			datePickerTo = (DatePicker) searchDialog
					.findViewById(R.id.date_to);
			if ((datePickerFrom != null) && (filterDateTo != -1)) {
				
				Calendar cal = GregorianCalendar.getInstance();
				cal.setTimeInMillis(filterDateTo);
				datePickerTo.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			}
			
			spinner = (Spinner) searchDialog
					.findViewById(R.id.score);
			if ((spinner != null) && (filterScore != -1)) {
				spinner.setSelection(filterScore - 1);
			}
			
			searchDialog.show();
		} else if (item.getItemId() == R.id.ois_menu_see_map) {
			
			Intent myMapIntent = new Intent(getBaseContext(),
					OIsMapActivity.class);
			myMapIntent.putExtra(OIsMapActivity.PROJECT, project);
			myMapIntent.putExtra(OIsMapActivity.THEME, theme);
			myMapIntent.putExtra(OIsMapActivity.RUBRIC, rubric);
			int size = Math.min(oiAdapter.getOIs().size(), 20);
			ArrayList<OI> ois = new ArrayList<OI>(oiAdapter.getOIs().subList(0,
					size));
			myMapIntent.putParcelableArrayListExtra(OIsMapActivity.OIs,
					FilterUtils.filterOIs(ois, filterScore, filterDateFrom,
							filterDateTo));
			myMapIntent.putExtra(OIsMapActivity.MAX, cpt);
			myMapIntent.putExtra(OIsMapActivity.FILTERSCORE, filterScore);
			myMapIntent.putExtra(OIsMapActivity.FILTERFROM, filterDateFrom);
			myMapIntent.putExtra(OIsMapActivity.FILTERTO, filterDateTo);
			startActivity(myMapIntent);
		} else {
		}

		return super.onOptionsItemSelected(item);
	}
}
