package com.constellation.core.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.constellation.core.R;
import com.constellation.core.adapter.ProjectAdapter;
import com.constellation.core.exception.ConnectionConstellationException;
import com.constellation.core.manager.ConstellationManager;
import com.constellation.core.model.Project;
import com.constellation.core.services.ServiceObserver;
import com.constellation.core.utils.DialogUtils;

public class MainHomeActivity extends Activity implements ServiceObserver {

	/**
	 * Gestionnaire des données contellation.
	 */
	private ConstellationManager constellationManager;

	/**
	 * Identifiant de la tâche de récupération des projets.
	 */
	private Integer taskIdProjects;

	/**
	 * Liste des projets.
	 */
	private List<Project> projects;
	
	/**
	 * Liste des projets téléchargés.
	 */
	private List<Project> projectsStored;

	/**
	 * Roulette de chargement de la page.
	 */

	private ProgressBar projectsProgressBar;

	/**
	 * ListView des projets.
	 */
	private ListView projects_listView;
	
	/**
	 * Tous les guides
	 */
	private TextView allGuidesView;
	
	/**
	 * Mes guides
	 */
	private TextView myGuidesView;
	
	/**
	 * Projets chargés
	 */
	private boolean isLoaded;
	
	/**
	 * Tous les guides
	 */
	private boolean isAllGuides;
	
	/**
	 * Le Handler qui gère l'affichage de l'image sur la thread principal.
	 */
	private static Handler handler;
	/**
	 * Etat de l'activité.
	 */
	protected boolean isSuspendedActivity = false;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.player);
		if (handler == null) {
			handler = new Handler();
		}
		this.isSuspendedActivity = false;
		isAllGuides = true;
		isLoaded = false;
		initView();
		//loadData();
	}
	
	@Override
	protected void onStart() {
		
		super.onStart();
		loadData();
	}

	public void initView() {
		projects_listView = (ListView) findViewById(R.id.projects_list);
		allGuidesView = (TextView) findViewById(R.id.all_guides);
		myGuidesView = (TextView) findViewById(R.id.my_guides);
		projectsProgressBar = (ProgressBar) findViewById(R.id.player_progress);
		projects_listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				Project currentProject = projects.get(position);

				Intent projectIntent = new Intent(getBaseContext(),
						GuideHomeActivity.class);
				projectIntent.putExtra("splash",
						currentProject.getSplashScreen());
				projectIntent.putExtra("project_id", currentProject.getId());
				startActivity(projectIntent);
			}
		});
		
		allGuidesView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				isAllGuides = true;
				allGuidesView.setBackgroundResource(R.drawable.tab_left_on);
				allGuidesView.setTextColor((getResources().getColor(R.color.darkblue)));
				myGuidesView.setBackgroundResource(R.drawable.tab_right_off);
				myGuidesView.setTextColor((getResources().getColor(R.color.lightgray)));
				projects_listView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View v,
							int position, long id) {
						Project currentProject = projects.get(position);

						Intent projectIntent = new Intent(getBaseContext(),
								GuideHomeActivity.class);
						projectIntent.putExtra("splash",
								currentProject.getSplashScreen());
						projectIntent.putExtra("project_id", currentProject.getId());
						startActivity(projectIntent);
					}
				});
				displayProjects(projects);
			}
		});
		
		myGuidesView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				isAllGuides = false;
				allGuidesView.setBackgroundResource(R.drawable.tab_left_off);
				allGuidesView.setTextColor((getResources().getColor(R.color.lightgray)));
				myGuidesView.setBackgroundResource(R.drawable.tab_right_on);
				myGuidesView.setTextColor((getResources().getColor(R.color.darkblue)));
				projects_listView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View v,
							int position, long id) {
						Project currentProject = projectsStored.get(position);

						Intent projectIntent = new Intent(getBaseContext(),
								GuideHomeActivity.class);
						projectIntent.putExtra("splash",
								currentProject.getSplashScreen());
						projectIntent.putExtra("project_id", currentProject.getId());
						startActivity(projectIntent);
					}
				});
				displayProjects(projectsStored);
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractActivity#loadData()
	 */
	public void loadData() {
		constellationManager = ConstellationManager.getConstellationManager(this);
		constellationManager.addListener(this);
		taskIdProjects = constellationManager.getProjects();

		Log.d(getClass().getSimpleName(), "Load Data : " + taskIdProjects);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.core.activity.AbstractActivity#dataChanged(java.lang
	 * .Object, java.lang.Integer)
	 */
	public void dataChanged(final Object o, final Integer taskId) {
		handler.post(new Runnable() {

			@SuppressWarnings("unchecked")
			public void run() {
				Log.d(getClass().getCanonicalName(), "Data Changed");

				if (taskId.equals(taskIdProjects)) {
					projects = (List<Project>) o;
					projectsStored = new ArrayList<Project>();
					for (int i=0; i<projects.size();i++) {
						Project p = projects.get(i);
						if (p.isStore()) {
							projectsStored.add(p);
						}
					}
					Log.d(getClass().getSimpleName(), "All projects : "+projects.size());
					Log.d(getClass().getSimpleName(), "Projects stored : "+projectsStored.size());
					projectsProgressBar.setVisibility(View.GONE);
					isLoaded = true;
					if (isAllGuides){
						displayProjects(projects);
					} else{
						displayProjects(projectsStored);
					}
				}
			}
		});
	}

	/**
	 * Affiche les projets
	 */
	private void displayProjects(List<Project> projectsDisplayed) {
		if (isLoaded){
			ProjectAdapter projectAdapter = new ProjectAdapter(this,
					(ArrayList<Project>) projectsDisplayed, handler);
			projects_listView.setAdapter(projectAdapter);
		}
	}

	/**
	 * Appelée lorsque l'activité passe en arrière plan; Libérez les écouteurs,
	 * arrêtez les threads, votre activité peut disparaître de la mémoire.
	 */
	@Override
	protected void onStop() {

		this.isSuspendedActivity = true;
		super.onStop();
	}

	/**
	 * Appelée lorsque l'activité sort de son état de veille.
	 */
	@Override
	protected void onRestart() {

		super.onRestart();
		this.isSuspendedActivity = false;
	}

	/**
	 * Appelée lorsque l'activité est suspendue. Stoppez les actions qui
	 * consomment des ressources. L'activité va passer en arrière-plan.
	 */
	@Override
	protected void onPause() {

		this.isSuspendedActivity = true;
		super.onPause();
	}

	/**
	 * Appelée après le démarrage ou une pause. Relancez les opérations arrêtés
	 * (threads). Mettez à jour votre application et vérifiez vos écouteurs.
	 */
	@Override
	protected void onResume() {

		super.onResume();
		this.isSuspendedActivity = false;
	}

	@Override
	public void errorTreatment(Throwable e, Integer taskId) {

		if (!this.isSuspendedActivity) {

			if (e instanceof ConnectionConstellationException) {
				DialogUtils.showConnectionError(this, handler);
			} else {
				DialogUtils.showMessageError(this, handler);
			}
		}
	}
}
