package com.constellation.core.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.constellation.core.R;
import com.constellation.core.model.OI;
import com.google.android.maps.GeoPoint;

/**
 * Cette activité permet d'afficher un OI sur une carte.
 * 
 * @author j.varin
 * 
 */
public class DetailMapActivity extends AbstractMapActivity {

	/**
	 * Le nom du paramètre par lequel on passe l'OI.
	 */
	public static final String OI = "oi";

	/**
	 * OI à afficher.
	 */
	private OI oi;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		initView();

		maxLat = (int) Integer.MIN_VALUE;
		minLat = (int) Integer.MAX_VALUE;
		maxLon = (int) Integer.MIN_VALUE;
		minLon = (int) Integer.MAX_VALUE;
		
		loadData();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractActivity#loadData()
	 */
	@Override
	public void loadData() {

		oi = (OI) getIntent().getParcelableExtra(OI);
		super.loadData();

		displayOI(oi);

		double latitude = oi.getCoordY() * 1000000;
		double longitude = oi.getCoordX() * 1000000;
		GeoPoint detailPoint = new GeoPoint((int) latitude, (int) longitude);
		mapController.animateTo(detailPoint);
		mapController.setZoom(16);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractMapActivity#displayHeader()
	 */
	@Override
	protected void displayHeader() {

		super.displayHeader();
		mapHeaderView.setHeaderTitle(oi.getTitle());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_map, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.home) {
			returnHome();
		} else if (item.getItemId() == R.id.caption) {
			Dialog dialog = new Dialog(DetailMapActivity.this);
			dialog.setContentView(R.layout.caption);
			dialog.setTitle(R.string.menu_caption);
			dialog.show();
		} else {
		}
		return super.onOptionsItemSelected(item);
	}
}
