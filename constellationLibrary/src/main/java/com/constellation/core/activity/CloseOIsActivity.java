package com.constellation.core.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.location.Location;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.constellation.core.R;
import com.constellation.core.adapter.OIAdapter;
import com.constellation.core.model.OI;
import com.constellation.core.model.Project;
import com.constellation.core.model.Rubric;
import com.constellation.core.model.Theme;
import com.constellation.core.model.ThemeType;

public class CloseOIsActivity extends OIsActivity {

	/**
	 * Le nom du paramètre par lequel on passe la rubrique.
	 */
	public static final String OI = "oi";

	/**
	 * Identifiant de la tache de récupération des OIs.
	 */
	private int closeOisTaskId;

	/**
	 * Vue de la liste de OIs.
	 */
	private ListView oisListView;

	/**
	 * OI de repère
	 */
	private OI oiRef;

	/**
	 * OIs à afficher
	 */
	private ArrayList<OI> newOIs;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractActivity#loadData()
	 */
	@Override
	public void loadData() {

		project = (Project) getIntent().getParcelableExtra(PROJECT);
		theme = (Theme) getIntent().getParcelableExtra(THEME);
		Rubric currentRubric = (Rubric) getIntent().getParcelableExtra(RUBRIC);
		String currentRubricId = currentRubric.getId();
		oiRef = (OI) getIntent().getParcelableExtra(OI);
		filterScore = getIntent().getIntExtra("filterScore", -1);
		filterDateFrom = getIntent().getIntExtra(OIsActivity.FILTERFROM, -1);
		filterDateTo = getIntent().getIntExtra(OIsActivity.FILTERTO, -1);
		// TODO

		rubric = new Rubric();
		rubric.setTitle(getString(R.string.around) + " - " + oiRef.getTitle());
		rubric.setTotalOIs(20);
		rubric.setType(0);

		setCurrentRubric(rubric);

		oiAdapter = new OIAdapter(this, new ArrayList<OI>(), getHandler(), 0);

		oisListView = (ListView) findViewById(R.id.ois_list);
		oisListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				Intent oiIntent = new Intent(getBaseContext(), OIActivity.class);
				oiIntent.putExtra(OIActivity.PROJECT, project);
				oiIntent.putExtra(OIActivity.RUBRIC, rubric);
				oiIntent.putExtra(OIActivity.THEME, theme);
				int size = Math.min(oiAdapter.getOIs().size(), 20);
				ArrayList<OI> ois = new ArrayList<OI>(oiAdapter.getOIs()
						.subList(0, size));
				oiIntent.putExtra(OIActivity.OIs, ois);
				oiIntent.putExtra(OIActivity.POSITION, position);
				startActivity(oiIntent);
			}
		});
		oisListView.setAdapter(oiAdapter);

		displayHeader();
		displayBackground();

		StringBuffer rubricIdListStr = new StringBuffer();
		boolean isFirst = true;
		for (Theme themeItem : project.getThemes()) {
			if (themeItem.getType().equals(ThemeType.Rubric)) {
				for (Rubric rubricItem : themeItem.getRubrics()) {

					if (!currentRubricId.equals(rubricItem.getId())) {
						if (isFirst == false) {
							rubricIdListStr.append("|");
						} else {
							isFirst = false;
						}
						rubricIdListStr.append(rubricItem.getId());
					}
				}
			}
		}
		String rubricId = rubricIdListStr.toString();

		Float coordX = new Float(oiRef.getCoordY());
		Float coordY = new Float(oiRef.getCoordX());
		Float radius = theme.getRadius();

		closeOisTaskId = constellationManager.getOIsByRubricIdAround(
				project.getEctId(), project.getPlaylistId(), rubricId,
				oiRef.getIdType(), coordX.toString(), coordY.toString(), "10",
				20);// TODO Remettre Radius

		Log.d(getClass().getCanonicalName(), "EctId : " + project.getEctId());
		Log.d(getClass().getCanonicalName(),
				"PlaylistId : " + project.getPlaylistId());
		Log.d(getClass().getCanonicalName(), "Liste des rubriques : "
				+ rubricId);
		Log.d(getClass().getCanonicalName(),
				"Type OI à éviter : " + oiRef.getIdType());
		Log.d(getClass().getCanonicalName(), "X : " + coordX.toString());
		Log.d(getClass().getCanonicalName(), "Y : " + coordY.toString());
		Log.d(getClass().getCanonicalName(), "Radius : " + radius.toString());

		Log.d(getClass().getCanonicalName(), "Load Data : " + closeOisTaskId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.core.activity.AbstractMapActivity#dataChanged(java.
	 * lang.Object, java.lang.Integer)
	 */
	@Override
	public void dataChanged(final Object o, final Integer taskId) {

		getHandler().post(new Runnable() {

			@SuppressWarnings("unchecked")
			public void run() {
				if (taskId.equals(closeOisTaskId)) {
					newOIs = (ArrayList<OI>) o;
					Log.d(getClass().getSimpleName(), "Liste des OIs : "
							+ newOIs.size());
					filter(newOIs);
					displayOIs(newOIs, oiAdapter);
					refreshList(newOIs, rubric);
					oiAdapter.setTotalCount(newOIs.size());

					Location location = new Location("locRef");
					location.setLongitude(Double.parseDouble(Float
							.toString(oiRef.getCoordX())));
					location.setLatitude(Double.parseDouble(Float
							.toString(oiRef.getCoordY())));
					oiAdapter.setDistance(location);
				}
			}
		});
	}

	@Override
	public boolean isEnd() {
		if (oiAdapter.getOIs().size() == 0) {
			setProgressVisibility(View.GONE);
			Toast.makeText(getBaseContext(), getString(R.string.empty_adapter),
					3000).show();
		}
		return true;
	}

	@Override
	public void loadNextOIs() {
		return;
	}
}
