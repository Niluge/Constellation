package com.constellation.core.activity;

import java.util.List;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.constellation.core.R;
import com.constellation.core.manager.ConstellationManager;
import com.constellation.core.model.OI;
import com.constellation.core.model.Rubric;
import com.constellation.core.utils.DialogUtils;
import com.google.android.maps.GeoPoint;

/**
 * Cette activité permet d'afficher les OIs proche de la position courante sur
 * une carte.
 * 
 * 
 * @author j.varin
 * 
 */
public class CloseOIsMapActivity extends AbstractMapActivity {

	/**
	 * Gestionnaire des données contellation.
	 */
	private ConstellationManager constellationManager;

	/**
	 * Identifiant de la tache de récupération des OIs.
	 */
	private int closeOiTaskId;

	/**
	 * La position courante de l'utilisateur.
	 */
	private Location currentLocation;

	/**
	 * Boite de dialogue.
	 */
	private ProgressDialog geolocationProgressDialog;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		initView();

		constellationManager = ConstellationManager
				.getConstellationManager(this);
		constellationManager.addListener(this);

		loadData();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractActivity#loadData()
	 */
	@Override
	public void loadData() {

		super.loadData();
		rubric = new Rubric();
		rubric.setTitle("Autour de moi");
		rubric.setType(0);

		geolocalize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.core.activity.AbstractMapActivity#dataChanged(java.
	 * lang.Object, java.lang.Integer)
	 */
	@Override
	public void dataChanged(final Object o, final Integer taskId) {

		getHandler().post(new Runnable() {

			@SuppressWarnings("unchecked")
			public void run() {
				if (taskId.equals(closeOiTaskId)) {

					List<OI> ois = (List<OI>) o;
					for (OI oi : ois) {
						if (oi != null) {
							displayOI(oi);
						}
					}
					mapController.animateTo(new GeoPoint((maxLat + minLat) / 2,
							(maxLon + minLon) / 2));
					mapController.zoomToSpan(Math.abs(maxLat - minLat) + 20000,
							Math.abs(maxLon - minLon) + 150);
				}
			}
		});
	}

	/**
	 * Localise l'utilisateur et lance la recherche des OIs alentours.
	 */
	private void geolocalize() {

		geolocationProgressDialog = ProgressDialog.show(this,
				getString(R.string.geoloc_title),
				getString(R.string.geoloc_loading));
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		LocationListener locationListener = new ConstellationLocationListener();

		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 5000, 1, locationListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				10000, 1, locationListener);

		getHandler().postDelayed(new Runnable() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				if (currentLocation == null) {

					geolocationProgressDialog.dismiss();
					DialogUtils.showGeolocError(CloseOIsMapActivity.this,
							getHandler());
				}
			}
		}, 15000);
	}

	protected void loadDataLocation() {

	}

	/**
	 * Ecouteur de la position de l'utilisateur qui lance la récupération des
	 * données.
	 * 
	 * @author j.varin
	 * 
	 */
	private class ConstellationLocationListener implements LocationListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.location.LocationListener#onLocationChanged(android.location
		 * .Location)
		 */
		@Override
		public void onLocationChanged(Location location) {

			if (currentLocation == null) {

				geolocationProgressDialog.dismiss();
				currentLocation = location;

				String ectid = project.getEctId();
				String playlistId = project.getPlaylistId();
				StringBuffer rubricIdListStr = new StringBuffer();
				boolean isFirst = true;
				for (Rubric rubricItem : theme.getRubrics()) {

					if (isFirst == false) {
						rubricIdListStr.append("|");
					} else {
						isFirst = false;
					}
					rubricIdListStr.append(rubricItem.getId());
				}
				String rubricId = rubricIdListStr.toString();
				Float radius = theme.getRadius();

				Double coordX = currentLocation.getLatitude();
				Double coordY = currentLocation.getLongitude();

				maxLat = (int) (coordX * 1000000);
				minLat = (int) (coordX * 1000000);
				maxLon = (int) (coordY * 1000000);
				minLon = (int) (coordY * 1000000);

				GeoPoint projectPoint = new GeoPoint((int) (coordX * 1000000),
						(int) (coordY * 1000000));
				mapController.animateTo(projectPoint);
				mapController.setZoom(11);
				
				closeOiTaskId = constellationManager
						.getOIsByRubricIdAround(ectid, playlistId,
								rubricId, "", coordX.toString(),
								coordY.toString(), radius.toString(), 50);
				
			}

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.location.LocationListener#onProviderDisabled(java.lang.String
		 * )
		 */
		@Override
		public void onProviderDisabled(String provider) {
			return;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.location.LocationListener#onProviderEnabled(java.lang.String)
		 */
		@Override
		public void onProviderEnabled(String provider) {
			return;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.location.LocationListener#onStatusChanged(java.lang.String,
		 * int, android.os.Bundle)
		 */
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			return;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_map, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.caption) {
			Dialog dialog = new Dialog(CloseOIsMapActivity.this);
			dialog.setContentView(R.layout.caption);
			dialog.setTitle(R.string.menu_caption);
			dialog.show();
		} else if (item.getItemId() == R.id.home) {
			returnHome();
		} else {
		}
		return super.onOptionsItemSelected(item);
	}
}
