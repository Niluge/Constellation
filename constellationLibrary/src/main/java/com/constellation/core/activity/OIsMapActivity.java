package com.constellation.core.activity;

import java.util.List;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.constellation.core.R;
import com.constellation.core.manager.ConstellationManager;
import com.constellation.core.model.OI;
import com.constellation.core.utils.FilterUtils;
import com.google.android.maps.GeoPoint;

/**
 * Cette activité permet d'afficher la liste des OIs d'une rubrique sur une
 * carte.
 * 
 * @author j.varin
 * 
 */
public class OIsMapActivity extends AbstractMapActivity {

	/**
	 * Le nom du paramètre par lequel on passe la rubrique.
	 */
	public static final String FILTERSCORE = "filterScore";

	/**
	 * Le nom du paramètre par lequel on passe la rubrique.
	 */
	public static final String FILTERFROM = "filterDateFrom";

	/**
	 * Le nom du paramètre par lequel on passe la rubrique.
	 */
	public static final String FILTERTO = "filterDateTo";
	
	/**
	 * Gestionnaire des données contellation.
	 */
	private ConstellationManager constellationManager;

	/**
	 * Identifiant de la tache de récupération des OIs.
	 */
	private int oisTaskId;

	/**
	 * OI de repère
	 */
	private List<OI> ois;

	private int max;

	private int cpt = -1;
	
	/**
	 * Filtre par étoiles
	 */
	protected int filterScore;

	/**
	 * Filtre date de début
	 */
	protected int filterDateFrom;

	/**
	 * Filtre date de fin
	 */
	protected int filterDateTo;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		initView();

		maxLat = (int) Integer.MIN_VALUE;
		minLat = (int) Integer.MAX_VALUE;
		maxLon = (int) Integer.MIN_VALUE;
		minLon = (int) Integer.MAX_VALUE;

		constellationManager = ConstellationManager
				.getConstellationManager(this);
		constellationManager.addListener(this);

		loadData();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractActivity#loadData()
	 */
	@Override
	public void loadData() {

		super.loadData();
		filterScore = getIntent().getIntExtra(OIsActivity.FILTERSCORE, -1);
		filterDateFrom = getIntent().getIntExtra(OIsActivity.FILTERFROM, -1);
		filterDateTo = getIntent().getIntExtra(OIsActivity.FILTERTO, -1);
		max = getIntent().getIntExtra(MAX, -1);
		ois = getIntent().getParcelableArrayListExtra(OIs);
		if (ois != null) {
			
			cpt = ois.size();
			for (OI oi : ois) {
				if (oi != null) {
					displayOI(oi);
				}
			}
		}

		if (max > cpt) {
			oisTaskId = constellationManager.getOIsByRubricId(
					project.getEctId(), project.getPlaylistId(),
					rubric.getId(), cpt);
		}

		// Centre la carte et ajuste le zoom
		mapController.animateTo(new GeoPoint((maxLat + minLat) / 2,
				(maxLon + minLon) / 2));
		mapController.zoomToSpan(Math.abs(maxLat - minLat) + 20000,
				Math.abs(maxLon - minLon) + 150);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.core.activity.AbstractMapActivity#dataChanged(java.
	 * lang.Object, java.lang.Integer)
	 */
	@Override
	public void dataChanged(final Object o, final Integer taskId) {

		getHandler().post(new Runnable() {

			@SuppressWarnings("unchecked")
			public void run() {
				if (taskId.equals(oisTaskId)) {

					List<OI> ois = (List<OI>) o;
					List<OI> newOIs = FilterUtils.filterOIs(ois, filterScore, filterDateFrom, filterDateTo);
					for (OI oi : newOIs) {
						if (oi != null) {
							displayOI(oi);
						}
					}

					mapController.animateTo(new GeoPoint((maxLat + minLat) / 2,
							(maxLon + minLon) / 2));
					mapController.zoomToSpan(Math.abs(maxLat - minLat) + 20000,
							Math.abs(maxLon - minLon));

					if (max >= cpt) {

						cpt += 10;
						oisTaskId = constellationManager.getOIsByRubricId(
								project.getEctId(), project.getPlaylistId(),
								rubric.getId(), cpt);
					}
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_map, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.caption) {
			Dialog dialog = new Dialog(OIsMapActivity.this);
			dialog.setContentView(R.layout.caption);
			dialog.setTitle(R.string.menu_caption);
			dialog.show();
		} else if (item.getItemId() == R.id.home) {
			returnHome();
		} else {
		}
		return super.onOptionsItemSelected(item);
	}
}
