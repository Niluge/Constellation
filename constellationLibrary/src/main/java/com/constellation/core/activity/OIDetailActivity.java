package com.constellation.core.activity;

import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.constellation.core.R;
import com.constellation.core.model.OI;
import com.constellation.core.model.Project;
import com.constellation.core.model.Rubric;
import com.constellation.core.model.Theme;
import com.constellation.core.view.HeaderLayout;

public class OIDetailActivity extends AbstractActivity {

	/**
	 * Le nom du paramètre par lequel on passe le guide.
	 */
	public static final String PROJECT = "project";

	/**
	 * Le nom du paramètre par lequel on passe le thème.
	 */
	public static final String THEME = "theme";

	/**
	 * Le nom du paramètre par lequel on passe la rubrique.
	 */
	public static final String RUBRIC = "rubric";

	/**
	 * Le nom du paramètre par lequel on passe l'OI.
	 */
	public static final String OI = "oi";
	
	/**
	 * Header de la page.
	 */
	private HeaderLayout header;
	
	/**
	 * TextView du titre
	 */
	private TextView titleView;
	
	/**
	 * TextView du contenu
	 */
	private TextView contentView;

	/**
	 * Le guide à afficher.
	 */
	private Project project;
	
	/**
	 * Le thème à afficher.
	 */
	private Theme theme;
	
	/**
	 * La rubrique à afficher.
	 */
	private Rubric rubric;
	
	/**
	 * L'OI à afficher.
	 */
	private OI oi;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.details_oi);
		
		initView();
		loadData();
	}

	private void initView() {
		header = (HeaderLayout) findViewById(R.id.oi_detail_header);
		titleView = (TextView) findViewById(R.id.details);
		contentView = (TextView) findViewById(R.id.text);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractActivity#loadData()
	 */
	@Override
	public void loadData() {
		project = (Project) getIntent().getParcelableExtra(OIDetailActivity.PROJECT);
		theme = (Theme) getIntent().getParcelableExtra(OIDetailActivity.THEME);
		rubric = (Rubric) getIntent().getParcelableExtra(OIDetailActivity.RUBRIC);
		oi = (OI) getIntent().getParcelableExtra(OIDetailActivity.OI);
		
		displayHeader();
		displayTitle();
		displayContent();
	}
	/**
	 * Affiche le header
	 */
	public void displayHeader() {
		if ((theme.getHeader() != null) && (!theme.getHeader().equals(""))) {
			header.setHeaderUrl(theme.getHeader());
		} else if ((project.getHeader() != null)
				&& (!project.getHeader().equals(""))) {
			header.setHeaderUrl(project.getHeader());
		}
		header.setHasTitle(true);
		header.setHeaderTitle(rubric.getTitle());
		header.setHeaderTitlePictoUrl(theme.getImg());
	}
	/**
	 * Affiche le titre
	 */
	public void displayTitle() {
		String titleOI;
		if ((oi.getTitle() != null)
				&& (!oi.getTitle().equals(""))) {
			titleOI = oi.getTitle();
		} else {
			titleOI = oi.getTrad().getTitre();
		}
		titleView.setText(getString(R.string.details) + " - " + titleOI);
	}
	
	/**
	 * Affiche le contenu
	 */
	public void displayContent() {
		String content = "";
		
		if ((oi.getTrad().getChapeau() != null)
				&& (oi.getTrad().getChapeau().length() > 0)) {
			content += oi.getTrad().getChapeau().replaceAll("\n", "<br/>")
					+ "<br/><br/>";
		}
		if ((oi.getTrad().getArticle() != null)
				&& (oi.getTrad().getArticle().length() > 0)) {
			content += oi.getTrad().getArticle().replaceAll("\n", "<br/>")
					+ "<br/>";
		}
		
		// Dates
		String dates ="";
 	   	if ((oi.getDates()!=null)&&(oi.getDates().size()>0)){
 	   		for (int i=0;i<oi.getDates().size();i++){
 	   			String from = oi.getDates().get(i).getFrom();
 	   			String to = oi.getDates().get(i).getTo();
 	   			if (from.equals(to))
 	   				dates += "\n"+from;
 	   			else
 	   				dates += "\nDu "+from+" au "+to;
 	   		}
 	   	}
 	    if (dates.length()>0) {
 	    	content += "<br/><b><u>" + "Dates"
					+ " :</u></b><br/>";
			content += dates.replaceAll("\n", "<br/>")
					+ "<br/>";
		}
 	    
		// Services
		if ((oi.getSceaServices() != null) && (oi.getSceaServices().size() > 0)) {
			content += "<br/><b><u>" + getString(R.string.services)
					+ " :</u></b><br/>";
			for (int i = 0; i < oi.getSceaServices().size(); i++) {
				content += oi.getSceaServices().get(i).getName() + "<br/>";
			}
		}
		// Activités
		if ((oi.getSceaActivites() != null)
				&& (oi.getSceaActivites().size() > 0)) {
			content += "<br/><b><u>" + getString(R.string.activities)
					+ " :</u></b><br/>";
			for (int i = 0; i < oi.getSceaActivites().size(); i++) {
				content += oi.getSceaActivites().get(i).getName() + "<br/>";
			}
		}
		// Conforts
		if ((oi.getSceaConforts() != null) && (oi.getSceaConforts().size() > 0)) {
			content += "<br/><b><u>" + getString(R.string.comforts)
					+ " :</u></b><br/>";
			for (int i = 0; i < oi.getSceaConforts().size(); i++) {
				content += oi.getSceaConforts().get(i).getName() + "<br/>";
			}
		}
		// Encadrement
		if ((oi.getSceaEncadrements() != null)
				&& (oi.getSceaEncadrements().size() > 0)) {
			content += "<br/><b><u>" + getString(R.string.surrounding)
					+ " :</u></b><br/>";
			for (int i = 0; i < oi.getSceaEncadrements().size(); i++) {
				content += oi.getSceaEncadrements().get(i).getName()
						+ "<br/>";
			}
			;
		}
		// Equipements
		if ((oi.getSceaEquipements() != null)
				&& (oi.getSceaEquipements().size() > 0)) {
			content += "<br/><b><u>" + getString(R.string.facilities)
					+ " :</u></b><br/>";
			for (int i = 0; i < oi.getSceaEquipements().size(); i++) {
				content += oi.getSceaEquipements().get(i).getName()
						+ "<br/>";
			}
		}
		// Handicap
		if ((oi.getClassHandi() != null) && (oi.getClassHandi().size() > 0)) {
			content += "<br/><b><u>" + getString(R.string.accessibility)
					+ " :</u></b><br/>";
			for (int i = 0; i < oi.getClassHandi().size(); i++) {
				content += oi.getClassHandi().get(i).getName() + "<br/>";
			}
		}
		// Langue accueil
		if ((oi.getLangAccueil() != null) && (oi.getLangAccueil().size() > 0)) {
			content += "<br/><b><u>" + getString(R.string.speak)
					+ " :</u></b><br/>";
			for (int i = 0; i < oi.getLangAccueil().size(); i++) {
				content += oi.getLangAccueil().get(i).getName() + "<br/>";
			}
		}
		// Mode de paiement
		if ((oi.getModesPaiement() != null)
				&& (oi.getModesPaiement().size() > 0)) {
			content += "<br/><b><u>" + getString(R.string.payment)
					+ " :</u></b><br/>";
			for (int i = 0; i < oi.getModesPaiement().size(); i++) {
				content += oi.getModesPaiement().get(i).getName() + "<br/>";
			}
		}
		contentView.setText(Html.fromHtml(content));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back_home, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.home) {
			returnHome();
		} else {
		}
		return super.onOptionsItemSelected(item);
	}
}
