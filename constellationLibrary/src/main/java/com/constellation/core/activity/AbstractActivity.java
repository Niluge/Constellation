package com.constellation.core.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.constellation.core.exception.ConnectionConstellationException;
import com.constellation.core.services.ServiceObserver;
import com.constellation.core.utils.DialogUtils;

/**
 * Activité abstraite qui contient les actions communes à toutes les activités.
 * 
 * @author j.varin
 * 
 */
public class AbstractActivity extends Activity implements ServiceObserver {

	/**
	 * Le Handler qui gère l'affichage de l'image sur la thread principal.
	 */
	private static Handler handler;

	/**
	 * Home Page de l'application.
	 */
	public final static String ACTION_HOME_ACCESS = "com.constellation.core.activity.GuideHomeActivity";

	/**
	 * La caractère espace.
	 */
	public final static String BLANK_SPACE = " ";

	/**
	 * L'écouteur de remise à zéro de l'historique.
	 */
	protected BroadcastReceiver broadcastReceiver;

	/**
	 * Etat de l'activité.
	 */
	protected boolean isSuspendedActivity = false;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grâce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (handler == null) {
			handler = new Handler();
		}
	}

	/**
	 * Appelée lorsque l'activité démarre. Permet d'initaliser les contrôles.
	 */
	@Override
	protected void onStart() {

		super.onStart();

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(ACTION_HOME_ACCESS);
		if (broadcastReceiver == null) {
			broadcastReceiver = new BroadcastReceiver() {

				@Override
				public void onReceive(Context context, Intent intent) {

					finish();
				}
			};
		}
		registerReceiver(broadcastReceiver, intentFilter);
		this.isSuspendedActivity = false;
	}

	/**
	 * Appelée lorsque l'activité a fini son cycle de vie. C'est ici que nous
	 * placerons notre de libération de mémoire, fermeture de fichiers et autres
	 * opérations de "nettoyage".
	 */
	@Override
	protected void onDestroy() {

		if (broadcastReceiver != null) {
			unregisterReceiver(broadcastReceiver);
		}
		super.onDestroy();
	}

	/**
	 * Appelée lorsque l'activité passe en arrière plan; Libérez les écouteurs,
	 * arrêtez les threads, votre activité peut disparaître de la mémoire.
	 */
	@Override
	protected void onStop() {

		this.isSuspendedActivity = true;
		super.onStop();
	}

	/**
	 * Appelée lorsque l'activité sort de son état de veille.
	 */
	@Override
	protected void onRestart() {

		super.onRestart();
		this.isSuspendedActivity = false;
	}

	/**
	 * Appelée lorsque l'activité est suspendue. Stoppez les actions qui
	 * consomment des ressources. L'activité va passer en arrière-plan.
	 */
	@Override
	protected void onPause() {

		this.isSuspendedActivity = true;
		super.onPause();
	}

	/**
	 * Appelée après le démarrage ou une pause. Relancez les opérations arrêtés
	 * (threads). Mettez à jour votre application et vérifiez vos écouteurs.
	 */
	@Override
	protected void onResume() {

		super.onResume();
		this.isSuspendedActivity = false;
	}

	/**
	 * Lance le chargement des données externes.
	 */
	public void loadData() {
		return;
	}

	/**
	 * Renvoie à la page d'accueil.
	 */
	public void returnHome() {

		Activity activity = this;
		Intent intent = new Intent(activity, GuideHomeActivity.class);
		intent.putExtra("timeToWait",1000);
		startActivity(intent);
	}

	/**
	 * Gets the handler used to post runnables in the main thread
	 * 
	 * @return the handler
	 */
	public Handler getHandler() {
		return handler;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.player.services.ServiceObserver#dataChanged(java.lang
	 * .Object, java.lang.Integer)
	 */
	@Override
	public void dataChanged(Object o, Integer taskId) {
		return;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.player.services.ServiceObserver#errorTreatment(java
	 * .lang.Throwable, java.lang.Integer)
	 */
	@Override
	public void errorTreatment(final Throwable e, Integer taskId) {

		if (!this.isSuspendedActivity) {

			if (e instanceof ConnectionConstellationException) {
				DialogUtils.showConnectionError(this, getHandler());
			} else {
				DialogUtils.showMessageError(this, getHandler());
			}
		}
	}
}
