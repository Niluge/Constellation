package com.constellation.core.activity;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.constellation.core.R;
import com.constellation.core.adapter.PictoHomeAdapter;
import com.constellation.core.manager.ConstellationManager;
import com.constellation.core.model.Project;
import com.constellation.core.model.Rubric;
import com.constellation.core.model.Template;
import com.constellation.core.model.Theme;
import com.constellation.core.model.ThemeType;
import com.constellation.core.view.HeaderLayout;

/**
 * Cette activité permet d'afficher la liste des thèmes d'un guide.
 * 
 * Les paramètres d'entrée sont : PROJECT Le guide IS_HOME_PAGE Etat de la page.
 * 
 * @author j.varin, j.attal
 * 
 */
public class ThemeActivity extends AbstractActivity {

	/**
	 * Identifiant de la tache de téléchargement du guide.
	 */
	private Integer downloadTaskId;

	/**
	 * Gestionnaire des données contellation.
	 */
	private ConstellationManager constellationManager;

	/**
	 * Le nom du paramètre par lequel on passe le guide.
	 */
	public static final String PROJECT = "project";

	/**
	 * Le nom du paramètre par lequel on passe l'état de la page.
	 */
	public static final String IS_HOME_PAGE = "isHomePage";

	/**
	 * Le guide Ã  afficher.
	 */
	private Project project;

	/**
	 * Indique si la page est la Home Page.
	 */
	private boolean isHomePage;

	/**
	 * Header de la page.
	 */
	private HeaderLayout header;

	/**
	 * Vue de la grille de rubriques.
	 */
	private GridView themesGridview;

	/**
	 * Fond de la page.
	 */
	private LinearLayout background;

	/**
	 * Le thème courant.
	 */
	private Theme currentTheme;

	/**
	 * Dialog de téléchargement
	 */
	private Dialog downloadingDialog;

	/**
	 * Dialog de téléchargement
	 */
	private ProgressBar progressDownloading;

	/**
	 * Dialog de téléchargement
	 */
	private int nbRubrics;

	private PictoHomeAdapter pictoHomeAdapter;

	private int currentLine = 0;
	private Theme divider;
	private Theme empty;

	/**
	 * Appelée lorsque l'activité est créée. Permet de restaurer l'état de
	 * l'interface utilisateur grÃ¢ce au paramètre savedInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.themes);
		initView();

		loadData();
	}

	/**
	 * Initialise l'ensemble des vues contenues dans la page.
	 */
	private void initView() {

		header = (HeaderLayout) findViewById(R.id.themes_header);
		background = (LinearLayout) findViewById(R.id.main);

		themesGridview = (GridView) findViewById(R.id.themes_grid);
		themesGridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				currentTheme = (Theme) pictoHomeAdapter.getItem(position);
				ThemeType type = currentTheme.getType();

				Log.d(getClass().getCanonicalName(), "Type de thème : " + type);
				// Liste des rubriques
				if (type.equals(ThemeType.Rubric)) {

					Intent myIntent = new Intent(getBaseContext(),
							HeadingActivity.class);
					myIntent.putExtra(HeadingActivity.PROJECT, project);
					myIntent.putExtra(HeadingActivity.THEME, currentTheme);
					startActivity(myIntent);
				}

				// Around me
				if (type.equals(ThemeType.AroundMe)) {

					Intent myIntent = new Intent(getBaseContext(),
							CloseOIsMapActivity.class);
					myIntent.putExtra(CloseOIsMapActivity.PROJECT, project);
					myIntent.putExtra(CloseOIsMapActivity.THEME, currentTheme);
					startActivity(myIntent);
				}

				// Webview
				if (type.equals(ThemeType.Web)) {
					Intent myIntent = new Intent(getBaseContext(),
							WebActivity.class);
					myIntent.putExtra(CloseOIsMapActivity.PROJECT, project);
					myIntent.putExtra(CloseOIsMapActivity.THEME, currentTheme);
					myIntent.putExtra("url", currentTheme.getUrl());
					startActivity(myIntent);
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.constellation.core.activity.AbstractActivity#loadData()
	 */
	@Override
	public void loadData() {

		project = (Project) getIntent().getParcelableExtra(PROJECT);
		isHomePage = getIntent().getBooleanExtra(IS_HOME_PAGE, false);
		if (project.getTemplate() != null) {
			Template tmp = project.getTemplate();
			if (tmp.equals(Template.Orange)) {
				background.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_orange));
			} else if (tmp.equals(Template.Blue)) {

				background.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_blue));
			} else {
				background.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.back_green));
			}
			header.setBlur(tmp);
		}

		divider = new Theme();
		divider.setName("-----");
		divider.setType(ThemeType.Separator);
		divider.setDesc("divider");
		empty = new Theme();
		empty.setName("-----");
		empty.setType(ThemeType.Separator);
		empty.setDesc("empty");

		displayHome();
	}

	/**
	 * Affiche la page.
	 */
	protected void displayHome() {

		header.setHeaderUrl(project.getHeader());
		ArrayList<Theme> itemsHome = new ArrayList<Theme>();
		for (int i = 0; i < project.getThemes().size(); i++) {
			Theme item = project.getThemes().get(i);

			if (currentLine == 0) {
				if (item.getType() == ThemeType.Separator) {
					
					itemsHome.add(divider);
					itemsHome.add(divider);
				} else {
					if ((item.getType() != ThemeType.AroundMe)
							|| (project.isActiveArroundMe())) {
						
						itemsHome.add(item);
						currentLine = 1;
					}
				}
			} else {
				if (item.getType().equals(ThemeType.Separator)) {
					
					itemsHome.add(empty);
					itemsHome.add(divider);
					itemsHome.add(divider);
				} else if ((item.getType() != ThemeType.AroundMe)
						|| (project.isActiveArroundMe())) {
					itemsHome.add(item);
				}
				currentLine = 0;
			}
		}
		for (int i = 0; i < itemsHome.size(); i++) {
			Log.d("*****itemsHome*****", itemsHome.get(i).getDesc());
		}
		pictoHomeAdapter = new PictoHomeAdapter(this, itemsHome, getHandler());
		themesGridview.setAdapter(pictoHomeAdapter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		if (isHomePage) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.menu_home, menu);
		}
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (getString(R.string.standalone).equals("true")) {
			// menu.removeItem(R.id.menu_download); //TODO
		}
		return super.onPrepareOptionsMenu(menu);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.menu_legal_info) {
			Dialog dialog = new Dialog(ThemeActivity.this);
			dialog.setContentView(R.layout.copyright);
			dialog.setTitle(R.string.copyright_title);
			dialog.show();
			return true;
		} else if (item.getItemId() == R.id.menu_download) {
			downloadProject();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	private void downloadProject() {
		nbRubrics = 0;
		for (Theme theme : project.getThemes()) {
			if (theme.getRubrics() != null) {
				for (@SuppressWarnings("unused")
				Rubric rubric : theme.getRubrics()) {
					nbRubrics++;
				}
			}
		}

		constellationManager = ConstellationManager
				.getConstellationManager(this);
		constellationManager.addListener(this);
		downloadTaskId = constellationManager.downloadProject(project.getId());
		downloadingDialog = new Dialog(ThemeActivity.this);
		downloadingDialog.setTitle(R.string.downloading);
		downloadingDialog.setContentView(R.layout.downloading);
		downloadingDialog.show();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.core.activity.AbstractActivity#dataChanged(java.lang
	 * .Object, java.lang.Integer)
	 */
	@Override
	public void dataChanged(final Object o, final Integer taskId) {

		getHandler().post(new Runnable() {
			@Override
			public void run() {

				Log.d(getClass().getCanonicalName(), "Data changed");
				if (taskId.equals(downloadTaskId)) {
					if (o instanceof Project) {
						Project p = (Project) o;
						Log.d(getClass().getSimpleName(),
								"Got Project " + p.getTitle());
						progressDownloading = (ProgressBar) downloadingDialog
								.findViewById(R.id.progressBar);
						progressDownloading.setProgress(10);

					}
					if (o instanceof String) {
						Log.d(getClass().getSimpleName(), "Got Progress");
						String progress = (String) o;

						if (progress.equals("end")) {
							progressDownloading.setProgress(100);
							Toast.makeText(getBaseContext(),
									getString(R.string.downloading_end), 5000);
							Log.d(getClass().getSimpleName(), "END");
							downloadingDialog.dismiss();
						} else {
							int cpt = Integer.parseInt(progress);
							progressDownloading.setProgress(10
									+ (90 / nbRubrics) * cpt);
						}
					}
				}
			}
		});
	}
}
