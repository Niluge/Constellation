package com.constellation.core.services;

/**
 * Interface qui permet la gestion asynchrone de la récupération des données.
 * 
 * @author j.varin
 * 
 */
public interface ServiceObserver {

	/**
	 * Signale à l'observer la récupération de l'objet passé en paramètre.
	 * 
	 * @param o
	 *            L'objet récupérée.
	 * @param taskId
	 *            L'identifiant de la tâche.
	 */
	void dataChanged(Object o, Integer taskId);

	/**
	 * Signale à l'observer que la récupération de l'objet a échoué.
	 * 
	 * @param e
	 *            L'exception retournée.
	 * @param taskId
	 *            L'identifiant de la tâche.
	 */
	void errorTreatment(Throwable e, Integer taskId);
}
