package com.constellation.core.services;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.type.TypeReference;

import android.content.Context;

import com.constellation.core.exception.ConnectionConstellationException;
import com.constellation.core.exception.ConstellationException;
import com.constellation.core.model.OI;
import com.constellation.core.model.OIs;
import com.constellation.core.model.Project;
import com.constellation.core.model.Projets;
import com.constellation.core.utils.FileUtils;

/**
 * Service de récupération des données relatives aux applications Constellation
 * depuis les Web Service.
 * 
 * @author j.varin
 * @see Project
 * @see OI
 */
public class WebAccessService {

	private Context context;

	/**
	 * URL de recette du Web Service (A mettre dans fichier conf).
	 */
	// private final static String CONSTELLATION_WEB_SERVICE_REC =
	// "wssandbox.enjoyconstellation.com/";

	/**
	 * URL de production du Web Service (A mettre dans fichier conf).
	 */
	private final static String CONSTELLATION_WEB_SERVICE_PROD = "wsmobile.enjoyconstellation.com/";

	/**
	 * URL du service Constellation.
	 */
	private final static String CONSTELLATION_WEB_SERVICE_URL = "http://"
			+ CONSTELLATION_WEB_SERVICE_PROD + "DMSFluxSV.asmx/";

	/**
	 * Nom de la méthode de récupération des projets.
	 */
	private static final String GET_MOBILE_PROJECTS_BY_CODE = "DMSGetMobileProjectsByCodeWithTheme";

	/**
	 * Nom de la méthode de récupération d'un projet.
	 */
	private static final String GET_MOBILE_PROJECT_BY_ID = "DMSGetMobileProjectByIdWithTheme";

	/**
	 * Nom de la méthode de récupération d'une liste d'identifants d'OI.
	 */
	private static final String GET_LISTE_FROM_PLAYLIST_RUBS = "DMSGetListeFromPlayListRubs";

	/**
	 * Nom de la méthode de récupération d'une liste d'identifants d'OI proche
	 * d'un point donné.
	 */
	private static final String GET_LISTE_FROM_PLAYLIST_RUBS_AROUND = "DMSGetListeFromPlayListRubsAround";

	/**
	 * Nom de la méthode de récupération d'une liste d'OIs.
	 */
	private static final String GET_OIS_FROM_LISTE = "DMSGetOisFromListJSONMobile";

	/**
	 * Nom du paramètre ProjectCode.
	 */
	private static final String PARAM_PROJECT_CODE = "projectCode";

	/**
	 * Nom du paramètre ProjectId.
	 */
	private static final String PARAM_PROJECT_ID = "projectId";

	/**
	 * Nom du paramètre idLang.
	 */
	private static final String PARAM_LANG_ID = "idLang";

	/**
	 * Nom du paramètre idEct.
	 */
	private static final String PARAM_ECT_ID = "IdEct";

	/**
	 * Nom du paramètre idPlaylist.
	 */
	private static final String PARAM_PLAYLIST_ID = "IdPlaylist";

	/**
	 * Nom du paramètre sort.
	 */
	private static final String PARAM_SORT = "Sort";

	/**
	 * Nom du paramètre idRubriques.
	 */
	private static final String PARAM_RUBRICS_ID = "IdRubriques";

	/**
	 * Nom du paramètre OisList.
	 */
	private static final String PARAM_OIS_LIST = "OisList";

	/**
	 * Nom du paramètre IdLangResult.
	 */
	private static final String PARAM_ID_LANG_RESULT = "IdLangResult";

	/**
	 * Nom du paramètre typeOI.
	 */
	private static final String PARAM_TYPE_OI = "typeOI";

	/**
	 * Nom du paramètre X.
	 */
	private static final String PARAM_X = "X";

	/**
	 * Nom du paramètre Y.
	 */
	private static final String PARAM_Y = "Y";

	/**
	 * Nom du paramètre KmRayon.
	 */
	private static final String PARAM_KM_RAYON = "KmRayon";

	/**
	 * Valeur du paramètre idLang pour le français.
	 */
	private static final String FRENCH_LANG_ID = "134";

	/**
	 * Valeur du paramètre sort pour l'aléatoire.
	 */
	private static final String RANDOM_SORT = "1";

	public WebAccessService(Context context) {

		this.context = context;
	}

	/**
	 * Retourne tous les projets mobiles publiés.
	 * 
	 * @return La liste de projets.
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public List<Project> getProjects() throws ConstellationException {

		// Construction des paramètres.
		List<NameValuePair> getProjectsByCodeParam = new ArrayList<NameValuePair>();
		getProjectsByCodeParam.add(new BasicNameValuePair(PARAM_PROJECT_CODE,
				""));

		// Récupération du résultat.
		return callWebService(GET_MOBILE_PROJECTS_BY_CODE,
				getProjectsByCodeParam, new TypeReference<Projets>() {
				}).getProjects();
	}

	/**
	 * Retourne les détails d'un projet spécifique depuis le Web Service
	 * Constellation.
	 * 
	 * @param projectId
	 *            L'identifiant du projet que l'on veut récupérer.
	 * @return Les détails du projet.
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public Project getProjectById(String projectId)
			throws ConstellationException {

		// Construction des paramètres.
		List<NameValuePair> getProjectByIdParams = new ArrayList<NameValuePair>();
		getProjectByIdParams.add(new BasicNameValuePair(PARAM_PROJECT_ID,
				projectId));
		getProjectByIdParams.add(new BasicNameValuePair(PARAM_LANG_ID,
				FRENCH_LANG_ID));

		// Récupération du résultat.
		return callWebService(GET_MOBILE_PROJECT_BY_ID, getProjectByIdParams,
				new TypeReference<Project>() {
				});
	}

	/**
	 * Retourne les détails d'un projet spécifique depuis le Web Service
	 * Constellation et enregistre le fichier JSON associé.
	 * 
	 * @param projectId
	 *            L'identifiant du projet que l'on veut récupérer.
	 * @return Les détails du projet.
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public Project downloadProject(String projectId)
			throws ConstellationException {

		// Construction des paramètres.
		List<NameValuePair> getProjectByIdParams = new ArrayList<NameValuePair>();
		getProjectByIdParams.add(new BasicNameValuePair(PARAM_PROJECT_ID,
				projectId));
		getProjectByIdParams.add(new BasicNameValuePair(PARAM_LANG_ID,
				FRENCH_LANG_ID));

		// Récupération du résultat.
		return callAndRecordWebService(GET_MOBILE_PROJECT_BY_ID, projectId,
				getProjectByIdParams, new TypeReference<Project>() {
				});
	}

	/**
	 * Retourne la liste des identifiants des OIs de la rubrique.
	 * 
	 * 
	 * @param ectId
	 *            L'identifiant ECT du guide.
	 * @param playlistId
	 *            L'identifiant de la playlist associée au guide.
	 * @param rubricId
	 *            La liste des rubriques de la playlist.
	 * @return La liste des identifiants des OIs
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public List<String> getOIsIdListByRubricId(String ectId, String playlistId,
			String rubricId) throws ConstellationException {

		// Construction des paramètres.
		List<NameValuePair> getPlaylistRubsParams = new ArrayList<NameValuePair>();
		getPlaylistRubsParams.add(new BasicNameValuePair(PARAM_ECT_ID, ectId));
		getPlaylistRubsParams.add(new BasicNameValuePair(PARAM_PLAYLIST_ID,
				playlistId));
		getPlaylistRubsParams.add(new BasicNameValuePair(PARAM_SORT,
				RANDOM_SORT));
		getPlaylistRubsParams.add(new BasicNameValuePair(PARAM_RUBRICS_ID,
				rubricId));

		InputStream inputStream = getInputStream(GET_LISTE_FROM_PLAYLIST_RUBS,
				getPlaylistRubsParams);
		String xmlStream = FileUtils.getJSONStream(inputStream);

		// Récupération de la liste des identifiants des OIs.
		String oisListStr = FileUtils.getJSONStream(new ByteArrayInputStream(
				xmlStream.getBytes()));

		StringTokenizer oisListStrToken = new StringTokenizer(oisListStr, "|");
		List<String> oisList = new ArrayList<String>();

		while (oisListStrToken.hasMoreElements()) {
			oisList.add((String) oisListStrToken.nextElement());
		}

		return oisList;
	}

	/**
	 * Retourne les informations détaillées d'un ou plusieurs OIs depuis le Web
	 * Service Constellation.
	 * 
	 * @param ectId
	 *            L'identifiant ECT du guide.
	 * @param oisList
	 *            La liste des identifiants des OIs.
	 * @return La liste détaillée des OIs.
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public List<OI> getOIsByIdList(String ectId, String oisList)
			throws ConstellationException {

		// Construction des paramètres.
		List<NameValuePair> getOisFromListArgs = new ArrayList<NameValuePair>();
		getOisFromListArgs.add(new BasicNameValuePair(PARAM_ECT_ID, ectId));
		getOisFromListArgs.add(new BasicNameValuePair(PARAM_OIS_LIST, oisList));
		getOisFromListArgs.add(new BasicNameValuePair(PARAM_ID_LANG_RESULT,
				FRENCH_LANG_ID));

		// Récupération de la liste des OIs.
		return callWebService(GET_OIS_FROM_LISTE, getOisFromListArgs,
				new TypeReference<OIs>() {
				}).getOIs();
	}

	/**
	 * Retourne les informations détaillées d'un ou plusieurs OIs depuis le Web
	 * Service Constellation.
	 * 
	 * @param ectId
	 *            L'identifiant ECT du guide.
	 * @param oisList
	 *            La liste des identifiants des OIs.
	 * @return La liste détaillée des OIs.
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public void downloadOIsByIdList(String ectId, String oisList,
			String rubricId) throws ConstellationException {

		// Construction des paramètres.
		List<NameValuePair> getOisFromListArgs = new ArrayList<NameValuePair>();
		getOisFromListArgs.add(new BasicNameValuePair(PARAM_ECT_ID, ectId));
		getOisFromListArgs.add(new BasicNameValuePair(PARAM_OIS_LIST, oisList));
		getOisFromListArgs.add(new BasicNameValuePair(PARAM_ID_LANG_RESULT,
				FRENCH_LANG_ID));

		// Récupération de la liste des OIs.
		recordWebService(GET_OIS_FROM_LISTE, getOisFromListArgs, rubricId);
	}

	/**
	 * Enregistre un fichier vide pour la rubrique donnée.
	 * 
	 * @param rubricId
	 *            L'identifiant de la rubrique
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public void recordEmptyFile(String rubricId) throws ConstellationException {

		try {
			this.context.openFileOutput("Rubric_" + rubricId + ".json",
					Context.MODE_PRIVATE);
		} catch (IOException e) {
			throw new ConstellationException("Cannot read JSON stream.", e);
		}
	}

	/**
	 * Retourne les informations détaillées d'un ou plusieurs OIs proche du
	 * point d'origine dans un rayon donné.
	 * 
	 * @param ectId
	 *            L'identifiant ECT du guide.
	 * @param playlistId
	 *            L'identifiant de la playlist associée au guide.
	 * @param rubricId
	 *            La liste des rubriques de la playlist.
	 * @param idType
	 *            Le type des OIs.
	 * @param coordX
	 *            La latitude du point d'origine.
	 * @param coordY
	 *            La longitude du point d'origine.
	 * @param radius
	 *            Le rayon d'action.
	 * @return La liste détaillée des OIs.
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public List<String> getOIsByRubricIdAround(String ectId, String playlistId,
			String rubricId, String idType, String coordX, String coordY,
			String radius) throws ConstellationException {

		// Construction des paramètres.
		List<NameValuePair> getPlaylistRubsAroundParams = new ArrayList<NameValuePair>();
		getPlaylistRubsAroundParams.add(new BasicNameValuePair(PARAM_ECT_ID,
				ectId));
		getPlaylistRubsAroundParams.add(new BasicNameValuePair(
				PARAM_PLAYLIST_ID, playlistId));
		getPlaylistRubsAroundParams.add(new BasicNameValuePair(
				PARAM_RUBRICS_ID, rubricId));
		getPlaylistRubsAroundParams.add(new BasicNameValuePair(PARAM_TYPE_OI,
				idType));
		getPlaylistRubsAroundParams
				.add(new BasicNameValuePair(PARAM_X, coordX));
		getPlaylistRubsAroundParams
				.add(new BasicNameValuePair(PARAM_Y, coordY));
		getPlaylistRubsAroundParams.add(new BasicNameValuePair(PARAM_KM_RAYON,
				radius));

		InputStream inputStream = getInputStream(
				GET_LISTE_FROM_PLAYLIST_RUBS_AROUND,
				getPlaylistRubsAroundParams);
		String xmlStream = FileUtils.getJSONStream(inputStream);

		// Récupération de la liste des identifiants des OIs.
		String oisListStr = FileUtils.getJSONStream(new ByteArrayInputStream(
				xmlStream.getBytes()));

		if ((oisListStr == null) || (oisListStr.length() == 0)) {
			return new ArrayList<String>();
		}

		StringTokenizer oisListStrToken = new StringTokenizer(oisListStr, "|");
		List<String> oisList = new ArrayList<String>();

		while (oisListStrToken.hasMoreElements()) {

			String element = (String) oisListStrToken.nextElement();
			StringTokenizer elementStrToken = new StringTokenizer(element, ";");
			elementStrToken.nextElement();
			elementStrToken.nextElement();
			oisList.add((String) elementStrToken.nextElement());
		}

		return oisList;
	}

	/**
	 * Retourne le résultat de la requête faite au Web Service Constellation.
	 * 
	 * @param <T>
	 *            Le type de l'objet à manipuler
	 * @param method
	 *            Le nom de la méthode.
	 * @param params
	 * 
	 * @param typeReference
	 *            Le type de retour souhaité.
	 * @return L'objet résultant de l'appel du Web Service.
	 * @throws ConstellationException
	 *             Une erreur s'est lors de la récupération des données.
	 */
	@SuppressWarnings("unchecked")
	private <T> T callWebService(String method, List<NameValuePair> params,
			TypeReference<T> typeReference) throws ConstellationException {

		// Récupération du flux à parser.
		InputStream inputStream = getInputStream(method, params);

		try {

			// Conversion du flux en String.
			String mainJSONStream = FileUtils.getJSONStream(inputStream);

			// Traitement de la réponse.
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			T result = (T) mapper.readValue(mainJSONStream, typeReference);

			// Fermeture du flux.
			inputStream.close();

			return result;
		} catch (IOException e) {
			throw new ConstellationException("Cannot read JSON stream.", e);
		}
	}

	/**
	 * Retourne le résultat de la requête faite au Web Service Constellation.
	 * 
	 * @param <T>
	 *            Le type de l'objet à manipuler
	 * @param method
	 *            Le nom de la méthode.
	 * @param params
	 * 
	 * @param typeReference
	 *            Le type de retour souhaité.
	 * @return L'objet résultant de l'appel du Web Service.
	 * @throws ConstellationException
	 *             Une erreur s'est lors de la récupération des données.
	 */
	@SuppressWarnings("unchecked")
	private <T> T callAndRecordWebService(String method, String projectId,
			List<NameValuePair> params, TypeReference<T> typeReference)
			throws ConstellationException {

		// Récupération du flux à parser.
		InputStream inputStream = getInputStream(method, params);

		try {
			FileOutputStream fos = this.context.openFileOutput("Project_"
					+ projectId + ".json", Context.MODE_PRIVATE);
			byte buf[] = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0) {
				fos.write(buf, 0, len);
			}
			fos.close();
			inputStream.close();

			FileInputStream fis = this.context.openFileInput("Project_"
					+ projectId + ".json");

			// Conversion du flux en String.
			String mainJSONStream = FileUtils.getJSONStream(fis);

			// Traitement de la réponse.
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			T result = (T) mapper.readValue(mainJSONStream, typeReference);

			// Fermeture du flux.
			fis.close();

			return result;
		} catch (IOException e) {
			throw new ConstellationException("Cannot read JSON stream.", e);
		}
	}

	/**
	 * Retourne le résultat de la requête faite au Web Service Constellation.
	 * 
	 * @param <T>
	 *            Le type de l'objet à manipuler
	 * @param method
	 *            Le nom de la méthode.
	 * @param params
	 * 
	 * @param typeReference
	 *            Le type de retour souhaité.
	 * @return L'objet résultant de l'appel du Web Service.
	 * @throws ConstellationException
	 *             Une erreur s'est lors de la récupération des données.
	 */
	private void recordWebService(String method, List<NameValuePair> params,
			String rubricId) throws ConstellationException {

		// Récupération du flux à parser.
		InputStream inputStream = getInputStream(method, params);

		try {

			FileOutputStream fos = this.context.openFileOutput("Rubric_"
					+ rubricId + ".json", Context.MODE_PRIVATE);
			byte buf[] = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0) {
				fos.write(buf, 0, len);
			}
			fos.close();
			inputStream.close();

		} catch (IOException e) {
			throw new ConstellationException("Cannot read JSON stream.", e);
		}
	}

	/**
	 * Retourne le flux retourné par le Web Service Constellation.
	 * 
	 * @param method
	 *            Le nom de la méthode.
	 * @param params
	 *            Les paramètres.
	 * @return Le flux résultant de l'appel du Web Service.
	 * @throws ConstellationException
	 *             Une erreur s'est lors de la récupération des données.
	 */
	private InputStream getInputStream(String method, List<NameValuePair> params)
			throws ConstellationException {

		ConnexionService connexionService = ConnexionService
				.getConnexionService();
		HttpClient httpClient = connexionService.getHttpClient();

		// Création de l'URL d'appel.
		StringBuilder urlBuilder = new StringBuilder();
		urlBuilder.append(CONSTELLATION_WEB_SERVICE_URL);
		urlBuilder.append(method);

		try {

			// Création de la requête
			HttpPost httpPost = new HttpPost(urlBuilder.toString());
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			httpPost.setHeader("Content-type",
					"application/x-www-form-urlencoded");

			// Récupération du résultat.
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();

			return httpEntity.getContent();
		} catch (UnsupportedEncodingException e) {
			throw new ConstellationException("Cannot read params.", e);
		} catch (ClientProtocolException e) {
			throw new ConnectionConstellationException(
					"Cannot connect to Web Service.", e);
		} catch (SocketException e) {
			throw new ConnectionConstellationException(
					"Cannot connect to Web Service.", e);
		} catch (SocketTimeoutException e) {
			throw new ConnectionConstellationException(
					"Connection to Web Service timeout.", e);
		} catch (UnknownHostException e) {
			throw new ConnectionConstellationException(
					"Cannot connect to Web Service.", e);
		} catch (IOException e) {
			throw new ConstellationException("Cannot read JSON stream.", e);
		}
	}
}
