package com.constellation.core.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.type.TypeReference;

import android.content.Context;
import android.util.Log;

import com.constellation.core.exception.ConnectionConstellationException;
import com.constellation.core.exception.ConstellationException;
import com.constellation.core.model.OI;
import com.constellation.core.model.Project;
import com.constellation.core.utils.FileUtils;

/**
 * Service permettant à l'accès aux différents guides et à leur rubriques.
 * 
 * @author j.varin
 * 
 */
public class ConstellationService {

	private Context context;

	public ConstellationService(Context context) {
		super();
		this.context = context;
	}

	public Project downloadProject(String id) throws ConstellationException {

		WebAccessService webAccessService = new WebAccessService(this.context);
		return webAccessService.downloadProject(id);
	}

	public void downloadOIFromRubric(String ectId, String playlistId,
			String rubricId) throws ConstellationException {

		WebAccessService webAccessService = new WebAccessService(this.context);
		List<String> oisList = webAccessService.getOIsIdListByRubricId(ectId,
				playlistId, rubricId);
		Log.e(getClass().getSimpleName(), "oisList : " + oisList); // TODO

		StringBuffer stringBuffer = new StringBuffer();
		boolean isFirst = true;
		for (Iterator<String> iterator = oisList.iterator(); iterator.hasNext();) {

			String id = iterator.next();
			if (isFirst == false) {
				stringBuffer.append("|");
			} else {
				isFirst = false;
			}
			stringBuffer.append(id);
		}
		Log.e(getClass().getSimpleName(), "stringBuffer : " + stringBuffer); // TODO
		if (stringBuffer.length() > 0) {
			webAccessService.downloadOIsByIdList(ectId, stringBuffer.toString(),
				rubricId);
		}
		else {
			webAccessService.recordEmptyFile(rubricId);
		}
	}

	/**
	 * Retourne tous les projets mobiles publiés.
	 * 
	 * @return La liste de projets.
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public List<Project> getProjects() throws ConstellationException {

		List<Project> result = new ArrayList<Project>();
		WebAccessService webAccessService = new WebAccessService(this.context);
		try {

			List<Project> wsResult = webAccessService.getProjects();
			for (Project project : wsResult) {

				if (FileUtils.isProjectExists(this.context, project.getId())) {
					Project fileProject = FileUtils.getObject("Project_"
							+ project.getId() + ".json", context,
							new TypeReference<Project>() {
							});
					fileProject.setStore(true);
					result.add(fileProject);
				} else {
					result.add(project);
				}
			}
		} catch (ConnectionConstellationException e) {
			result = FileUtils.getProjects(this.context);
			if (result == null) {
				throw e;
			}
		}

		return result;
	}

	/**
	 * Retourne les détails d'un projet spécifique.
	 * 
	 * @param id
	 *            L'identifiant du projet que l'on veut récupérer.
	 * @return Les détails du projet.
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public Project getProjectById(String id) throws ConstellationException {

		Project result = null;
		if (FileUtils.isProjectExists(this.context, id)) {
			result = FileUtils.getObject("Project_" + id + ".json", context,
					new TypeReference<Project>() {
					});
		}

		if (result != null) {
			return result;
		} else {

			WebAccessService webAccessService = new WebAccessService(
					this.context);
			result = webAccessService.getProjectById(id);
		}

		return result;
	}

	/**
	 * Retourne la liste des identifiants des OIs de la rubrique.
	 * 
	 * 
	 * @param ectId
	 *            L'identifiant ECT du guide.
	 * @param playlistId
	 *            L'identifiant de la playlist associée au guide.
	 * @param rubricId
	 *            La liste des rubriques de la playlist.
	 * @return La liste des identifiants des OIs
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public List<String> getOIsIdListByRubricId(String ectId, String playlistId,
			String rubricId) throws ConstellationException {

		WebAccessService webAccessService = new WebAccessService(this.context);
		return webAccessService.getOIsIdListByRubricId(ectId, playlistId,
				rubricId);
	}

	/**
	 * Retourne les informations détaillées d'un ou plusieurs OIs.
	 * 
	 * @param ectId
	 *            L'identifiant ECT du guide.
	 * @param rubricId
	 *            La liste des rubriques de la playlist.
	 * @return La liste détaillée des OIs.
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public List<OI> getOIsByRubricId(String ectId, String oisList)
			throws ConstellationException {

		WebAccessService webAccessService = new WebAccessService(this.context);
		return webAccessService.getOIsByIdList(ectId, oisList);
	}

	/**
	 * Retourne les informations détaillées d'un ou plusieurs OIs proche du
	 * point d'origine dans un rayon donné.
	 * 
	 * @param ectId
	 *            L'identifiant ECT du guide.
	 * @param playlistId
	 *            L'identifiant de la playlist associée au guide.
	 * @param rubricId
	 *            La liste des rubriques de la playlist.
	 * @param idType
	 *            Le type des OIs.
	 * @param coordX
	 *            La latitude du point d'origine.
	 * @param coordY
	 *            La longitude du point d'origine.
	 * @param radius
	 *            Le rayon d'action.
	 * @return La liste détaillée des OIs.
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public List<String> getOIsByRubricIdAround(String ectId, String playlistId,
			String rubricId, String idType, String coordX, String coordY,
			String radius) throws ConstellationException {

		WebAccessService webAccessService = new WebAccessService(this.context);
		return webAccessService.getOIsByRubricIdAround(ectId, playlistId,
				rubricId, idType, coordX.replaceAll(",", "."),
				coordY.replaceAll(",", "."), radius);
	}
}
