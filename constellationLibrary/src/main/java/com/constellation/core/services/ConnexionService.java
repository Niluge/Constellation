package com.constellation.core.services;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

/**
 * Service de gestion des connexions au Web Service.
 * 
 * @author j.varin
 * 
 */
public class ConnexionService {

	/**
	 * L'instance unique de ConnexionService.
	 */
	private static ConnexionService SINGLETON = new ConnexionService();

	/**
	 * Paramètres HTTP.
	 */
	private HttpParams httpParameters;

	/**
	 * Le manager de connexion.
	 */
	private ClientConnectionManager connexionManager;

	/**
	 * L'instance unique de ConnexionService.
	 * 
	 * @return Le singleton.
	 */
	public static ConnexionService getConnexionService() {
		return SINGLETON;
	}

	/**
	 * Constructeur par défaut.
	 */
	private ConnexionService() {

		this.httpParameters = new BasicHttpParams();
		int timeoutConnection = 8000;
		HttpConnectionParams.setConnectionTimeout(this.httpParameters,
				timeoutConnection);
		int timeoutSocket = 60000;
		HttpConnectionParams.setSoTimeout(this.httpParameters, timeoutSocket);

		SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		registry.register(new Scheme("https", SSLSocketFactory
				.getSocketFactory(), 443));

		this.connexionManager = new ThreadSafeClientConnManager(httpParameters,
				registry);
	}

	/**
	 * Retourne un client HTTP de connexion.
	 * 
	 * @return Un client HTTP par défaut.
	 */
	public HttpClient getHttpClient() {
		return new DefaultHttpClient(this.connexionManager, this.httpParameters);
	}
}
