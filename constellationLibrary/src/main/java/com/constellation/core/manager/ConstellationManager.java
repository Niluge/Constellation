package com.constellation.core.manager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.type.TypeReference;

import android.content.Context;
import android.util.Log;

import com.constellation.core.exception.ConstellationException;
import com.constellation.core.model.OI;
import com.constellation.core.model.OIs;
import com.constellation.core.model.Project;
import com.constellation.core.model.Rubric;
import com.constellation.core.model.Theme;
import com.constellation.core.services.ConstellationService;
import com.constellation.core.utils.FileUtils;
import com.constellation.core.utils.Task;

/**
 * Permet de lancer les tâches de {@link ConstellationService} de manière
 * asynchrone.
 * 
 * @author j.varin
 * 
 */
public class ConstellationManager extends AbstractManager {

	/**
	 * Singleton du manager
	 */
	private static ConstellationManager SINGLETON;

	private Context context;

	/**
	 * Le service associé.
	 */
	private ConstellationService constellationService;

	private Map<String, List<String>> oiIdMap;

	/**
	 * Cache des objets project
	 */
	private Map<String, WeakReference<Project>> projectCache;

	/**
	 * Cache des objets oi
	 */
	private Map<String, WeakReference<OI>> oiCache;

	/**
	 * Constructeur privé.
	 */
	private ConstellationManager(Context context) {

		this.context = context;
		constellationService = new ConstellationService(this.context);
		oiIdMap = new HashMap<String, List<String>>();
		projectCache = new HashMap<String, WeakReference<Project>>();
		oiCache = new HashMap<String, WeakReference<OI>>();
	}

	/**
	 * Retourne l'instance unique de ConstellationManager.
	 * 
	 * @return Le singleton ConstellationManager.
	 */
	public static ConstellationManager getConstellationManager(Context context) {

		if (SINGLETON == null) {
			SINGLETON = new ConstellationManager(context);
		}

		return SINGLETON;
	}

	public Integer getOI(final String rubricId, final String playlistId,
			final String ectId, final int position) {

		final Integer taskId = new Date().hashCode();
		Runnable runnable = new Runnable() {
			public void run() {
				try {

					OI oi;
					if (FileUtils.isRubricExists(context, rubricId)) {

						List<OI> ois = FileUtils.getObject(
								"Rubric_" + rubricId + ".json", context,
								new TypeReference<OIs>() {
								}).getOIs();

						oi = ois.get(position);
					} else {

						List<String> oisList = new ArrayList<String>();
						if (oiIdMap.containsKey(rubricId)) {
							oisList = oiIdMap.get(rubricId);
						} else {

							oisList = constellationService
									.getOIsIdListByRubricId(ectId, playlistId,
											rubricId);

							oiIdMap.put(rubricId, oisList);
						}

						String oiId = oisList.get(position);
						if (oiCache.containsKey(oiId)) {
							oi = oiCache.get(oiId).get();
						} else {
							List<OI> remoteOIsList = constellationService
									.getOIsByRubricId(ectId, oiId);
							if (remoteOIsList.size() > 0) {
								oi = remoteOIsList.get(0);
								oiCache.put(oi.getId(), new WeakReference<OI>(
										oi));
							} else {
								throw new ConstellationException();
							}
						}
					}
					fireDataChanged(oi, taskId);
				} catch (ConstellationException e) {
					errorTreatment(e, taskId);
				}
			}
		};
		Task task = new Task(runnable, taskId);
		task.start();
		return task.getTaskId();
	}

	/**
	 * Retourne tous les projets mobiles publiés.
	 * 
	 * @return L'identifiant de la tâche.
	 */
	public Integer getProjects() {

		final Integer taskId = new Date().hashCode();
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					List<Project> projects = constellationService.getProjects();
					fireDataChanged(projects, taskId);
				} catch (ConstellationException e) {
					errorTreatment(e, taskId);
				}
			}
		};
		Task task = new Task(runnable, taskId);
		task.start();
		return task.getTaskId();
	}

	/**
	 * Retourne les détails d'un projet spécifique.
	 * 
	 * @param id
	 *            L'identifiant du projet que l'on veut récupérer.
	 * @return L'identifiant de la tâche.
	 * @throws ConstellationException
	 *             Une erreur s'est produite lors de la récupération des
	 *             données.
	 */
	public Integer getProjectById(final String id) {

		final Integer taskId = id.hashCode();
		Runnable runnable = new Runnable() {
			public void run() {
				try {

					Project project;
					if (FileUtils.isProjectExists(context, id)) {

						project = FileUtils.getObject("Project_" + id
								+ ".json", context, new TypeReference<Project>() {
						});
						fireDataChanged(project, taskId);
						return;
					}
					
					if (projectCache.containsKey(id)) {
						project = projectCache.get(id).get();
						if (project == null) {
							project = constellationService.getProjectById(id);
							projectCache.put(id, new WeakReference<Project>(
									project));
						}
					} else {

						project = constellationService.getProjectById(id);
						projectCache.put(id,
								new WeakReference<Project>(project));
					}
					fireDataChanged(project, taskId);
				} catch (ConstellationException e) {
					errorTreatment(e, taskId);
				}
			}
		};
		Task task = new Task(runnable, taskId);
		task.start();
		return task.getTaskId();
	}

	/**
	 * Retourne les informations détaillées d'un ou plusieurs OIs.
	 * 
	 * @param ectId
	 *            L'identifiant ECT du guide.
	 * @param playlistId
	 *            L'identifiant de la playlist associée au guide.
	 * @param rubricId
	 *            La liste des rubriques de la playlist.
	 * @return L'identifiant de la tâche.
	 */
	public Integer getOIsByRubricId(final String ectId,
			final String playlistId, final String rubricId, final int index) {

		final Integer taskId = ectId.hashCode() + playlistId.hashCode()
				+ new Date().hashCode();
		Runnable runnable = new Runnable() {
			public void run() {

				try {

					if (FileUtils.isRubricExists(context, rubricId)) {

						OIs result = FileUtils.getObject("Rubric_" + rubricId
								+ ".json", context, new TypeReference<OIs>() {
						});
						List<OI> ois = new ArrayList<OI>();
						if (result != null) {
							ois = result.getOIs();
						}
						fireDataChanged(ois, taskId);
						return;
					}

					List<String> oisList = new ArrayList<String>();
					if (oiIdMap.containsKey(rubricId)) {
						oisList = oiIdMap.get(rubricId);
					} else {

						oisList = constellationService.getOIsIdListByRubricId(
								ectId, playlistId, rubricId);

						oiIdMap.put(rubricId, oisList);
					}

					if (index >= oisList.size()) {

						fireDataChanged(new ArrayList<OI>(), taskId);
						return;
					}

					int size = Math.min(index + 10, oisList.size());
					List<String> finalOIsList = oisList.subList(index, size);

					List<OI> ois = new ArrayList<OI>();
					StringBuffer stringBuffer = new StringBuffer();
					boolean isFirst = true;
					for (Iterator<String> iterator = finalOIsList.iterator(); iterator
							.hasNext();) {

						String id = iterator.next();
						if (oiCache.containsKey(id)) {
							OI oi = oiCache.get(id).get();
							ois.add(oi);
						} else {

							if (isFirst == false) {
								stringBuffer.append("|");
							} else {
								isFirst = false;
							}
							stringBuffer.append(id);
						}
					}

					if (stringBuffer.length() > 0) {

						List<OI> remoteOIsList = constellationService
								.getOIsByRubricId(ectId,
										stringBuffer.toString());
						for (OI oi : remoteOIsList) {
							oiCache.put(oi.getId(), new WeakReference<OI>(oi));
						}
						ois.addAll(remoteOIsList);
					}

					fireDataChanged(ois, taskId);
				} catch (ConstellationException e) {
					errorTreatment(e, taskId);
				}
			}
		};
		Task task = new Task(runnable, taskId);
		task.start();
		return task.getTaskId();
	}

	/**
	 * Retourne les informations détaillées d'un ou plusieurs OIs proche du
	 * point d'origine dans un rayon donné.
	 * 
	 * @param ectId
	 *            L'identifiant ECT du guide.
	 * @param playlistId
	 *            L'identifiant de la playlist associée au guide.
	 * @param rubricId
	 *            La liste des rubriques de la playlist.
	 * @param idType
	 *            Le type des OIs.
	 * @param coordX
	 *            La latitude du point d'origine.
	 * @param coordY
	 *            La longitude du point d'origine.
	 * @param radius
	 *            Le rayon d'action.
	 * @param length
	 *            Le nombre de résultat
	 * @return L'identifiant de la tâche.
	 */
	public Integer getOIsByRubricIdAround(final String ectId,
			final String playlistId, final String rubricId,
			final String idType, final String coordX, final String coordY,
			final String radius, final int length) {

		final Integer taskId = ectId.hashCode() + playlistId.hashCode()
				+ rubricId.hashCode();
		Runnable runnable = new Runnable() {
			public void run() {
				try {

					List<String> oisList = constellationService
							.getOIsByRubricIdAround(ectId, playlistId,
									rubricId, idType, coordX, coordY, radius);
					int size = oisList.size();//Math.min(oisList.size(), length);
					oisList = oisList.subList(0, size);

					List<OI> ois = new ArrayList<OI>();
					StringBuffer stringBuffer = new StringBuffer();
					boolean isFirst = true;
					for (Iterator<String> iterator = oisList.iterator(); iterator
							.hasNext();) {

						String id = iterator.next();
						if (oiCache.containsKey(id)) {
							OI oi = oiCache.get(id).get();
							ois.add(oi);
						} else {

							if (isFirst == false) {
								stringBuffer.append("|");
							} else {
								isFirst = false;
							}
							stringBuffer.append(id);
						}
					}

					if (stringBuffer.length() > 0) {

						List<OI> remoteOIsList = constellationService
								.getOIsByRubricId(ectId,
										stringBuffer.toString());
						for (OI oi : remoteOIsList) {
							if (oi != null) {
								oiCache.put(oi.getId(), new WeakReference<OI>(
										oi));
							}
						}
						ois.addAll(remoteOIsList);
					}

					List<OI> finalOis = new ArrayList<OI>();
					for (String oiId : oisList) {

						for (OI oi : ois) {

							if (oiId.equals(oi.getId())) {
								finalOis.add(oi);
								break;
							}
						}
					}

					fireDataChanged(finalOis, taskId);
				} catch (ConstellationException e) {
					errorTreatment(e, taskId);
				}
			}
		};
		Task task = new Task(runnable, taskId);
		task.start();
		return task.getTaskId();
	}

	public Integer downloadProject(final String id) {

		final Integer taskId = id.hashCode();
		Runnable runnable = new Runnable() {
			public void run() {
				Project project = null;
				try {
					project = constellationService.downloadProject(id);
				} catch (ConstellationException e) {
					errorTreatment(e, -1);
				}

				fireDataChanged(project, taskId);

				if (project != null) {
					int cpt = 0;
					if (project.getThemes() != null) {
						for (Theme theme : project.getThemes()) {
							if (theme.getRubrics() != null) {
								for (Rubric rubric : theme.getRubrics()) {

									try {
										constellationService
												.downloadOIFromRubric(
														project.getEctId(),
														project.getPlaylistId(),
														rubric.getId());
										Log.d(getClass().getSimpleName(),
												"Got a Rubric");
										cpt++;
										fireDataChanged(String.valueOf(cpt),
												taskId);
									} catch (ConstellationException e) {
										errorTreatment(e, taskId);
									}
								}
							}
						}
					}
				}
				fireDataChanged("end", taskId);
			}
		};
		Task task = new Task(runnable, taskId);
		task.start();
		return task.getTaskId();
	}
}
