package com.constellation.core.manager;

import java.util.ArrayList;
import java.util.List;

import com.constellation.core.services.ServiceObserver;

/**
 * Abstraction des managers pour la gestion des observers.
 * 
 * @author j.varin
 * 
 */
public abstract class AbstractManager {

	/**
	 * Liste des observers.
	 */
	private List<ServiceObserver> observers = null;

	/**
	 * Ajout d'un observer.
	 * 
	 * @param observer
	 *            L'observer à ajouter.
	 */
	public void addListener(ServiceObserver observer) {

		if (observers == null) {
			observers = new ArrayList<ServiceObserver>();
		}
		observers.add(observer);
	}

	/**
	 * Suppression d'un observer.
	 * 
	 * @param observer
	 *            L'observer à supprimer.
	 */
	public void removeListener(ServiceObserver observer) {
		if (observers != null) {
			observers.remove(observer);
		}
	}

	/**
	 * Notification des listeners.
	 * 
	 * @param data
	 *            L'objet à notifier.
	 * @param taskId
	 *            L'identifiant de la tâche.
	 */
	protected void fireDataChanged(Object data, Integer taskId) {

		if (observers != null) {
			for (ServiceObserver observer : observers) {
				observer.dataChanged(data, taskId);
			}
		}
	}

	/**
	 * Notification des listeners d'une erreur.
	 * 
	 * @param error
	 *            L'erreur du traitement.
	 * @param taskId
	 *            L'identifiant de la tâche.
	 */
	protected void errorTreatment(Throwable error, Integer taskId) {

		if (observers != null) {
			for (ServiceObserver observer : observers) {
				observer.errorTreatment(error, taskId);
			}
		}
	}
}
