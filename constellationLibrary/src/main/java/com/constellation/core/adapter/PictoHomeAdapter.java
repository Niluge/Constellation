package com.constellation.core.adapter;

import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.constellation.core.R;
import com.constellation.core.model.Theme;
import com.constellation.core.model.ThemeType;
import com.constellation.core.view.DistantImageView;

public class PictoHomeAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private ArrayList<Theme> mItemsHome;
	private Handler mhandler;

	public PictoHomeAdapter(Context c, ArrayList<Theme> itemsHome,
			Handler handler) {

		mInflater = LayoutInflater.from(c);
		mItemsHome = itemsHome;
		mhandler = handler;
	}

	@Override
	public int getCount() {
		return mItemsHome.size();
	}

	@Override
	public Object getItem(int position) {
		return mItemsHome.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Theme theme = mItemsHome.get(position);
		LinearLayout item = null;

		if ((convertView == null) || (convertView.getTag() == null)) {
			item = (LinearLayout) mInflater.inflate(R.layout.item_home, parent,
					false);
			item.setTag(true);
		} else {
			item = (LinearLayout) convertView;
		}

		if (theme.getType() == ThemeType.Separator) {
			if (theme.getDesc().equals("empty")) {
				item = (LinearLayout) mInflater.inflate(R.layout.empty, parent,
						false);
			}
			if (theme.getDesc().equals("divider")) {
				item = (LinearLayout) mInflater.inflate(
						R.layout.dotted_divider, parent, false);
			}
			item.setTag(null);
		} else {

			TextView text = (TextView) item.findViewById(R.id.title);
			text.setText(theme.getName().toUpperCase(Locale.FRANCE));
			DistantImageView picto = (DistantImageView) item
					.findViewById(R.id.theme_picto);
			picto.setHandler(mhandler);
			picto.setUrl(mItemsHome.get(position).getImg());
			item.setTag(true);
		}

		return item;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		
		Theme theme = mItemsHome.get(position);
		return (theme.getType() != ThemeType.Separator);
	}
}