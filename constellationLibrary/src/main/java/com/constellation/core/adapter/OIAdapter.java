package com.constellation.core.adapter;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.constellation.core.R;
import com.constellation.core.activity.OIsActivity;
import com.constellation.core.model.OI;
import com.constellation.core.utils.FilterUtils;
import com.constellation.core.view.DistantImageView;

/**
 * Permet la gestion de la liste de OIs.
 * 
 * @author j.varin, j.attal
 * 
 */
public class OIAdapter extends BaseAdapter {

	private ArrayList<OI> ois;

	/**
	 * Récupère le layout.
	 */
	private LayoutInflater oisInflater;

	/**
	 * Le gestionnaire d'affichage de la page.
	 */
	private Handler handler;
	/**
	 * Contexte de l'application
	 */
	private Context context;
	/**
	 * Nombre total d'OIs
	 */
	private int count;
	/**
	 * Chargement en cours
	 */
	private boolean loading;
	/**
	 * Distance de l'OI de référence
	 */
	private boolean displayDistance;
	/**
	 * Coordonnées de l'OI de référence
	 */
	private Location refPoint;

	/**
	 * Filtre par étoiles
	 */
	// private int filterScore;

	/**
	 * Le constructeur
	 * 
	 * @param context
	 *            Le contexte de l'application.
	 * @param ois
	 *            Liste de OIs à afficher.
	 * @param handler
	 *            Le gestionnaire d'affichage de la page.
	 */
	public OIAdapter(Context context, ArrayList<OI> ois, Handler handler,
			int count) {

		this.ois = ois;
		this.oisInflater = LayoutInflater.from(context);
		this.handler = handler;
		this.context = context;
		this.count = count;
		this.loading = false;
		this.displayDistance = false;
	}

	/**
	 * Ajout d'OIs dans l'adapter
	 * 
	 * @param ois
	 */
	public void setOIs(ArrayList<OI> ois) {
		this.ois.addAll(ois);
		this.loading = false;
		this.notifyDataSetChanged();
	}

	public ArrayList<OI> getOIs() {
		return this.ois;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return this.ois.size();
	}

	public void setTotalCount(int size) {
		this.count = size;
		this.loading = false;
		this.notifyDataSetChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) {
		return position;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	public void setDistance(Location ref) {
		this.displayDistance = true;
		refPoint = ref;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;
		if ((convertView == null) || (convertView.getTag() == null)) {
			
			convertView = oisInflater.inflate(R.layout.oi_row, parent, false);
			holder = new ViewHolder();
			holder.image = (DistantImageView) convertView
					.findViewById(R.id.oi_image);
			holder.text = (TextView) convertView.findViewById(R.id.oi_title);
			holder.starsBar = (RatingBar) convertView
					.findViewById(R.id.oi_stars);
			holder.textD = (TextView) convertView.findViewById(R.id.oi_text);
			holder.distanceView = (TextView) convertView
					.findViewById(R.id.distance);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		// Chargement de l'OI dans l'adapter
		OI oi = ois.get(position);
		holder.image.setHandler(handler);
		holder.image.setDefaultImage(context.getResources().getDrawable(
				R.drawable.img_default));
		
		if (oi != null) {
			// Chargement des images dans la Gallery
			if ((oi.getMedias().size() > 0)
					&& (oi.getMedias().get(0).getUrl().length() > 0)) {
				holder.image.setUrl(oi.getMedias().get(0).getUrl());
			}
			else {
				holder.image.setUrl("");
			}
			if ((oi.getTitle() != null) && (oi.getTitle().length() > 0)) {
				holder.text.setText(oi.getTitle() + " ");
			} else if (oi.getTrad().getTitre() != null) {
				holder.text.setText(oi.getTrad().getTitre() + " ");
			}

			// Brève description
			String dates = "";
			if ((oi.getDates() != null) && (oi.getDates().size() > 0)) {
				for (int i = 0; i < oi.getDates().size(); i++) {
					String from = oi.getDates().get(i).getFrom();
					String to = oi.getDates().get(i).getTo();
					if (from.equals(to))
						dates += "Le " + from + "\n";
					else
						dates += "Du " + from + " au " + to + "\n";
				}
			}
			holder.textD.setText(dates + getAddress(oi) + getCom(oi));
		}
		if (displayDistance) {
			
			Location pointOI = new Location("pointOI");
			pointOI.setLongitude(Double.parseDouble(Float.toString(oi.getCoordX())));
			pointOI.setLatitude(Double.parseDouble(Float.toString(oi.getCoordY())));
			Float distanceM = pointOI.distanceTo(refPoint);
			float distanceKm = distanceM / 1000;

			NumberFormat formater = new DecimalFormat(".#");
			String distanceKmStr = formater.format(distanceKm);

			holder.distanceView.setVisibility(View.VISIBLE);
			if (distanceKm < 1) {
				holder.distanceView.setText(distanceM.intValue() + " m ");
			} else {
				holder.distanceView.setText(distanceKmStr + " km ");
			}
		}

		// Affichage des étoiles
		int nbStars = 0;
		if (oi != null) {
			if ((oi.getClassOfficiels() != null)
					&& ((oi.getClassOfficiels().size() > 0))) {
				nbStars = FilterUtils.getScore(oi.getClassOfficiels().get(0)
						.getCode());
			} else if ((oi.getClassLabels() != null)
					&& ((oi.getClassLabels().size() > 0))) {
				nbStars = FilterUtils.getScore(oi.getClassLabels().get(0)
						.getCode());
			}
		}

		if (nbStars > 0) {

			holder.starsBar.setVisibility(View.VISIBLE);
			holder.starsBar.setNumStars(nbStars);
			holder.starsBar.setRating(Float.intBitsToFloat(nbStars));
		} else {
			holder.starsBar.setVisibility(View.GONE);
		}

		// Chargement des 10 OIs suivants
		if ((position >= (getCount() - 1)) && (position != (count - 1))) {
			if (!loading) {
				((OIsActivity) context).loadNextOIs();
				Log.d(getClass().getSimpleName(), "Load next");
				this.loading = true;
			}
			View loadingView = oisInflater.inflate(R.layout.loading, null);
			loadingView.setTag(null);
			loadingView.setClickable(false);
			return loadingView;
		}
		return convertView;
	}

	private String getCom(OI oi) {
		String communication = "";
		for (int i = 0; i < oi.getMoyensCom().size(); i++) {
			communication += oi.getMoyensCom().get(i).getType() + " : "
					+ oi.getMoyensCom().get(i).getCoord() + "\n";
		}
		return communication;
	}

	private String getAddress(OI oi) {
		// Address0
		String address = oi.getAddress().getAddress0();
		if (!oi.getAddress().getAddress0().equals(""))
			address += " ";
		// ComplementAddress
		address += oi.getAddress().getComplementAddress();
		if (!oi.getAddress().getComplementAddress().equals(""))
			address += "\n";
		// LibelleVoie
		address += oi.getAddress().getLibelleVoie();
		if (!oi.getAddress().getLibelleVoie().equals(""))
			address += "\n";
		// DistributionSpeciale
		address += oi.getAddress().getDistributionSpeciale();
		if (!oi.getAddress().getDistributionSpeciale().equals(""))
			address += "\n";
		// CodePostal
		address += oi.getAddress().getCodePostal();
		if (!oi.getAddress().getCodePostal().equals(""))
			address += " ";
		// Commune
		address += oi.getAddress().getCommune();

		if (!address.equals(""))
			address += "\n";
		return address;
	}

	private class ViewHolder {
		DistantImageView image;
		TextView text;
		RatingBar starsBar;
		TextView textD;
		TextView distanceView;
	}

}