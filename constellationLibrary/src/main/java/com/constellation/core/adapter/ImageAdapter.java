package com.constellation.core.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.constellation.core.R;
import com.constellation.core.model.EmbeddedMedia;
import com.constellation.core.model.OI;
import com.constellation.core.view.DistantImageView;

public class ImageAdapter extends BaseAdapter {
	private Context mContext;
	private Handler mhandler;
	int mH;
	int mW;
	private OI mOI;
	private Drawable fond;

	public ImageAdapter(Context c, Handler handler, int h, Drawable fond, OI oi) {
		mContext = c;
		mhandler = handler;
		mH = h / 3;
		mW = (mH * 3) / 2;
		mOI = oi;
		this.fond = fond;
	}

	@Override
	public int getCount() {
		return mOI.getMedias().size() + mOI.getEmbeddedMedias().size();
	}

	@Override
	public Object getItem(int position) {
		if (position < mOI.getEmbeddedMedias().size()) {
			return mOI.getEmbeddedMedias().get(position);
		} else {
			int location = position - mOI.getEmbeddedMedias().size();
			return mOI.getMedias().get(location);
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LinearLayout l = new LinearLayout(mContext);
		l.setLayoutParams(new Gallery.LayoutParams(mW - (mH / 10), mH
				- (mH / 10)));
		l.setGravity(Gravity.CENTER);
		l.setBackgroundDrawable(fond);

		if (position < mOI.getEmbeddedMedias().size()) {
			
			l.setOrientation(LinearLayout.VERTICAL);
			ImageView image = new ImageView(mContext);
			image.setImageDrawable(mContext.getResources().getDrawable(
					R.drawable.ic_play));
			image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			l.addView(image);
	
			TextView i = new TextView(mContext);
			
			EmbeddedMedia media = mOI.getEmbeddedMedias().get(position);
			i.setText(media.getTitre());
			i.setGravity(Gravity.CENTER_HORIZONTAL);
	
			l.addView(i);
		} else {

			DistantImageView i = new DistantImageView(mContext);
			i.setDefaultImage(mContext.getResources().getDrawable(
					R.drawable.img_default));
			int plus = 4;
			if (mH <= 120)
				plus += 2;
			i.setLayoutParams(new Gallery.LayoutParams(mW - (mH / 10) + plus,
					mH - (mH / 10) + plus));
			i.setPadding(4, 4, 4, 4);
			i.setScaleType(ImageView.ScaleType.FIT_CENTER);
			i.setHandler(mhandler);
			i.setImageResource(R.drawable.img_default);
			int location = position - mOI.getEmbeddedMedias().size();
			i.setUrl(mOI.getMedias().get(location).getUrl()
					.replaceFirst("/m_", "/f_"));

			l.addView(i);
		}

		return l;
	}
}