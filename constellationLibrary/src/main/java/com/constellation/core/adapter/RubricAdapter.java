package com.constellation.core.adapter;

import java.util.List;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.constellation.core.R;
import com.constellation.core.model.Rubric;
import com.constellation.core.view.DistantImageView;

/**
 * Permet la gestion de la liste de rubriques.
 * 
 * @author j.varin
 * 
 */
public class RubricAdapter extends BaseAdapter {
	
	/**
	 * Liste de rubriques à afficher.
	 */
	private List<Rubric> rubrics;
	
	/**
	 * Récupère le layout.
	 */
	private LayoutInflater rubricsInflater;
	
	/**
	 * Le gestionnaire d'affichage de la page.
	 */
	private Handler handler;

	/**
	 * Le constructeur.
	 * 
	 * @param context
	 *            Le contexte de l'application.
	 * @param rubrics
	 *            Liste de rubriques à afficher.
	 * @param handler
	 *            Le gestionnaire d'affichage de la page.
	 * @param displayNumber 
	 */
	public RubricAdapter(Context c, List<Rubric> rubrics, Handler handler) {

		this.rubrics = rubrics;
		this.rubricsInflater = LayoutInflater.from(c);
		this.handler = handler;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return rubrics.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) {
		return position;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Rubric rubric = rubrics.get(position);
		LinearLayout item = null;
		if (convertView == null) {
			item = (LinearLayout) rubricsInflater.inflate(R.layout.rubric_row,
					parent, false);
		} else {
			item = (LinearLayout) convertView;
		}

		TextView rubricTextView = (TextView) item
				.findViewById(R.id.rubric_text);

		StringBuilder rubricText = new StringBuilder();
		rubricText.append(rubric.getTitle());
		rubricText.append(" (");
		rubricText.append(rubric.getTotalOIs());
		rubricText.append(")");
		rubricTextView.setText(rubricText.toString());

		DistantImageView rubricPictoView = (DistantImageView) item
				.findViewById(R.id.rubric_picto);
		rubricPictoView.setImageResource(R.drawable.ic_i_defaut);
		rubricPictoView.setHandler(handler);
		rubricPictoView.setUrl(rubric.getImage());

		return item;
	}
}