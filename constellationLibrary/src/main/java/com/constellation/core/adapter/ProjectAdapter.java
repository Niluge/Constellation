package com.constellation.core.adapter;

import java.util.ArrayList;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.constellation.core.R;
import com.constellation.core.model.Project;
import com.constellation.core.view.DistantImageView;

/**
 * Permet la gestion de la liste de OIs.
 * 
 * @author j.varin, j.attal
 * 
 */
public class ProjectAdapter extends BaseAdapter {
	
	
    private ArrayList<Project> projects;
    
    /**
	 * Récupère le layout.
	 */
	private LayoutInflater projectsInflater;
	
	/**
	 * Le gestionnaire d'affichage de la page.
	 */
	private Handler handler;
	
	
	/**
	 * Le constructeur.
	 * 
	 * @param context
	 *            Le contexte de l'application.
	 * @param ois
	 *            Liste de OIs à afficher.
	 * @param handler
	 *            Le gestionnaire d'affichage de la page.
	 */
    public ProjectAdapter(Context context, ArrayList<Project> projects, Handler handler) {
    	
    	this.projects = projects;
    	this.projectsInflater = LayoutInflater.from(context);
        this.handler = handler;
    }
    /*
     * (non-Javadoc)
     * @see android.widget.Adapter#getCount()
     */
    @Override
	public int getCount() {
        return projects.size();
    }

    /*
     * (non-Javadoc)
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
	public Object getItem(int position) {
        return position;
    }

    /*
     * (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
	public long getItemId(int position) {
        return position;
    }

    /*
     * (non-Javadoc)
     * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
    	
    	Project project = projects.get(position);
    	LinearLayout item = null;
		if (convertView == null) {
			item = (LinearLayout) projectsInflater.inflate(R.layout.project_row,
					parent, false);
		} else {
			item = (LinearLayout) convertView;
		}
		
		TextView project_name = (TextView)item.findViewById(R.id.project_name);
		project_name.setText(project.getTitle());
 	   	TextView project_desc = (TextView)item.findViewById(R.id.project_desc);
 	   	project_desc.setText(project.getDescription());
 	   	
 	   
 	   	DistantImageView image = (DistantImageView)item.findViewById(R.id.project_logo);
 	   	image.setImageResource(R.drawable.ic_t_default);
 	   	image.setHandler(handler);
 	   	image.setUrl(project.getLogo());
 	   	
 	   	return item;
    }
   
}