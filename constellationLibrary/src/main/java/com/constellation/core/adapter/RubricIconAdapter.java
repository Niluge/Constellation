package com.constellation.core.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.constellation.core.R;
import com.constellation.core.model.Rubric;
import com.constellation.core.view.DistantImageView;

public class RubricIconAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ArrayList<Rubric> mItemsRubric;
    private Handler mhandler;

    public RubricIconAdapter(Context c,ArrayList<Rubric> itemsRubric,Handler handler) {
        mInflater = LayoutInflater.from(c);
        mItemsRubric = itemsRubric;
        mhandler = handler;
    }
    
    @Override
	public int getCount() {
        return mItemsRubric.size();
    }

    @Override
	public Object getItem(int position) {
        return null;
    }

    @Override
	public long getItemId(int position) {
        return position;
    }

   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout item = null;
        if (convertView == null) {
        	item = fillView(item,parent,position);
        } else {
        	item = (LinearLayout)convertView;
        	item = fillView(item,parent,position);
        }
        return item;
        
    }
   private LinearLayout fillView(LinearLayout item,ViewGroup parent,int position)
   {
	   	item = (LinearLayout)mInflater.inflate(R.layout.rubric_item, parent, false);
	   	
		   	TextView text = (TextView) item.findViewById(R.id.rubric_text);
		   	StringBuilder rubricText = new StringBuilder();
			rubricText.append(mItemsRubric.get(position).getTitle());
			rubricText.append(" (");
			rubricText.append(mItemsRubric.get(position).getTotalOIs());
			rubricText.append(")");
			text.setText(rubricText.toString());
		   	DistantImageView picto = (DistantImageView) item.findViewById(R.id.rubric_picto);
		   	picto.setHandler(mhandler);
		   	String pictoUrl = mItemsRubric.get(position).getImage().replace("/c_", "/i_");
		   	picto.setUrl(pictoUrl);
	   	return item;
   }
}