package com.constellation.core.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.constellation.core.R;
import com.constellation.core.model.Template;

public class HeaderLayout extends FrameLayout {

	/**
	 * URL de l'image du header.
	 */
	private String headerUrl;

	private String headerTitle;

	private String headerTitlePictoUrl;

	private Template blur;

	private boolean hasTitle;

	public HeaderLayout(Context context) {

		super(context);
		construct(context, null);
	}

	public HeaderLayout(Context context, AttributeSet attrs) {

		super(context, attrs);
		construct(context, attrs);
	}

	public HeaderLayout(Context context, AttributeSet attrs, int defStyle) {

		super(context, attrs, defStyle);
		construct(context, attrs);
	}

	/**
	 * @return the headerTitle
	 */
	public String getHeaderUrl() {
		return headerUrl;
	}

	/**
	 * @param headerTitle
	 *            the headerTitle to set
	 */
	public void setHeaderUrl(String headerUrl) {
		this.headerUrl = headerUrl;
	}

	/**
	 * @return the headerTitle
	 */
	public String getHeaderTitle() {
		return headerTitle;
	}

	/**
	 * @param headerTitle
	 *            the headerTitle to set
	 * @param trebuc
	 */
	public void setHeaderTitle(String headerTitle) {
		this.headerTitle = headerTitle;
	}

	public void setBlur(Template blur) {
		this.blur = blur;
	}

	public Template getBlur() {
		return blur;
	}

	/**
	 * @return the hasTitle
	 */
	public boolean hasTitle() {
		return hasTitle;
	}

	/**
	 * @param hasTitle
	 *            the hasTitle to set
	 */
	public void setHasTitle(boolean hasTitle) {
		this.hasTitle = hasTitle;
	}

	/**
	 * @return the headerTitlePicto
	 */
	public String getHeaderTitlePictoUrl() {
		return headerTitlePictoUrl;
	}

	/**
	 * @param headerTitlePicto
	 *            the headerTitlePicto to set
	 */
	public void setHeaderTitlePictoUrl(String headerTitlePictoUrl) {
		this.headerTitlePictoUrl = headerTitlePictoUrl;
	}

	private void construct(Context context, AttributeSet attrs) {

		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.header, this);
		this.hasTitle = false;
	}

	@Override
	public void drawableStateChanged() {

		DistantImageView headerImageView = (DistantImageView) findViewById(R.id.header_image);
		if ((headerUrl != null) && (headerUrl.length() > 0)) {
			headerImageView.setHandler(getHandler());
			headerImageView.setUrl(headerUrl);
		}

		if (hasTitle) {

			TextView headerTitleTextView = (TextView) findViewById(R.id.header_title_text);
			headerTitleTextView.setText(headerTitle);
			Typeface trebuc = Typeface.createFromAsset(
					getContext().getAssets(), "fonts/trebuc.ttf");
			headerTitleTextView.setTypeface(trebuc);

			DistantImageView headerTitlePictoImageView = (DistantImageView) findViewById(R.id.header_title_picto_image);
			if ((headerTitlePictoUrl != null)
					&& (headerTitlePictoUrl.length() > 0)) {
				headerTitlePictoImageView.setHandler(getHandler());
				headerTitlePictoImageView.setUrl(headerTitlePictoUrl);
			}

			ImageView headerBlurView = (ImageView) findViewById(R.id.header_blur);
			headerBlurView.setVisibility(View.GONE);
		} else {

			LinearLayout headerTitleView = (LinearLayout) findViewById(R.id.header_title);
			headerTitleView.setVisibility(View.GONE);
			FrameLayout headertitlePictoView = (FrameLayout) findViewById(R.id.header_title_picto);
			headertitlePictoView.setVisibility(View.GONE);
		}

		super.drawableStateChanged();
	}
}
