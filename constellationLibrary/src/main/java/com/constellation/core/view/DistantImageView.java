package com.constellation.core.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.constellation.core.view.ImageCache.ImageCallback;

/**
 * Vue qui télécharge et affiche des images depuis une URL.
 * 
 * @author j.varin
 * 
 */
public class DistantImageView extends ImageView implements ImageCallback {

	/**
	 * L'URL de l'image à afficher.
	 */
	private String url = null;

	/**
	 * L'image par défaut.
	 */
	private Drawable defaultImage;

	/**
	 * Le Handler qui gère l'affichage de l'image sur la thread principal.
	 */
	private Handler handler;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View#getHandler()
	 */
	@Override
	public Handler getHandler() {
		return handler;
	}

	/**
	 * Modifie le handler.
	 * 
	 * @param handler
	 *            Le handler à modifier.
	 */
	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param context
	 *            Le context d'appel.
	 */
	public DistantImageView(Context context) {
		super(context);
	}

	/**
	 * Constructeur paramétré.
	 * 
	 * @param context
	 *            Le context d'appel.
	 * @param attrs
	 *            Les attributs.
	 */
	public DistantImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Constructor paramétré.
	 * 
	 * @param context
	 *            Le context d'appel.
	 * @param attrs
	 *            Les attributs.
	 * @param defStyle
	 *            La définition du style.
	 */
	public DistantImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	/**
	 * Modifie l'URL de l'image à afficher.
	 * 
	 * @param url
	 *            L'url à modifier.
	 */
	public void setUrl(String url) {

		this.url = url;
		if ((this.url != null) && (this.url.length() > 0)) {
			ImageCache.getInstance().loadAsync(this.url, this,
					this.getContext());
		} else {
			if (defaultImage != null) {
				this.setImageDrawable(defaultImage);
			}
		}
	}

	/**
	 * Modifie l'image par défaut.
	 * 
	 * @param defaultImage
	 *            L'image par défaut.
	 */
	public void setDefaultImage(Drawable defaultImage) {
		this.defaultImage = defaultImage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.player.view.ImageCache.ImageCallback#onImageLoaded(
	 * android.graphics.drawable.Drawable, java.lang.String)
	 */
	@Override
	public void onImageLoaded(final Drawable image, String url) {

		if (getHandler() == null) {
			return;
		}

		if (this.url.equalsIgnoreCase(url)) {
			getHandler().post(new Runnable() {

				@Override
				public void run() {
					setImageDrawable(image);
				}
			});
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.constellation.player.view.ImageCache.ImageCallback#onDownlaodFailed
	 * (android.graphics.drawable.Drawable, java.lang.String)
	 */
	@Override
	public void onDownlaodFailed(Drawable image, String url) {

		final DistantImageView imageView = this;
		if (this.url.equals(url)) {
			getHandler().post(new Runnable() {

				@Override
				public void run() {
					imageView.setImageDrawable(defaultImage);
				}
			});
		}
	}
}
