package com.constellation.core.view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Permet la gestion du cache des images.
 * 
 * @author j.varin
 * 
 */
public class ImageCache {

	/**
	 * Interface qui permet le téléchargement des images de manière asynchrone.
	 * 
	 * @author j.varin
	 * 
	 */
	public static interface ImageCallback {

		/**
		 * Traitement quand le téléchargement a réussi.
		 * 
		 * @param image
		 *            L'image à afficher.
		 * @param url
		 *            L'url.
		 */
		void onImageLoaded(Drawable image, String url);

		/**
		 * Traitement quand le téléchargement a échoué.
		 * 
		 * @param image
		 *            L'image à afficher.
		 * @param url
		 *            L'url.
		 */
		void onDownlaodFailed(Drawable image, String url);
	}

	/**
	 * Singlaton du cache.
	 */
	private static ImageCache SINGLETON = null;

	/**
	 * Verrou d'accès au cache.
	 */
	private final Object lock = new Object();

	/**
	 * La map du contenu des images du cache.
	 */
	private HashMap<String, WeakReference<Drawable>> cache;

	/**
	 * La map des classes traitant les images.
	 */
	private HashMap<String, List<ImageCallback>> callbacks;

	/**
	 * Retourne l'instance unique de ImageCache.
	 * 
	 * @return Le singleton ImageCache.
	 */
	public synchronized static ImageCache getInstance() {

		if (SINGLETON == null) {
			SINGLETON = new ImageCache();
		}
		return SINGLETON;
	}

	/**
	 * Constructeur.
	 */
	private ImageCache() {

		this.cache = new HashMap<String, WeakReference<Drawable>>();
		this.callbacks = new HashMap<String, List<ImageCallback>>();
	}

	/**
	 * Retourne le hash code de l'URL passé en paramètre.
	 * 
	 * @param url
	 *            L'URL à traiter.
	 * @return Le hash code associé.
	 */
	private String getHash(String url) {

		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(url.getBytes());

			return new BigInteger(digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException ex) {
			return url;
		}
	}

	/**
	 * Retourne le contenu d'une image du cache correspondant au hash code passé
	 * en paramètre.
	 * 
	 * @param hash
	 *            Le hash code de l'URL de l'image.
	 * @return Le contenu de l'image.
	 */
	private Drawable drawableFromCache(String hash) {

		Drawable d = null;
		synchronized (lock) {

			if (this.cache.containsKey(hash)) {

				WeakReference<Drawable> ref = this.cache.get(hash);
				if (ref != null) {
					d = ref.get();
					if (d == null)
						this.cache.remove(hash);
				}
			}
		}
		return d;
	}

	/**
	 * Retourne le contenu de l'image associé à l'URL passé en paramètre.
	 * 
	 * @param url
	 *            L'URL de l'image.
	 * @param hash
	 *            Le hash code de l'URL.
	 * @param context
	 *            Le contexte d'appel.
	 * @return Le contenu de l'image.
	 */
	private Drawable loadSync(String url, String hash, Context context) {

		Drawable d = null;
		try {
			d = drawableFromCache(hash);

			if (d == null) {

				File f = new File(context.getCacheDir(), hash);
				if (!f.exists()) {
					InputStream is = new URL(url).openConnection()
							.getInputStream();
					if (f.createNewFile()) {
						FileOutputStream fo = new FileOutputStream(f);
						byte[] buffer = new byte[256];
						int size;
						while ((size = is.read(buffer)) > 0)
							fo.write(buffer, 0, size);
						fo.flush();
						fo.close();
					}
				}
				d = Drawable.createFromPath(f.getAbsolutePath());
				synchronized (lock) {
					cache.put(hash, new WeakReference<Drawable>(d));
				}
			}
		} catch (Exception ex) {
			return null;
		} catch (OutOfMemoryError ex) {
			return null;
		}
		return d;
	}

	/**
	 * Enregistre le gestionnaire de traitement d'image et lance la récupération
	 * de l'image contenu dans l'URL.
	 * 
	 * @param url
	 *            L'URL de l'image.
	 * @param callback
	 *            Le gestionnaire de traitement de l'image
	 * @param context
	 *            Le contexte d'appel.
	 */
	public void loadAsync(final String url, final ImageCallback callback,
			final Context context) {

		final String hash = getHash(url);
		synchronized (lock) {
			List<ImageCallback> imageCallbacks = this.callbacks.get(hash);
			if (imageCallbacks != null) {
				if (callback != null)
					imageCallbacks.add(callback);
				return;
			}

			imageCallbacks = new ArrayList<ImageCallback>();
			if (callback != null)
				imageCallbacks.add(callback);
			this.callbacks.put(hash, imageCallbacks);
		}

		new Thread(new Runnable() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {

				Drawable d = loadSync(url, hash, context);
				List<ImageCallback> imageCallbacks;
				synchronized (lock) {
					imageCallbacks = callbacks.remove(hash);
				}
				for (ImageCallback iter : imageCallbacks) {
					if (d != null)
						iter.onImageLoaded(d, url);
					else
						iter.onDownlaodFailed(d, url);
				}
			}
		}, "ImageCache loader: " + url).start();
	}
}
