package com.constellation.core.view;

import com.constellation.core.model.OI;
import com.constellation.core.model.Project;
import com.constellation.core.model.Rubric;
import com.constellation.core.model.Theme;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class ConstellationOverlay extends OverlayItem{
	

	public ConstellationOverlay(GeoPoint point, String title, String snippet) {
		super(point, title, snippet);
	}
	private Project project;
	private Theme theme;
	private Rubric rubric;
	private OI oi;
	
	public void setProject(Project project) {
		this.project = project;
	}

	public Project getProject() {
		return project;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}

	public Theme getTheme() {
		return theme;
	}

	public void setRubric(Rubric rubric) {
		this.rubric = rubric;
	}

	public Rubric getRubric() {
		return rubric;
	}

	public void setOi(OI oi) {
		this.oi = oi;
	}

	public OI getOi() {
		return oi;
	}

}
