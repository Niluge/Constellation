package com.constellation.core.view;

import java.util.ArrayList;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.constellation.core.R;
import com.constellation.core.activity.OIActivity;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

public class APIItemizedOverlay extends ItemizedOverlay<ConstellationOverlay> {
	private ArrayList<ConstellationOverlay> mOverlays = new ArrayList<ConstellationOverlay>();
	private Context mContext;
	private boolean isOnItem;
	private ConstellationOverlay item;
	private LinearLayout mapDesc;
	private LinearLayout OIDesc;
	private MapController mapController;
	private int currentIndex;

	public APIItemizedOverlay(Drawable defaultMarker, Context context) {
		super(boundCenterBottom(defaultMarker));
		mContext = context;
		currentIndex = -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.android.maps.ItemizedOverlay#draw(android.graphics.Canvas,
	 * com.google.android.maps.MapView, boolean)
	 */
	@Override
	public void draw(android.graphics.Canvas canvas, MapView mapView,
			boolean shadow) {
		super.draw(canvas, mapView, false);
		mapDesc = (LinearLayout) mapView.getParent().getParent();
		OIDesc = (LinearLayout) mapDesc.findViewById(R.id.mapText);
		mapController = mapView.getController();
		if (isOnItem) {
			Log.d("Draw", "draw on item");
			mapController.animateTo(item.getPoint());
			OIDesc.setVisibility(View.VISIBLE);
			TextView textTheme = (TextView) mapDesc
					.findViewById(R.id.mapTextTheme);
			textTheme.setText(item.getTitle());
			TextView textTitle = (TextView) mapDesc
					.findViewById(R.id.mapTextTitle);
			textTitle.setText(item.getSnippet());
			isOnItem = false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.android.maps.ItemizedOverlay#createItem(int)
	 */
	@Override
	protected ConstellationOverlay createItem(int i) {
		return mOverlays.get(i);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.android.maps.ItemizedOverlay#size()
	 */
	@Override
	public int size() {
		return mOverlays.size();
	}

	public void addOverlay(ConstellationOverlay overlay) {
		mOverlays.add(overlay);
		populate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.android.maps.ItemizedOverlay#onTap(int)
	 */
	@Override
	protected boolean onTap(int index) {
		Log.d("Tap", "Tap : " + index);
		boolean isTheSame;
		if (currentIndex == index) {
			isTheSame = true;
		} else {
			isTheSame = false;
		}
		currentIndex = index;
		item = mOverlays.get(index);
		Log.d("Title", "Title : " + item.getTitle());
		if ((OIDesc.getVisibility() == View.VISIBLE) && (isTheSame)) {
			Intent intent = new Intent(mContext, OIActivity.class);
			intent.putExtra("project", item.getProject());
			intent.putExtra("theme", item.getTheme());
			intent.putExtra("rubric", item.getRubric());
			intent.putExtra("oi", item.getOi());
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			mContext.startActivity(intent);
		}
		isOnItem = true;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.android.maps.ItemizedOverlay#onTouchEvent(android.view.MotionEvent
	 * , com.google.android.maps.MapView)
	 */
	@Override
	public boolean onTouchEvent(android.view.MotionEvent event, MapView mapView) {
		if ((!isOnItem)
				&& (event.getAction() == android.view.MotionEvent.ACTION_MOVE)) {

			mapDesc = (LinearLayout) mapView.getParent().getParent();
			OIDesc = (LinearLayout) mapDesc.findViewById(R.id.mapText);
			OIDesc.setVisibility(View.GONE);
		}

		return super.onTouchEvent(event, mapView);
	}
}
